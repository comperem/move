# behaviors:followRoute(), goToGate(), goToPoint()
#       stayInBounds()
#       avoidBySteer()
#       avoidByBrake()
#       avoidComputeOtherVehPositionInLocalFrame()
#       detectAndReport()
#
# this is an include in the Behaviors class, in ./behaviors.py
#
# Marc Compere, comperem@erau.edu
# created : 18 Feb 2024
# modified: 18 Feb 2024
#
# --------------------------------------------------------------
# Copyright 2018 - 2024 Marc Compere
#
# This file is part of the Mobility Virtual Environment (MoVE).
# MoVE is open source software licensed under the GNU GPLv3.
#
# MoVE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3
# as published by the Free Software Foundation.
#
# MoVE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License version 3 for more details.
#
# A copy of the GNU General Public License is included with MoVE
# in the COPYING file, or see <https://www.gnu.org/licenses/>.
# --------------------------------------------------------------

import random
import time
from datetime import datetime
import queue
import numpy as np
import logging
from math import sin,cos,atan2,pi,copysign,sqrt,tanh
import code # drop into a python interpreter to debug using: code.interact(local=locals())
from pprint import pformat # pretty print v2v structure




# --------------------------------------------------------------
#                  behavior: followRoute()
# --------------------------------------------------------------
# followRoute process: if enabled, provide steer and speed commands to follow
# the route specified in the config file in ../scenario and waypoints
# in the restore_waypoint() function defined in ../routes
def followRoute(self,bId,behName,priority,veh,debug):
    logging.debug('\t\t[{0}]: bId={1} [{2}], priority={3}'.format( datetime.now().strftime('%c') ,bId,behName,priority))
    e=veh.e
    route=self.route
    routeCfg=self.routeCfg
    
    steer_max = 35*np.pi/180.0 # (rad) set lock-to-lock max steer limit of 35deg, from ME620 Adv. Veh. Dyn. (~0.6rad)
    k_steer = 1.5 # steer gain between 1 and 1.5 is good; tuned in Simulink, ME620 Advanced Vehicle Dynamics
    
    dt = 0.1 # (s) waypoint steering update period
    T_console_update = 1.0 # (s)
    N_console_update = T_console_update/dt # integer number of e.wait(dt)'s to skip before providing a console update
    
    if not hasattr(route,'i'):
        route.i = 1 # if IC has not been set at 1st waypoint, we must be aiming for 1st waypoint; route waypoints are 0-based
        #spd_mps_i=route.spd_mps[ route.i ]
    
    if not hasattr(route,'lap'):
        route.lap=1 # this should be set in route_init() in route_init.py in ./veh_model
    
    if ('waypointDelay' in routeCfg):
        waypointDelay = routeCfg['waypointDelay'] # make a dict of all waypoint delays, like: waypointDelay={3: 5.1, 7: 2.0}
        spd_of_zero = 0.0
        already_paused_at_this_waypoint=False
    
    loopCnt=0
    atEndOfLaps  = False # (T/F) this must be False to start route following and changes to True at the end
    atEndOfRoute = False # (T/F) this (also) must be False to start route following
    
    # compute initial distance to first waypoint, route.i
    dX            = route.X[ route.i ] - veh.pos.X # (m) X-component of vector pointing from veh to next waypoint (tip-tail)
    dY            = route.Y[ route.i ] - veh.pos.Y # (m) Y-component of veh-to-next-waypoint vector (tip - tail)
    dist_XY_start = np.sqrt( pow(dX,2) + pow(dY,2) ) # (m) 2D distance between next waypoint and vehicle
    
    logging.debug('{0}: entering loop starting at waypoint route.i=[{1}], dist_XY_start={2}'.format(behName,route.i,dist_XY_start))
    
    while e.is_set() is False:
        
        e.wait(dt) # (sec) behavior is dormant for this duration; e.wait() is an interruptable sleep() with independent timers despite using the same event, e
        
        loopCnt = loopCnt+1
        
        if (not atEndOfLaps) and (not atEndOfRoute):
            
            if (loopCnt%N_console_update)==0: # modulo test is True every N'th: test = ( (cnt%N)==0 )
                logging.debug('{0}: route update, current waypoint route.i=[{1}, route.lap=[{2}]'.format(behName,route.i,route.lap))
            
            # --------------------------------------------------------------
            # steer toward the next waypoint - turn which way and how much? (based on 2D distance only)
            dX = route.X[ route.i ] - veh.pos.X # (m) X-component of vector pointing from veh to next waypoint (tip-tail)
            dY = route.Y[ route.i ] - veh.pos.Y # (m) Y-component of veh-to-next-waypoint vector (tip - tail)
            psi_to_waypoint = atan2( dY , dX ) # (rad) 4-quadrant arc-tangent provides orientation from veh to next waypoint
            
            psi_veh = veh.pos.psi         # (rad) current vehicle heading, which could be large from multiple turns (i.e. >> +/- 2pi)
            
            psi_err_raw = psi_to_waypoint - psi_veh # (rad) heading error; this could be a large number, like +/- 100, way beyond +/- pi
            
            # wrap heading error, psi_err_raw, such that psi_err is on [-pi  +pi]
            # credit: Mike Fair, Nov 2004
            spsi = sin(psi_err_raw)
            cpsi = cos(psi_err_raw)
            # psi_err is in body-fixed frame on inteval [-pi +pi] - this essentially says, turn left (-) or turn right (+)
            psi_err = atan2(spsi,cpsi) # (rad) heading error w.r.t. vehicle x-axis, atan2() returns [-pi  +pi]
            
            steer_raw = k_steer*psi_err # (rad) steering gain for path following (simple pursuit control law w/o lateral error term)
            steer = np.clip( steer_raw , -steer_max, +steer_max) # (rad) impose max steer angle, clip is numpy's saturation function
            logging.debug('[{0}]: steer_max={1}, steer_raw={2}, steer={3}'.format( datetime.now().strftime('%c') ,steer_max,steer_raw,steer))
            # --------------------------------------------------------------
            
            # what is velocity command between these 2 waypoints?
            spd_mps_i=route.spd_mps[ route.i ] # (m/s) this speed is held constant for the entire waypoint segment
            
            if ('waypointDelay' in routeCfg) and (route.i in waypointDelay) and already_paused_at_this_waypoint==False:
                logging.debug('{0}: discovered a waypointDelay at route.i=[{1}], setting speed=0 and waiting {2} seconds...'.format(behName,route.i, waypointDelay[route.i]))
                self.qBehavior.put( [ bId , steer , spd_of_zero, 0.0, 0.0, 0.0 ] ) # [ bId , steer , speed, pitch, v_z, progress ]
                time.sleep( waypointDelay[route.i] )
                already_paused_at_this_waypoint=True # just pause once during this waypoint
                logging.debug('{0}: done! resuming previous velocity, spd_mps_i={1}, at route.i={2}'.format(behName,spd_mps_i,route.i))
                self.qBehavior.put( [ bId , steer , spd_mps_i, 0.0, 0.0, 0.0 ] ) # [ bId , steer , speed, pitch, v_z, progress ]
            
            # --------------------------------------------------------------
            # check how close vehicle is to the next waypoint
            dist_XY = np.sqrt( pow(dX,2) + pow(dY,2) ) # (m) 2D distance between next waypoint and vehicle
            # if vehicle is close, trigger the increment to the next waypoint
            if (dist_XY < 2*veh.L_char):
                route.i = route.i + 1 # increment waypoint index
                already_paused_at_this_waypoint=False # reset waypointDelay flag for this new waypoint
                if (debug>0): logging.debug('{0:s}'.format(behName))
                if (debug>0): logging.debug('{0:s}, distance threshold reached - heading to the next waypoint! route.i={1}, route.lap={2}'.format(behName, route.i, route.lap))
                if (debug>0): logging.debug('{0:s}'.format(behName))
                
                # if you're not doing laps and want to end at endingWaypoint:
                if ('endingWaypoint' in routeCfg) and (routeCfg['endingWaypoint'] != -1) and (route.i>=routeCfg['endingWaypoint']):
                        atEndOfRoute = True
                        if routeCfg['endOption'] == 'stop':
                            spd_mps_i = 0
                            if (debug>0): logging.debug('{0:s}, reached endingWaypoint - STOPing with v=0!'.format(behName))
                            if (debug>0): logging.debug('{0:s}, final waypoint: {1}; lap num: {2} '.format(behName, route.i, route.lap))
                            veh.qRoute.put( [route.i, route.lap, spd_mps_i] ) # add message for gather_outputs() in myVehicle.py
                
                # if there are N waypoints, increment lap counter when route.i>=N
                # b/c python is 0-based (if route.i=20 and max waypoints is 19,
                # you're at the last waypoint so increment lap counter at route.i=20)
                if (route.i>=route.N): # routes and waypoints are 0-based so the i'th waypoint is 1 less in the route definition file
                    # increment lap counter
                    route.lap = route.lap + 1
                    route.i   = routeCfg['lapRestartWaypoint'] # steer toward lapRestartWaypoint
                    if (debug>0): logging.debug('{0:s}, end of waypoints - starting lap [{1}] and heading toward waypoint {2}'.format(behName, route.lap, route.i))
                    veh.qRoute.put( [route.i, route.lap, spd_mps_i] ) # add message for gather_outputs() in myVehicle.py
                    
                    # upon reaching last waypoint, continue at first waypoint or stop?
                    if (route.lap>=route.nLapsMax):
                        atEndOfLaps = True
                        if routeCfg['endOption'] == 'stop':
                            spd_mps_i = 0
                            if (debug>0): logging.debug('{0:s}, end of waypoints and all laps - STOP with v=0!'.format(behName))
                            if (debug>0): logging.debug('{0:s}, final number of laps: '.format(behName, route.lap-1))
                            veh.qRoute.put( [route.i, route.lap, spd_mps_i] ) # add message for gather_outputs() in myVehicle.py
                veh.qRoute.put( [route.i, route.lap, spd_mps_i] ) # add message for gather_outputs() in myVehicle.py; status update only for monitoring
                
                # recompute dist_XY_start at new waypoint once route.i is determined
                dX            = route.X[ route.i ] - veh.pos.X # (m) X-component of vector pointing from veh to next waypoint (tip-tail)
                dY            = route.Y[ route.i ] - veh.pos.Y # (m) Y-component of veh-to-next-waypoint vector (tip - tail)
                dist_XY_start = np.sqrt( pow(dX,2) + pow(dY,2) ) # (m) 2D distance between next waypoint and vehicle
            
            if (debug>1): logging.debug('{0:s}, bId={1:d}, pri={2:d}: steer={3:f}(rad), spd={4:f}(m/s)'.format(behName, bId, priority, steer, spd_mps_i))
            
            # enable and update with these cmds: [ bId , steer , speed, pitch, progress ]
            progressDist = (dist_XY_start-dist_XY) / dist_XY_start # [0 1] progress on distance covered since starting this waypoint
            self.qBehavior.put( [ bId , steer , spd_mps_i, 0.0, 0.0, progressDist ] ) # [ bId , steer , speed, pitch, v_z, progress ]
        
        else: # we're at the end of the route - stop navigating
            self.qBehavior.put( [ bId , 0 ] ) # disable this behavior with [bId, 0]
            dt = 1 # (s) go back to slower polling while no longer computing a dynamic steering input
            if (debug>0): logging.debug('{0:s}, disabling bId={1:d}'.format(behName, bId, priority))
            
    logging.debug('[{0}]: bId={1} [{2}], exiting.'.format( datetime.now().strftime('%c') ,bId,behName))







# --------------------------------------------------------------
#                  behavior: goToGate()
# --------------------------------------------------------------
# periodic turn: steer toward the next gate; if out of perpendicular gate path, steer towards the far gate post
def goToGate(self,bId,behName,priority,veh,debug):
    logging.debug('\t\t[{0}]: bId={1} [{2}], priority={3}'.format( datetime.now().strftime('%c') ,bId,behName,priority))
    e=veh.e
    gates         = self.gates.gates # class with: gates.gates, gates.firstGate, gates.lap
    lap           = self.gates.lap
    firstGate     = self.gates.firstGate
    randFlag      = veh.randFlag # (0/1) use random numbers?
    
    steer_max = 35*np.pi/180.0 # (rad) set lock-to-lock max steer limit of 35deg, from ME620 Adv. Veh. Dyn. (~0.6rad)
    k_steer = 1.5 # steer gain between 1 and 1.5 is good; tuned in Simulink, ME620 Advanced Vehicle Dynamics
    
    dt = 0.2 # (s) gate steering update period
    T_console_update = 3.0 # (s)
    N_console_update = T_console_update/dt # integer number of e.wait(dt)'s to skip before providing a console update
            
    current_gate = self.gates.current_gate # gate names are keys in the config file; strings like 'gate_1' or whatever is in the .cfg file
    last_gate = current_gate
    #randNum = randFlag*random.uniform(0.0,+1.0)
    
    #logging.debug('gate={0}'.format(gates))
    A = np.array([ gates[ current_gate ]['ptA_X'], gates[ current_gate ]['ptA_Y'] ]) #  (m) 2D gate calculation only; RHR from post A to post B with +Z down defines which side is (+) side of gate
    B = np.array([ gates[ current_gate ]['ptB_X'], gates[ current_gate ]['ptB_Y'] ]) #  (m) post B of current gate
    P = np.array([ veh.pos.X, veh.pos.Y ]) #  (m) 2D gate calculation only
    (s0,L_s0,psi_to_gate,dist_XY,side) = compute_perpendicular_angle_to_line_in_space(A,B,P)
    dist_XY_start = dist_XY # (m) 2D distance between next gate and vehicle
    
    loopCnt=0
    logging.debug('{0}: entering loop starting at gate [{1}]'.format(behName,current_gate))
    while e.is_set() is False:
        
        e.wait(dt) # (sec) behavior is dormant for this duration; e.wait() is an interruptable sleep() with independent timers despite using the same event, e
        
        loopCnt = loopCnt+1
        
        if (loopCnt%N_console_update)==0: # modulo test is True every N'th: test = ( (cnt%N)==0 )
            logging.debug('{0}: time-based update, current gate=[{1}]'.format(behName,current_gate))
        # --------------------------------------------------------------
        # steer toward the gate - turn which way and how much? (based on 2D distance only)
        # see: Compere_handwritten_notes_shortest_perpendicular_distance_from_point_to_line_in_space.2020.05.26.pdf
        
        # compute s0 perpendicular to gate line from A to B; this determines if vehicle
        # is to the left of the gate (pt A), in the gate, or to the right of the gate (pt B)
        P = np.array([ veh.pos.X, veh.pos.Y ]) #  (m) 2D gate calculation only
        
        (s0,L_s0,psi_to_gate,dist_XY,side) = compute_perpendicular_angle_to_line_in_space(A,B,P)
        #logging.debug('[{0}]: s0={1}, L_s0={2}, psi_to_gate={3}, dist_XY={4}, side={5}'.format( datetime.now().strftime('%c') ,s0,L_s0,psi_to_gate,dist_XY,side))
        
        psi_veh = veh.pos.psi         # (rad) current vehicle heading, which could be large from multiple turns (i.e. >> +/- 2pi)
        
        psi_err_raw = psi_to_gate - psi_veh # (rad) heading error; this could be a large number, like +/- 100, way beyond +/- pi
        
        # bound heading error, psi_err_raw, such that psi_err is on [-pi  +pi]
        # credit: Mike Fair, Nov 2004
        spsi = sin(psi_err_raw)
        cpsi = cos(psi_err_raw)
        # psi_err is in body-fixed frame on inteval [-pi +pi] - this essentially says, turn left or turn right
        psi_err = atan2(spsi,cpsi) # (rad) heading error w.r.t. vehicle x-axis, atan2() returns [-pi  +pi]
        
        steer_raw = k_steer*psi_err # (rad) steering gain for path following (simple pursuit control law w/o lateral error term)
        steer = np.clip( steer_raw , -steer_max, +steer_max) # (rad) impose max steer angle, clip is numpy's saturation function
        #logging.debug('[{0}]: steer_max={1}, steer_raw={2}, steer={3}'.format( datetime.now().strftime('%c') ,steer_max,steer_raw,steer))
        
        
        # --------------------------------------------------------------
        # if vehicle has reached current gate, trigger the increment to the next gate
        if (dist_XY <= 1.0*veh.L_char):
            if (debug>0): logging.debug('{0:s}'.format(behName))
            if (debug>0): logging.debug('{0:s}, reached current gate=[{1}]'.format(behName, current_gate))
            if (debug>0): logging.debug('{0:s}'.format(behName))
            
            last_gate    = current_gate # capture current gate num before it's reassigned
            current_gate = gates[ current_gate ]['nextGate'] # figure out where to go next based on what's in the current gate's 'nextGate' dictionary entry (this is assigned in the config file)
            #randNum = randFlag*random.uniform(0.0,+1.0) # update random amount to steer towards the center of the gate to prevent gathering near the closest ends of two gates
            
            if (debug>0): logging.debug('{0:s}, headed to next gate=[{1}]'.format(behName, current_gate))
            A = np.array([ gates[ current_gate ]['ptA_X'], gates[ current_gate ]['ptA_Y'] ]) #  (m) 2D gate calculation only; RHR from post A to post B with +Z down defines which side is (+) side of gate
            B = np.array([ gates[ current_gate ]['ptB_X'], gates[ current_gate ]['ptB_Y'] ]) #  (m) post B of current gate
            dist_XY_start = dist_XY # (m) 2D distance between next gate and vehicle
            
            if (current_gate == firstGate): # increment lap counter; these are both strings
                lap = lap + 1
            
            veh.qGate.put( [current_gate, lap, veh.vel.spd_mps] ) # add message for gather_outputs() in myVehicle.py; status update only for monitoring
                
            if (debug>1): logging.debug('{0:s}, bId={1:d}, pri={2:d}: steer={3:f}(rad), spd={4:f}(m/s)'.format(behName, bId, priority, steer, veh.vel.spd_mps))
            
        progressDist = (dist_XY_start-dist_XY) / dist_XY_start # [0 1] progress on distance covered since starting this waypoint
        # enable and update with these cmds: [ bId , steer , speed, pitch, v_z, progress ] # veh.vel.spd_mps
        self.qBehavior.put( [ bId , steer , veh.vel.spd_mps, 0.0, 0.0, progressDist ] ) # assign speed equal to current speed (no change to speed command with gates)
            
    logging.debug('[{0}]: bId={1} [{2}], exiting.'.format( datetime.now().strftime('%c') ,bId,behName))
    
    
    
    
    
    
    
    
    
    
    
# --------------------------------------------------------------
#                  behavior: goToPoint()
# --------------------------------------------------------------
# go to point : steer toward the next point
# mission-enabled?: Yes, this is the first mission-enabled behavior
def goToPoint(self,bId,behName,priority,veh,debug):
    
    logging.debug('\t\t[{0}]: bId={1} [{2}], priority={3}'.format( datetime.now().strftime('%c') ,bId,behName,priority))
    e=veh.e
    points          = self.points.points # class with: points.points, points.IC_pt, points.lap
    lap             = self.points.lap
    IC_pt           = self.points.IC_pt
    randFlag        = veh.randFlag # (0/1) use random numbers?
    runStateLast    = veh.runState[0]
    runStateChanged = False # make local decisions on runState changes
    
    steer_max = 35*np.pi/180.0 # (rad) set lock-to-lock max steer limit of 35deg, from ME620 Adv. Veh. Dyn. (~0.6rad)
    k_steer = 1.5 # steer gain between 1 and 1.5 is good; tuned in Simulink, ME620 Advanced Vehicle Dynamics
    
    dt = 0.2 # (s) steering update period
    T_console_update = 3.0 # (s)
    N_console_update = T_console_update/dt # integer number of e.wait(dt)'s to skip before providing a console update
    
    point_cnt      = 0
    current_point  = self.points.current_point # points are 1-based, by convention only; enforce 1-based points in config file also
    last_point     = current_point
    rThreshold     = 1.0*veh.L_char # (m) radius for triggering point-reached
    Z_BL           = 0.5 # (m) boundary layer in inertial Z direction for tanh() function to achieve commanded vertical setpoint: u_z = zDotMax*tanh( dZ/Z_BL )
    Z_threshold    = 0.01 # (m) elevation threshold for triggering point-reached in Z-direction
    zDotMax        = 1.0 # (m/s) body-fixed max elevation rate for elevation changes (e.g. during take-off and landings)
    #randNum = randFlag*random.uniform(0.0,+1.0)
    
    try:
        velCmd_xy      = points[ current_point ]['vel'] # (m/s) horizontal speed command
        dX             = points[ current_point ]['pt_X'] - veh.pos.X  # (m) X-component of vector pointing from veh to next waypoint (tip-tail)
        dY             = points[ current_point ]['pt_Y'] - veh.pos.Y  # (m) Y-component of veh-to-next-waypoint vector (tip - tail)
        dZ             = points[ current_point ]['pt_Z'] - veh.pos.Z  # (m) Z-component of veh-to-next-waypoint vector (tip - tail)
        dist_XYZ_start = np.sqrt( pow(dX,2) + pow(dY,2) + pow(dZ,2) ) # (m) 3D distance between next waypoint location and vehicle's current position
    except KeyError:
        for i in range(5): logging.debug(' ')
        logging.debug('\t\terror: could not find initial point! current_point={0}'.format(current_point))
        logging.debug('\t\texiting this thread and all threads by running:   veh.exit()')
        #veh.e.set() # exit all threads
        veh.exit()
        
    
    # discriminate between a sequence of points and usingMissionCommands
    goToPointActive=True # prioritize() will always see goToPoint() with hand raised
    if (veh.usingMissionCommands==True):
        goToPointActive=False # only raise hand when missionStateSequencer tells goToPoint() when to activate
    
    loopCnt=0
    logging.debug('{0}: entering loop starting at point [{1}], goToPointActive={2}'.format(behName,current_point,goToPointActive))
    while e.is_set() is False:
        
        e.wait(dt) # (sec) behavior is dormant for this duration; e.wait() is an interruptable sleep() with independent timers despite using the same event, e
        
        loopCnt = loopCnt+1
        
        # use local var to capture veh.runState change that occurred (only) in main_veh_model.py | main()
        if (runStateLast is not veh.runState[0]):
            runStateChanged = True
            runStateLast    = veh.runState[0]
        else:
            runStateChanged = False
        
        if (loopCnt%N_console_update)==0: # modulo test is True every N'th: test = ( (cnt%N)==0 )
            logging.debug('{0}: time-based update, current point=[{1}]'.format(behName,current_point))
        
        
        
        # if there are any missionCommands for this particular vehicle, then listen to missionStateSequencer()
        # otherwise, march through the points
        if (veh.usingMissionCommands==True) and (len(veh.goToPointCmd)>0): # flag for behaviors to activate from missionStateSequencer() or 'nextPoint' or 'nextGate'?
            # check for new commands from missionStateSequencer()
            goToPointActive = True          # (0/1) is goToPoint behavior active for prioritize() to select?
            cmdMsg          = veh.goToPointCmd.pop() # cmdMsg={'pt':pt, 'vel':vel, 'rThresh':rThresh, 'arrivalType':arrivalType}
            current_point   = cmdMsg['pt']  # go to this point now (regardless of approach to previous point)
            velCmd_xy       = cmdMsg['vel'] # (m/s) go this fast
            zDotMax         = cmdMsg['riseRate'] # (m/s) rise rate for vertical maneuvers to hit an elevation setpoint
            dX              = points[ current_point ]['pt_X'] - veh.pos.X  # (m) X-component of vector pointing from veh to next waypoint (tip-tail)
            dY              = points[ current_point ]['pt_Y'] - veh.pos.Y  # (m) Y-component of veh-to-next-waypoint vector (tip - tail)
            dZ              = points[ current_point ]['pt_Z'] - veh.pos.Z  # (m) set IC on Z for this point
            dist_XYZ_start  = np.sqrt( pow(dX,2) + pow(dY,2) + pow(dZ,2) ) # (m) 3D distance between next waypoint location and vehicle's current position
            
            logging.debug('{0}: just received missionStateSequencer command! new current point=[{1}], goToPointActive={2}'.format(behName,current_point,goToPointActive))
            logging.debug('{0}: resetting distance to current_point={1}! dist_XYZ_start=[{2}](m), dZ=[{3}](m)'.format(behName,current_point,dist_XYZ_start,dZ))
        
        #  if runState == 'set', but just once upon change
        if (veh.runState[0]==2) and (runStateChanged==True):
            # this will set the progressDist correctly at first waypoint
            velCmd_xy       = points[ current_point ]['vel'] # (m/s)
            dX              = points[ current_point ]['pt_X'] - veh.pos.X  # (m) X-component of vector pointing from veh to next waypoint (tip-tail)
            dY              = points[ current_point ]['pt_Y'] - veh.pos.Y  # (m) Y-component of veh-to-next-waypoint vector (tip - tail)
            dZ              = points[ current_point ]['pt_Z'] - veh.pos.Z  # (m) Z-component of veh-to-next-waypoint vector (tip - tail)
            dist_XYZ_start  = np.sqrt( pow(dX,2) + pow(dY,2) + pow(dZ,2) ) # (m) 3D distance between next waypoint location and vehicle's current position
            logging.debug('{0}: resetting distance to current_point={1}! dist_XYZ_start=[{2}](m)'.format(behName,current_point,dist_XYZ_start))
            runStateChanged=False
        
        # --------------------------------------------------------------
        
        # steer toward the next waypoint - turn which way and how much? (based on 2D distance only)
        dX              = points[ current_point ]['pt_X'] - veh.pos.X  # (m) X-component of vector pointing from veh to next waypoint (tip-tail)
        dY              = points[ current_point ]['pt_Y'] - veh.pos.Y  # (m) Y-component of veh-to-next-waypoint vector (tip - tail)
        dZ              = points[ current_point ]['pt_Z'] - veh.pos.Z  # (m) Z-component of veh-to-next-waypoint vector (tip - tail)
        dist_XY         = np.sqrt( pow(dX,2) + pow(dY,2) )             # (m) 2D distance between next waypoint and vehicle
        dist_XYZ        = np.sqrt( pow(dX,2) + pow(dY,2) + pow(dZ,2) ) # (m) 3D distance between next waypoint and vehicle
        velCmd_z        = zDotMax*tanh( dZ/Z_BL ) # (m/s) vel command for changing elevations is a saturation function tanh() with a boundary layer
        
        if (dist_XYZ_start>0):
            self.pctCompleteGoToPoint = (dist_XYZ_start-dist_XYZ) / dist_XYZ_start # [0 1] progress on distance covered since starting this waypoint
        else:
            self.pctCompleteGoToPoint = 0.0
        
        
        psi_to_point = atan2( dY , dX ) # (rad) 4-quadrant arc-tangent provides orientation from veh to next waypoint
        psi_veh = veh.pos.psi         # (rad) current vehicle heading, which could be large from multiple turns (i.e. >> +/- 2pi)
        psi_err_raw = psi_to_point - psi_veh # (rad) heading error; this could be a large number, like +/- 100, way beyond +/- pi
        
        # bound heading error, psi_err_raw, such that psi_err is on [-pi  +pi]
        # credit: Mike Fair, Nov 2004
        spsi = sin(psi_err_raw)
        cpsi = cos(psi_err_raw)
        # psi_err is in body-fixed frame on inteval [-pi +pi] - this essentially says, turn left or turn right
        psi_err = atan2(spsi,cpsi) # (rad) heading error w.r.t. vehicle x-axis, atan2() returns [-pi  +pi]
        
        steer_raw = k_steer*psi_err # (rad) steering gain for path following (simple pursuit control law w/o lateral error term)
        steer = np.clip( steer_raw , -steer_max, +steer_max) # (rad) impose max steer angle, clip is numpy's saturation function
        #logging.debug('[{0}]: steer_max={1}, steer_raw={2}, steer={3}'.format( datetime.now().strftime('%c') ,steer_max,steer_raw,steer))
        
        if (dist_XY <= rThreshold):
            logging.debug('{0:s}, reached XY location for points=[{1}]! setting horizontal velCmd_xy=0'.format(behName, current_point))
            velCmd_xy = 0.0 # (m/s) if you're at the (lat,lon) coordinates, then stop (assuming a multi-rotor or VTOL that *can* stop in mid-air)
        
        # --------------------------------------------------------------
        # if vehicle has reached current point in XY and Z directions, trigger the increment to the next point            
        if (dist_XY <= rThreshold) and (abs(dZ) < Z_threshold) and (veh.runState[0]==3) and (goToPointActive==True):
            if (debug>0): logging.debug('{0:s}'.format(behName))
            if (debug>0): logging.debug('{0:s}, reached current point=[{1}]'.format(behName, current_point))
            
            last_point = current_point # capture current point num before it's reassigned
            
            # reached the current_point
            if (veh.usingMissionCommands==True):
                goToPointActive = False   # (0/1) is goToPoint behavior active for prioritize() to select?
                veh.goToPointDone.set()   # tell missionStateSequencer() the point was reached
                
                # disable this behavior; "put your hand down" so prioritize() no longer selects this behavior; note: make sure there is a lower priority behvaior still enabled and selectable like 'hover' or 'wander'
                self.qBehavior.put( [ bId , 0 ] ) # disable with [bId, 0]
                logging.debug('{0:s}, reached point=[{1}]. disabling goToPoint()'.format(behName, current_point))
            else:
                # proceed to 'nextPoint'
                current_point = points[ current_point ]['nextPoint'] # figure out where to go next based on what's in the current point's 'nextPoint' dictionary entry (this is assigned in the config file)
                logging.debug('{0:s}, headed to next point=[{1}]'.format(behName, current_point))
                velCmd_xy       = points[ current_point ]['vel'] # (m/s) retrieve velocity associated wtih this new waypoint
                #velCmd_z        = 0.0 # (m/s) elevation rate, positive or negative
                dX              = points[ current_point ]['pt_X'] - veh.pos.X  # (m) X-component of vector pointing from veh to next waypoint (tip-tail)
                dY              = points[ current_point ]['pt_Y'] - veh.pos.Y  # (m) Y-component of veh-to-next-waypoint vector (tip - tail)
                dZ              = points[ current_point ]['pt_Z'] - veh.pos.Z  # (m) Z-component of veh-to-next-waypoint vector (tip - tail)
                dist_XYZ_start  = np.sqrt( pow(dX,2) + pow(dY,2) + pow(dZ,2) ) # (m) 3D distance between next waypoint location and vehicle's current position
            
            if (current_point==IC_pt): # increment lap counter
                lap = lap + 1
            
            if (debug>1): logging.debug('{0:s}, bId={1:d}, pri={2:d}: steer={3:f}(rad), spd={4:f}(m/s)'.format(behName, bId, priority, steer, veh.vel.spd_mps))
        
        veh.qPoint.put( [current_point, lap, veh.vel.spd_mps] ) # add message for gather_outputs() in myVehicle.py; status update only for monitoring
        
        # enable and update with these cmds: [ bId , steer , speed, pitch, v_z, progress ] # veh.vel.spd_mps
        if (goToPointActive==True):
            self.qBehavior.put( [ bId , steer , velCmd_xy, 0.0, velCmd_z, self.pctCompleteGoToPoint ] ) # assign speed equal to current speed (no change to speed command with points)
        
        
    logging.debug('[{0}]: bId={1} [{2}], exiting.'.format( datetime.now().strftime('%c') ,bId,behName))