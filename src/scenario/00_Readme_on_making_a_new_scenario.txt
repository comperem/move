Steps to making a new multi-vehicle scenario in MoVE:

(1.0.0)     make sure you know what you want in the scenario by answering the following
            questions:
    (1.1)   how many simulated vehicles should be in the scenario?
    (1.2)   how many live-GPS-follower vehicles should be in the scenario?
    (1.3)   where should this scenario be located? (latitude and longitude)
    (1.4)   what types of behaviors should each of the vehicles exhibit?
    (1.5)   what are the names of all the vehicles? (Joe, UAV_1, myUGV, and so on)
    (1.6)   do initial conditions need to be specified for each vehicle individually?
    (1.7)   what are maximum speeds and characteristic length of each vehicle?
    (1.8)   are there route waypoints? If so, review making routes described in:
            00_Readme_on_making_a_new_route.txt
    (1.9)   if vehicles need to navigate a route but there are no specific waypoints
            then gates may be useful. Gates are easier to use than routes and are
            specified by 2 points (side A and B) with their latitude and longitude,
    
(2.0.0)     to create a new scenario configuration, .cfg, file, copy one of the existing
            examples in ./scenario such as default.cfg
            and designate:
    (2.1)   number of native MoVE simulated vehicles:   nVeh_builtin  = 3
    (2.2)   number of live-GPS-follower vehicles    :   nVeh_live_gps = 0
    (2.3)   scenario origin latitude and longitude  :
                lat_origin =  29.190337 # (decimal deg) Embry-Riddle soccer fields
                lon_origin = -81.045672 # (decimal deg)
    (2.4)   vehicle characteristic length           :   L_char = 2.0 # (m)
    (2.5)   vehicle maximum speed                   :   v_max = 6.7 # (m/s) max spd
    
    (2.6)   specify default vehicle type in [veh_details]
    (2.7)   specify individual vehicle properties: names, vehType, vehSubType, initial conditions
    
    (2.8)   determine default behaviors for all and specific behaviors for individuals:
            (i) the recommended approach is to disable all behaviors by default in
                the [veh_beh] section, except, of course the behaviors all should use
            (ii) enable specific behaviors for specific vehicles in [veh_behaviors_individual]
            To disable a behavior, assign it's priority to 0.
            Behavior priorities can be along any scale, however the behavior scheduler
            sorts on integer behavior priorities.
    
    (2.9)   if the scenario's goal is to specify group motion for multiple vehicles
            in a simple way, then [gates] may be a good solution. Gates are specified
            by two locations. Point A is the left side of the gate and Point B is the right:
                Point A is specified first with a (lat,lon) pair.
                Point B is specified second with another (lat,lon) pair to define a gate,
            or passage way through which all vehicles will proceed.
            Specifying [gates] is necessary but not sufficient.
            Do not forget to enable the goToGate() behavior with sufficient priority
            in the [veh_behaviors_global] or [veh_behaviors_individual] sections.
    (2.10)  if [gates] were specified (and goToGate() behavior enabled), each vehicle
            will need an initial gate assignment specified in [veh_gate_assignments].
            When each vehicle reaches this initial gate, the vehicle will steer
            toward the next gate specified in the 'nextGate' for that gate.
            If no gates are specified, then make sure the goToGate() behavior is
            disabled by setting it's priority to zero.
    
    (2.11)  as an alternative to [gates], if the scenario has specific (lat/lon) waypoints
            that define a path, or route, then configure [veh_route_assignments]
            (2.11.1)    first make sure the routes file runs and generates no errors.
                        See ./routes/00_Readme_on_making_a_new_route.txt for more.
            (2.11.2)    specify the route file (that contains the waypoint defintion)
                        is properly designated in [veh_route_assignments]
    
    (2.12)  as an aid to debugging, a command line tool that is called by
            MoVE Core, the vehicle models and the map visualizataion is:
                readConfigFile.py
            This file is symbolically linked in each sub-folder within the MoVE
            directory structure. The links are a Linux/Unix file system feature
            that allows one file to be referenced from the other locations.
            The original from which others are linked is in ./scenario
    
            Edit readConfigFile.py (yes, at the bottom, in __main__) with the new
            config file you've just created. Run readConfigFile.py at the command
            line until there are no errors like this:
                python3 readConfigFile.py
            This file will parse the config file (the .cfg file) you've just created
            along with the waypoint routes file you've also (optionally) created.
            Get this to run without error before proceeding.
            
(3.0.0) Runtime programs: dashboard and map display
    (3.1)   There are two command-line programs that deliver dyanamic content
            to a web browser such as Chrome or Chromium.
            
            Both command line progams need an open browser to interact with.
            
            Open two browser windows, one on the left side, one on the right.
            
            Open two command-line terminal windows, one below each of the browsers.
            Change directories to ./v1.07_dev/data_displays
            
            (3.1.1) on the left terminal, run the dashboard display with:
                        ./run_move_dashboard.sh
                    This should populate the left-side browser with a ticking clock
                    in the green bar, some buttons, a console output window,
                    and a table with vehicle information at the bottom.
            (3.1.2) on the right terminal, the command needs more input than the
                    dashboard display so it knows what configuration file to use.
                    Run with:
                        ./run_move_map_client.sh -f ../scenario/default.cfg
                    Change the default.cfg file to the new config file you've just
                    created and tested with 'python3 readConfigFile.py'
                    This will bring up a live mapping display of all vehicles.


Still to complete:
(4.0.0) Features specific to behaviors:
    
    stayInBounds() behavior uses these in config file section: [scenario]
        boundary_Xmax = +100.0 # (m) UTM coordinates w.r.t. origin
        boundary_Xmin = -100.0 # (m)
        boundary_Ymax = +100.0 # (m)
        boundary_Ymin = -100.0 # (m)

---
Marc Compere, comperem@gmail.com
created : 03 Aug 2020
modified: 03 Aug 2020
