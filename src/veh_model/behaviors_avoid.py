# behaviors:
#       stayInBounds()
#       avoidBySteer()
#       avoidByBrake()
#       avoidComputeOtherVehPositionInLocalFrame()
#       detectAndReport()
#
# this is an include in the Behaviors class, in ./behaviors.py
#
# Marc Compere, comperem@erau.edu
# created : 18 Feb 2024
# modified: 18 Feb 2024
#
# --------------------------------------------------------------
# Copyright 2018 - 2024 Marc Compere
#
# This file is part of the Mobility Virtual Environment (MoVE).
# MoVE is open source software licensed under the GNU GPLv3.
#
# MoVE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3
# as published by the Free Software Foundation.
#
# MoVE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License version 3 for more details.
#
# A copy of the GNU General Public License is included with MoVE
# in the COPYING file, or see <https://www.gnu.org/licenses/>.
# --------------------------------------------------------------

import random
import time
from datetime import datetime
import queue
import numpy as np
import logging
from math import sin,cos,atan2,pi,copysign,sqrt,tanh
import code # drop into a python interpreter to debug using: code.interact(local=locals())
from pprint import pformat # pretty print v2v structure




# --------------------------------------------------------------
#                  behavior: stayInBounds()
# --------------------------------------------------------------
# stayInBounds: if vehicle exceeds bondaries, turn until back inside
def stayInBounds(self,bId,behName,priority,veh,debug):
    logging.debug('\t\t[{0}]: bId={1} [{2}], priority={3}'.format( datetime.now().strftime('%c') ,bId,behName,priority))
    e=veh.e
    geom=veh.geom
    geom_center = np.array([ geom.cg_X , geom.cg_Y , 0.0 ])
    
    steer_max = 35*np.pi/180.0 # (rad) set lock-to-lock max steer limit of 35deg, from ME620 Adv. Veh. Dyn. (~0.6rad)
    k_steer = 1.5 # pure-pursuit steering gain, chosen in Simulink, ME620 Advanced Vehicle Dynamics
    
    dt = 1 # (s) out-of-bounds evaluation period
    spd = 2; # (m/s) speed while turning
    remainingTime = -1 # (s)
    while e.is_set() is False:
        
        e.wait(dt) # (sec) behavior is dormant for this duration; e.wait() is an interruptable sleep() with independent timers despite using the same event, e
        
        # are we out of bounds?
        Xout = (veh.pos.X > geom.Xmax) or (veh.pos.X < geom.Xmin) # is veh out of bounds in X-dir?
        Yout = (veh.pos.Y > geom.Ymax) or (veh.pos.Y < geom.Ymin) # is veh out of bounds in Y-dir?
        
        # while out-of-bounds, steer back towards the rectangular geometry's centroid
        if (Xout==True) or (Yout==True) or (remainingTime>0):
            # yep, we're out of bounds... or we've been out of bounds and we're still returning to boundary centroid
            if (remainingTime<=0):
                getBackInThereTimer = 5 # (s) duration for get-back-in-there behavior to execute once it's gone out-of-bounds
                tStartForTimer = time.time() # (s) seconds since the epoch (a giant number)
                remainingTime = tStartForTimer - time.time() + getBackInThereTimer # (s) duration to keep steering back towards the interior
            else:
                remainingTime = tStartForTimer - time.time() + getBackInThereTimer # (s) duration to keep steering back towards the interior
            
            #determine which way to turn
            #dt = veh.cInt # provide higher frequency dynamic steering input matching veh.pos update rate
            
            # compute steer angle to bring the vehicle back in-bounds, which means
            # inside the rectangle defined by geom in MyVehicle class init: [Xmin Xmax Ymin Ymax])
            #
            # for steering control law diagram and equations
            # see: p.4 of 03_Compere_handwritten_notes_Path_Nav_Control_Lecture_1_of_2_14_Oct_2016.pdf
            veh_g = np.array([ veh.pos.X, veh.pos.Y, veh.pos.Z ])
            r_gc  = geom_center - veh_g # vector from veh cg to geometry gentroid; tip minus tail
            
            psi_c = atan2( r_gc[1] , r_gc[0] ) # (rad) heading angle from vehicle to geometry centroid
            psi_veh = veh.pos.psi                # (rad) current vehicle heading, which could be large from multiple turns (i.e. >> +/- 2pi)
            
            psi_err_raw = psi_c - psi_veh # (rad) heading error; this could be a large number, way beyond +/- pi
            
            # unwrap such that psi_err is on [-pi  +pi]
            # credit: Mike Fair, Nov 2004
            spsi = sin(psi_err_raw)
            cpsi = cos(psi_err_raw)
            # psi_err is in body-fixed frame on inteval [-pi +pi] - this essentially says, turn left or turn right
            psi_err = atan2(spsi,cpsi) # (rad) heading error w.r.t. vehicle x-axis, atan2() returns [-pi  +pi]
            
            steer_raw = k_steer*psi_err # (rad) steering gain for path following (even tho this is a stationary point)
            steer = np.clip( steer_raw , -steer_max, +steer_max) # (rad) impose max steer angle, clip is numpy's saturation function
            #logging.debug('[{0}]: steer_max={1}, steer_raw={2}, steer={3}'.format( datetime.now().strftime('%c') ,steer_max,steer_raw,steer))
            
            if (debug>1): logging.debug('{0:s}, bId={1:d}, pri={2:d}: steer={3:f}'.format(behName, bId, priority, steer))
            
            # enable with these cmds: [ bId , steer , speed, pitch, progress ]
            self.qBehavior.put( [ bId , steer , spd, 0.0, 0.0, 0.0 ] ) # [ bId , steer , speed, pitch, v_z, progress ]
            
        else: # we're in-bounds again
            self.qBehavior.put( [ bId , 0 ] ) # disable this behavior with [bId, 0]
            dt = 1 # (s) go back to slower polling while no longer computing a dynamic steering input
            
    logging.debug('[{0}]: bId={1} [{2}], exiting.'.format( datetime.now().strftime('%c') ,bId,behName))



# --------------------------------------------------------------
#                  behavior: avoidBySteer()
# --------------------------------------------------------------
# queue-driven loop to parse incoming vehicle message, do the math, and turn the other way (mainly for aerial vehicles)
def avoidBySteer(self,bId,behName,priority,veh,debug):
    e=veh.e
    randFlag=veh.randFlag # (0/1) use random numbers?
    
    logging.debug('\t\t[{0}]: bId={1} [{2}], priority={3}, vid={4}' \
           .format( datetime.now().strftime('%c') , bId, behName, priority, veh.vid))
    dt = 0.5 # (s) periodically loop to check for exit Event, e.is_set()
    #dtAvoid = 5.0 + randFlag*random.uniform(1.0,3.0) # (s) avoid turn duration; smaller is better - if complete and still at-risk it will trigger again
    spd = 2.236 # (m/s) vehicle nominal wander speed (vid=101 should go faster at psi_ic=-0.4636)
    
    while e.is_set() is False:
        
        # clear the que - zoom through all avoidCmd messages that may have piled up
        dtAvoid=dt
        while not veh.qAvoid.empty(): # this empties the queue coming from move core's collision detection (it can send multiple times)
            # payload_i = { 'theOtherOne': vid_j, 'pos': vehData_j['pos'], 'vel': vehData_j['vel'],
            #               'dist_XY': warn_data['dist_XY'], 'distRate': warn_data['distRate'] } # j pos, j vel
            payload = veh.qAvoid.get() # qAvoid message is from veh_udp_io.py;  defaults: Queue.get(block=True, timeout=None)
            
            logging.debug( '\t\tavoid queue size: {0}'.format(veh.qAvoid.qsize()) )
            
            logging.debug('[{0}]: ---------------------------'.format( datetime.now().strftime('%c') ))
            logging.debug('[{0}]: bId={1}, vid={2}, veh pos={3}'.format( datetime.now().strftime('%c') , bId, veh.vid, veh.pos.__dict__))
            logging.debug('[{0}]: bId={1}, received payload={2}'.format( datetime.now().strftime('%c') , bId, payload))
            logging.debug('[{0}]: ---------------------------'.format( datetime.now().strftime('%c') ))
            
            # turn left or right to avoid the other vehicle
            d_y, d_x = self.avoidComputeOtherVehPositionInLocalFrame( veh, payload )
            #dtAvoid = (pi*veh.L_char) / (4*spd*abs(turnCmd)) # hold-time to achieve 45-deg yaw change
            
            evasiveTurnCmd = 0.4 + randFlag*random.uniform(0.0,0.3) # (rad) steer signal to avoid collision
                                 # todo: make this a function of speed, thresholding to below a max lateral acceleration
            
            # if the other vehicle is on my RIGHT, then STEER LEFT to avoid
            if d_y >=0.0:
                logging.debug('\tother vehicle detected on RIGHT! '.format(payload['theOtherOne']))
                turnCmd = -(evasiveTurnCmd) # (rad) steer signal turning left (+Z and +z are down, RHR)
                logging.debug('\t[{0}]: vid={1}, turning left, steer={2}(deg)'.format( datetime.now().strftime('%c') , veh.vid, turnCmd*180/3.14159))
            
            # if the other vehicle is on my LEFT, then STEER RIGHT to avoid
            if d_y < 0.0:
                logging.debug('\tother vehicle detected on LEFT! '.format(payload['theOtherOne']))
                turnCmd = +(evasiveTurnCmd) # (rad) steer signal turning right (+Z and +z are down, RHR)
                logging.debug('\t[{0}]: vid={1}, turning right, steer={2}(deg)'.format( datetime.now().strftime('%c') , veh.vid, turnCmd*180/3.14159))
            
            #pitch = randFlag*random.uniform(-1.0,+1.0) # uniformly distributed random number on [-1 +1]
            if (debug>=1): logging.debug('{0:s}, bId={1:d}, pri={2:d}: turnCmd={3:f} (deg)'.format(behName, bId, priority, turnCmd*180/3.14149))
            
            # enable with these cmds: [ bId , steer , speed, pitch, progress ]
            self.qBehavior.put( [ bId , turnCmd , spd, 0.0, 0.0, 0.0 ] ) # [ bId , steer , speed, pitch, v_z, progress ]
        
        # execute the last turn computed
        e.wait(dtAvoid) # (sec) duration for this maneuver; interruptable sleep()
        
        self.qBehavior.put( [ bId , 0 ] ) # disable with [bId, 0]
    
    logging.debug('[{0}]: bId={1} [{2}], exiting.'.format( datetime.now().strftime('%c') ,bId,behName))



# --------------------------------------------------------------
#                  behavior: avoidByBrake()
# --------------------------------------------------------------
# queue-driven loop to parse incoming vehicle message, do the math, and slow or stop to avoid hitting another (ground) vehicle on a similar path

# UNFINISHED - UNTESTED - UNFINISHED - UNTESTED
def avoidByBrake(self,bId,behName,priority,veh,debug):
    e=veh.e
    randFlag=veh.randFlag # (0/1) use random numbers?
    
    logging.debug('\t\t[{0}]: bId={1} [{2}], priority={3}, vid={4}' \
           .format( datetime.now().strftime('%c') , bId, behName, priority, veh.vid))
    dt = 0.5 # (s) periodically loop to check for exit Event, e.is_set()
    #dtAvoid = 5.0 + randFlag*random.uniform(1.0,3.0) # (s) avoid turn duration; smaller is better - if complete and still at-risk it will trigger again
    spd = 2.236 # (m/s) vehicle nominal wander speed (vid=101 should go faster at psi_ic=-0.4636)
    
    while e.is_set() is False:
        
        # clear the que - zoom through all avoidCmd messages that may have piled up
        dtAvoid=dt
        while not veh.qAvoid.empty():
            # payload_i = { 'theOtherOne': vid_j, 'pos': vehData_j['pos'], 'vel': vehData_j['vel'],
            #               'dist_XY': warn_data['dist_XY'], 'distRate': warn_data['distRate'] } # j pos, j vel
            payload = veh.qAvoid.get() # qAvoid message is from veh_udp_io.py;  defaults: Queue.get(block=True, timeout=None)
            
            logging.debug( '\t\tavoid queue size: {0}'.format(veh.qAvoid.qsize()) )
            
            logging.debug('[{0}]: ---------------------------'.format( datetime.now().strftime('%c') ))
            logging.debug('[{0}]: bId={1}, vid={2}, veh pos={3}'.format( datetime.now().strftime('%c') , bId, veh.vid, veh.pos.__dict__))
            logging.debug('[{0}]: bId={1}, received payload={2}'.format( datetime.now().strftime('%c') , bId, payload))
            logging.debug('[{0}]: ---------------------------'.format( datetime.now().strftime('%c') ))
            
            # turn left or right to avoid the other vehicle
            d_y, d_x = self.avoidComputeOtherVehPositionInLocalFrame( veh, payload )
            #dtAvoid = (pi*veh.L_char) / (4*spd*abs(turnCmd)) # hold-time to achieve 45-deg yaw change
            
            # if the other vehicle is in front, slow down
            # example payload={ 'theOtherOne': 100, 'pos': {'X': 30.16, 'Y': 87.31, 'Z': 0.0, 'psi': 0.0,
            #                   'lat': 29.19, 'lon': -81.04}, 'vel': {'Xd': 5.0, 'Yd': 0.0, 'Zd': 0.0,
            #                   'psiDot': 0.0, 'spd_mps': 5.0}, 'dist_XY': 47.48, 'distRate': -13.35}
            vel = payload['vel']
            spd_other = np.sqrt( pow(vel['Xd'],2) + pow(vel['Yd'],2) + pow(vel['Zd'],2) )
            if d_x >=0.0:
                logging.debug('other vehicle detected IN FRONT. Brake to avoid hitting! '.format(payload['theOtherOne']))
                spd = 0.8*spd_other
                logging.debug('[{0}]: vid={1}, turning left, spd={2}(m/s)'.format( datetime.now().strftime('%c') , veh.vid, spd))
            
            if (debug>1): logging.debug('{0:s}, bId={1:d}, pri={2:d}: spd={3:f} (m/s)'.format(behName, bId, priority, spd))
            
            # enable with these cmds: [ bId , steer , speed, pitch, v_z, progress ]
            self.qBehavior.put( [ bId , 0.0 , spd, 0.0, 0.0, 0.0 ] )
        
        # execute the last turn computed
        e.wait(dtAvoid) # (sec) duration to pitch; interruptable sleep()
        
        self.qBehavior.put( [ bId , 0 ] ) # disable with [bId, 0]
    
    logging.debug('[{0}]: bId={1} [{2}], exiting.'.format( datetime.now().strftime('%c') ,bId,behName))



# -----------------------------------------------------------------
def avoidComputeOtherVehPositionInLocalFrame( self, veh, payload ):
    # helper function for avoid() behavior
    #
    # this function runs when an avoidance message was received by this
    # vehicle's avoid() behavior
    #   pos is the i'th vehicle's position
    #   payload contains information (from core) about the other incoming vehicle
    randFlag = veh.randFlag
    
    pos_i = veh.pos # (m) structure created within this vehicle model, see myVehicle __init__ constructor near: self.pos = myClass()
    pos_j = payload['pos'] # (m) 'the other guy's position' is a dict in a payload from Move Core, all_vehicles.py, poll_state(), checkDistVel() and 'theOtherOne'
    
    #print('pos_i={0},{1},{2}'.format(pos_i.X,pos_i.Y,pos_i.Z))
    #print('pos_j={0}'.format(pos_j))
    # vector from i'th (me) vehicle to j'th vehicle (the other guy) - recall vectors: "tip minus tail"
    dX = pos_j['X'] - pos_i.X # (m) X_j - X_i   in inertial XYZ frame
    dY = pos_j['Y'] - pos_i.Y # (m) Y_j - Y_i   in inertial XYZ frame
    dZ = pos_j['Z'] - pos_i.Z # (m) Z_j - Z_i   in inertial XYZ frame
    
    psi_i = pos_i.psi # (rad) my heading w.r.t. +X-axis
    
    # is the other guy in front or behind, and on my right or my left?
    #
    # see: Compere_handwritten_notes_Avoidance_maneuver_coord_transformation_Aug_2018.pdf
    # neglecting altitude difference:
    # d_x - location of the other vehicle in my reference frame (fore-to-aft)
    #      if d_x > 0, the other vehicle is in front of me
    #      if d_x < 0, the other vehicle is behind me
    #
    # d_y - location of the other vehicle in my reference frame (left-to-right)
    #      if d_y > 0, the other vehicle is on my right (so turn left to avoid)
    #      if d_y < 0, the other vehicle is on my left (so turn right to avoid)
    d_x = +cos(psi_i)*dX + sin(psi_i)*dY # (m) body-fixed x-axis projection of [dX,dY,dZ] vector
    d_y = -sin(psi_i)*dX + cos(psi_i)*dY # (m) body-fixed y-axis projection of [dX,dY,dZ] vector
    
    logging.debug('[{0}]: vid={1}, d_x={2:0.1f}(m), d_y={3:0.1f}(m), psi_i={4:0.2f}'.format( datetime.now().strftime('%c') , veh.vid, d_x, d_y, psi_i))
    
    return d_y, d_x



# --------------------------------------------------------------
#                  behavior: detectAndReport()
# --------------------------------------------------------------
# detectAndReport: monitor for priximity to another vehicle and report last-seen time and location
def detectAndReport(self,bId,behName,priority,veh,debug):
    logging.debug('\t\t[{0}]: bId={1} [{2}], priority={3}'.format( datetime.now().strftime('%c') ,bId,behName,priority))
    e=veh.e
    #geom=veh.geom
    #geom_center = np.array([ geom.cg_X , geom.cg_Y , 0.0 ])
    
    #steer_max = 35*np.pi/180.0 # (rad) set lock-to-lock max steer limit of 35deg, from ME620 Adv. Veh. Dyn. (~0.6rad)
    #k_steer = 1.5 # tuned in Simulink, ME620 Advanced Vehicle Dynamics
    
    use_2d_dist=1 # (0/1) use 2D or 3D distance calculation for detection thresholding?
    dt = 1 # (s) proximity evaluation period
    while e.is_set() is False:
    
        e.wait(dt) # (sec) behavior is dormant for this duration; e.wait() is an interruptable sleep() with independent timers despite using the same event, e
        
        dist_XY = 1e10 # (m) large nunber to hinder detection unless computation indicates to the contrary
        logging.debug('detectAndReport(): qDetect.qsize()={0}'.format(veh.qDetect.qsize()))
        while not veh.qDetect.empty():
            # get latest and greatest position information from Core about object of interest (all_vehicles.py, poll_state() )
            # payload_detect = { 'vid':vid, 'X':pos['X'], 'Y':pos['Y'], 'Z':pos['Z'], 'lat':pos['lat'], 'lon':pos['lon'] }
            payload_detect = veh.qDetect.get()
            dX = veh.pos.X - payload_detect['X']
            dY = veh.pos.Y - payload_detect['Y']
            if use_2d_dist==1:
                dZ=0.0
            else:
                dZ = veh.pos.Z - payload_detect['Z']
            
            dist_XY = sqrt( pow(dX,2) + pow(dY,2) + pow(dZ,2) ) # (m) distance from this vehicle to the coordinates of interest
            
            if (debug>0): logging.debug('{0:s}, detectId={1}, qDetect.qsize()={2}, lastSeen={3:0.3f}(s), payload_detect={4}'.\
                                format(behName, veh.detectMsg.objId, veh.qDetect.qsize(), veh.detectMsg.lastSeen,payload_detect))
        
        # nearby enough to detect?
        if (dist_XY <= self.detectThreshold):
            # yep, we're close enough to say this virtual vehicle could detect the object of interest
            #
            # --> THIS trigger is what a REAL vehicle could REALLY do with a REAL objects in the REAL world
            #
            veh.detectMsg.objId       = payload_detect['vid'] # object id you're looking for (e.g. a specific vid)
            veh.detectMsg.lastSeen    = time.time() # (s) timestamp from last-seen detection
            veh.detectMsg.lastLoc_X   = veh.pos.X # (m) location from last-seem detection
            veh.detectMsg.lastLoc_Y   = veh.pos.Y # (m)
            veh.detectMsg.lastLoc_Z   = veh.pos.Z # (m)
            veh.detectMsg.lastLoc_lat = veh.pos.lat # (decimal degrees)
            veh.detectMsg.lastLoc_lon = veh.pos.lon # (decimal degrees)
            if (debug>0):   logging.debug('{0:s},\t\t detected {1}!!!, lastSeen={2:0.3f}(s)'.\
                            format(behName, veh.detectMsg.objId, veh.detectMsg.lastSeen))
        
        if (debug>0):   logging.debug('{0:s}, detectId={1}, lastSeen={2:0.3f}(s)'.\
                        format(behName, veh.detectMsg.objId, veh.detectMsg.lastSeen))
    
    logging.debug('[{0}]: bId={1} [{2}], exiting.'.format( datetime.now().strftime('%c') ,bId,behName))