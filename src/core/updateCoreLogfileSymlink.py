#!/usr/bin/env python3
#
# create a symbolic link to a file that points to a different logfile each
# when new logfiles are created.
#
# if a symlink already exists, remove and replace it
#
# from: symbolic_link_example_b.py
#
#
# Marc Compere, comperem@gmail.com
# created : 18 Oct 2022
# modified: 28 Mar 2024
#
# ---
# Copyright 2018 - 2024 Marc Compere
#
# This file is part of the Mobility Virtual Environment (MoVE).
#
# MoVE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3
# as published by the Free Software Foundation.
#
# MoVE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License version 3 for more details.
#
# A copy of the GNU General Public License is included with MoVE
# in the COPYING file, or see <https://www.gnu.org/licenses/>.
# ---

import os
import os.path # islink(), isfile(), https://stackoverflow.com/a/82852/7621907
from datetime import datetime


# -----------------------------------------------------------------------------
# --- symbolic link name will remain constant but points to current logfile ---
def updateCoreLogfileSymlink(symName,fname):
    if os.path.islink(symName) is True:
        print("[symlink] symlink already exists: [{0}]".format(symName))
        os.unlink(symName) # if it's there, get rid of it
        print("[symlink] symlink removed:        [{0}]".format(symName))
    
    print('[symlink] creating symbolic link: [{0}] -> [{1}]'.format(symName, fname))
    try:
        os.symlink(fname, symName) # os.symlink(src,dst)     (from,to)
        #os.chmod(symName, 0o0777)  # chmod 777 on symbolic link so any user can delete and over-write on same machine
        print('[symlink] symbolic          link: [{0}] now points to: [{1}]'.format(symName, os.readlink(symName)) )
    except:
        print('[symlink] error! problem creating symbolic link [{0}] for cloud upload: {1}'.format(symName,sys.exc_info()[0]) )
        raise
# -----------------------------------------------------------------------------


if __name__ == "__main__":
    
    # create a logfile name that changes
    tNowFname  = datetime.now()
    myStr            = tNowFname.strftime('%Y_%m_%d__%H_%M_%S') # strftime() = string format time: str='2018_07_24__18_18_40'
    logfile_loc_core = '/tmp'
    fname_move       = '{0}/{1}_core_State_move_TESTING.csv'.format(logfile_loc_core,myStr)
    
    symName = '/tmp/moveCoreLogfile.csv' # this will remain constant; same file name every time
    updateCoreLogfileSymlink(symName,fname_move) # update symbolik link to current MoVE Core logfile








