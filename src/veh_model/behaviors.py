#!/usr/bin/env python3
#
# usage:
#    ./behaviors.py
#
# adapted from: threading_g_priority_based_scheduler.py
#
# Marc Compere, comperem@gmail.com
# created : 07 Jul 2018
# modified: 28 Nov 2023
#
# --------------------------------------------------------------
# Copyright 2018 - 2023 Marc Compere
#
# This file is part of the Mobility Virtual Environment (MoVE).
# MoVE is open source software licensed under the GNU GPLv3.
#
# MoVE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3
# as published by the Free Software Foundation.
#
# MoVE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License version 3 for more details.
#
# A copy of the GNU General Public License is included with MoVE
# in the COPYING file, or see <https://www.gnu.org/licenses/>.
# --------------------------------------------------------------

import random
import threading
import time
from datetime import datetime
import queue
import numpy as np
import logging
from math import sin,cos,atan2,pi,copysign,sqrt,tanh
import code # drop into a python interpreter to debug using: code.interact(local=locals())
from collections import deque
from veh_util import compute_perpendicular_angle_to_line_in_space # for navigating through gates
from pprint import pformat # pretty print v2v structure



sign = lambda x: x and (1, -1)[x<0] # from: https://stackoverflow.com/a/16726462/7621907

logging.basicConfig( level=logging.DEBUG, format='(%(threadName)-14s) %(message)s', )

class Behaviors():
    """    A class for vehicle behaviors to control a vehicle model                         """
    """    these behaviors provide vehicle operator, driver, pilot or pedestrian commands   """
    """    to make the vehicle or pedestrian model zoom around                              """
    
    # load behaviors in different module .py files
    # from: https://stackoverflow.com/a/63323064/7621907
    from behaviors_simple import wander, periodicTurn, periodicPitch, circleInPlace, multiRotorHover
    from behaviors_avoid  import stayInBounds, avoidBySteer, avoidByBrake, avoidComputeOtherVehPositionInLocalFrame, detectAndReport
    from behaviors_goto   import followRoute, goToGate, goToPoint
    from behaviors_swarm  import swarmSteer, swarmBoids
    
    
    def __init__(self,cfg,qCmdVeh,qBehInfo,e,debug):
        
        self.behaviorCfg  = cfg.behaviorCfg  # straight from readConfigFile.py, e.g. self.behaviorCfg = {'wander': 1, 'periodicTurn': 2, 'periodicPitch': '0', 'stayInBounds': 12, 'avoid': 10, 'detectAndReport': '0', 'followRoute': '0'}
        self.qBehavior    = queue.Queue()    # queue for all behavior threads to send commands to prioritize()
        self.qCmdVeh      = qCmdVeh          # queue for prioritize() to send the selected commands to the vehicle
        self.qBehInfo     = qBehInfo         # a que for behavior info maintained in exact parity with qCmdVeh
        self.e            = e                # for signaling exit to all Behavior threads
        self.debug        = debug
        self.runState     = cfg.runState     # this should bring runState and runStatePrev visibility to every beh thread; recall: this is a *mutable* list for class and main visibility
        self.runStatePrev = cfg.runStatePrev
        
        self.detectThreshold      = cfg.detectThreshold
        self.pctCompleteGoToPoint = 0.0      # used by behaviors | goToPoint() and missionStateSequencer() to update pctComplete
        goToPointActive           = True     # used by behaviors | goToPoint() and missionStateSequencer() to indicate action complete
        
        # migrate behaviorCfg dictionary to 2 lists: behaviors and priority
        logging.debug('len(self.behaviorCfg)={0}'.format( len(self.behaviorCfg) ))
        self.behaviors = [0]*len(self.behaviorCfg) # init two lists for behavior names and priority
        self.priority  = [0]*len(self.behaviorCfg)
        self.bId       = [0]*len(self.behaviorCfg)
        for i,(beh,pri) in enumerate( self.behaviorCfg.items() ):
            logging.debug('Behaviors.__init__: behaviorCfg[{0}] = {1}:{2}'.format(i,beh,pri))
            self.behaviors[i]= 'self.'+beh # behaviors defined *within* the class (here, this file) need self. in front of it
            #self.behaviors[i]= beh # behavior name must match method definition here in behaviors.py
            self.priority[i] = pri # behavior priority
            self.bId[i]      = i   # behavior ID for prioritize()
        
        # ------------------ points init -------------------
        #code.interact(local=dict(globals(), **locals())) # drop into a python interpreter with globals and locals available
        if hasattr(cfg,'points') and hasattr(cfg.points,'IC_pt') and len(cfg.points.points)>0:
            self.points          = cfg.points
            logging.debug('detected points!')
            for k,v in self.points.points.items():
                logging.debug('\t{0}={1}'.format(k,v))
            logging.debug('points.IC_pt={0}'.format(self.points.IC_pt))
            logging.debug('points.lap={0}'.format(self.points.lap))
        
        # ------------------ gates init -------------------
        #code.interact(local=dict(globals(), **locals())) # drop into a python interpreter with globals and locals available
        if hasattr(cfg,'gates') and hasattr(cfg.gates,'firstGate') and len(cfg.gates.gates)>0:
            self.gates          = cfg.gates
            logging.debug('detected gates!')
            for k,v in self.gates.gates.items():
                logging.debug('\t{0}={1}'.format(k,v))
            logging.debug('gates.firstGate={0}'.format(self.gates.firstGate))
            logging.debug('gates.lap={0}'.format(self.gates.lap))
        
        # ----------- BEGIN: route configuration -------------
        # bring in the route objects if this particular vehicle happened to be configured to follow a waypoint route
        if hasattr(cfg,'routeCfg'): # a route must have been configured in the .ini file and assigned early in main_veh_model.py's __main__
            self.routeCfg = cfg.routeCfg # assign Behavior's self.route to config file route discovered in cfg file for this particular vehicle
            #logging.debug('vid={0} using routeCfg: {1}'.format(cfg.vid, cfg.routeCfg.__dict__))
        
        if hasattr(cfg,'route'): # a route must have been configured in the .ini file and assigned early in main_veh_model.py's __main__
            self.route = cfg.route # assign Behavior's self.route to config file route discovered in cfg file for this particular vehicle
            #logging.debug('vid={0} using route: {1}'.format(cfg.vid, cfg.route.__dict__))
        else:
            self.behaviorCfg['followRoute'] = 0 # confirm followRoute() not started; no need to run followRoute() if there's no route specified for this vehicle
            logging.debug('vid={0} has no route; not starting followRoute() behavior'.format(cfg.vid))
        # ----------- END: route configuration -------------
        
        
        # summarize which behaviors to start
        logging.debug('[{0}]: configured behaviors for {1}'.format( datetime.now().strftime('%c') , cfg.vid))
        for behName,priority in self.behaviorCfg.items():
            toStart = (priority>0)
            logging.debug("\tself.behaviorCfg: behavior name: {0:20s},   priority: {1:20d},   will start?: {2:3d}".format(behName,priority,toStart))
        
    # =======================================================================================
    #                                   startBehaviors()
    # =======================================================================================
    # avoid starting threads in __init__ so two separate mains can use
    # these methods:   main_srt_veh.py (vehicle model)   and   behavior_scheduler.py (for testing)
    def startBehaviors(self,veh):
        
        # start behavior and prioritize() thread
        bStartedCnt=0
        logging.debug('[{0}]'.format(datetime.now().strftime('%c')))
        for i, behName in enumerate( self.behaviors ):
            if (self.priority[i]>0): # if this behavior should be enabled...
                logging.debug('\tstarting behavior thread bId={0} : {1:20s} with priority {2}'.format( self.bId[i], behName, self.priority[i]))
                #code.interact(local=dict(globals(), **locals())) # drop into a python interpreter with globals and locals available
                logging.debug('\tself.behaviors[i]={0}'.format( self.behaviors[i] ))
                self.th = threading.Thread(name=behName, target=eval(self.behaviors[i]), args=(self.bId[i], behName, self.priority[i], veh, self.debug))
                self.th.start()
                bStartedCnt+=1
        logging.debug(' ')
        
        logging.debug('[{0}]: started [{1}] behaviors out of [{2}] possible'.format( datetime.now().strftime('%c') , bStartedCnt,len(self.behaviors) ))
        
        # start prioritize() thread
        self.p = threading.Thread(name='PRIORITIZE', target=self.prioritize,args=() )
        self.p.start()
        
        logging.debug('N vehicle and behavior threads: {}'.format( threading.active_count() ))
    
    
    # -----------------------------------------------------------------
    #                        prioritize()
    # -----------------------------------------------------------------
    # prioritize() -  finds highest priority process and sends those commands to the vehicle
    #
    # this is the decision making function from Rodney Brook's 1986 subsumption architecture
    # - any behavior (or other) thread can place a message in the queue to request consideration: qBehavior
    # - if message length is 2, this means disable that behavior (or put your hand down)
    # - otherwise:
    #   - update steer, speed, pitch command from this particular behavior
    #   - update the processEnable[] list with the bId'th priority array, priority[bId]
    #   - find highest priority behavior in current processEnable[] array - this is the most important process at this iteration
    #   - if no other higher priority process is currently running,
    #     copy the highest priority behavior's steer,speed,pitch cmd to vehicle model
    # - to dynamically change behavior priority on the fly, change priority[bId]
    def prioritize(self):
        tStart = time.time()
        behaviors = self.behaviors
        priority  = self.priority
        qBehavior = self.qBehavior
        qCmdVeh   = self.qCmdVeh
        qBehInfo  = self.qBehInfo
        debug     = self.debug
        name      = threading.currentThread().getName()
        dt        = 0.5 # (s) loop periodically to catch the all-exit signal from main, e.is_set()
        
        nBehaviors      = len(behaviors) # 3-behaviors + avoid(); ensure behaviors[] is proper length
        processEnable   = [0]*nBehaviors # init a list of all zeros, one for each thread; lists are mutable
        processProgress = [0]*nBehaviors # processProgress is reported here by all behaviors for missionStateSequencer(), but is not used in the prioritize() selection method 
        
        # vehCmd == [steer, speed, pitch]
        vehCmd = [[0]*4 for i in range(nBehaviors)] # initial vehCmd matrix is 3xnBehaviors, [steer, spd, pitch, v_z]_i for each behavior
        
        while self.e.is_set() is False:
            
            # blocking queue.get() controls loop iteration, but periodic timeout catches graceful exit signal, e.is_set()
            try:
                # did any behaviors raise their hand for prioritized consideration to command the vehicle?
                msg = qBehavior.get(block=True,timeout=dt) # Queue.get(block=True, timeout=None), timeout is for catching the thread exit signal, e.is_set()
            except queue.Empty:
                #logging.error("queue.Empty() timeout")
                continue # "continues with the next cycle of the nearest enclosing loop"  (which is 'while self.e.is_set() is False')
            
            # ------------------------------------------------------
            # parse message, update processEnable and vehCmd arrays
            #
            # message: [ bId , steer , speed, pitch, v_z, pctComplete ]
            bId=msg[0] # what behavior just sent a message?
            if (debug>1): logging.debug('recvd msg from bId={0} [{1}], msg=[{2}]'.format(bId,behaviors[bId],msg))
            
            # a behavior message length of 2 means the behavior just said "I'm done and putting my hand down" 
            if len(msg)==2:
                processEnable[bId]=0 # disable this behavior
                if (debug>1): logging.debug('DISable bId={} [{}]'.format(bId,behaviors[bId]))
            else:
                # assign steer , speed, pitch for this row of the behavior table - it's not clear if this will get used yet
                vehCmd[bId][0]       = msg[1] # steer cmd for the bId'th behavior
                vehCmd[bId][1]       = msg[2] # speed cmd for the bId'th behavior
                vehCmd[bId][2]       = msg[3] # pitch cmd for the bId'th behavior
                vehCmd[bId][3]       = msg[4] # v_z   cmd for the bId'th behavior
                processProgress[bId] = msg[5] # [0 1], update this process' progress
                processEnable[bId]   = priority[bId] # enable this behavior ID (bId) in the table
                if (debug>1): logging.debug('ENable  bId={} [{}]'.format(bId,self.behaviors[bId]))
            
            # ------------------------------------------------------
            # find which process has highest priority and assign it's outputs to the vehicle command
            
            highestPriorityProcess = max(processEnable) # determine highest priority process in the processEnable[] list
            
            bIdCmd = processEnable.index( highestPriorityProcess ) # returns 0-based index of highest priority process
            
            # this situation is when a middle-priority process requests enabling but is not selected because a higher priority behavior is active
            if (priority[bId]<priority[bIdCmd]):
                # the behavior that raised it's hand got rejected b/c current behavior has greater priority
                # note: this case occurs when a lower priority behavior raises *or* lowers it's hand
                if (debug>1): logging.debug('[{0}], other behavior activity ignored, not high enough priority: bId:{1} [{2}], priority:[{3}]'.format( datetime.now().strftime('%c') , bId, behaviors[bId], priority[bId]) )
                if (debug>1): logging.debug('[{0}], keeping current behavior: bId={1} [{2}], priority:[{3}]'.format( datetime.now().strftime('%c') , bIdCmd, behaviors[bIdCmd], priority[bIdCmd]) )
            
            # assign actuator commands from the highest priority process just determined, bIdCmd
            else:
                # send highest priority process commands to the vehicle
                vehCmdOut = vehCmd[bIdCmd]
                qCmdVeh.put(vehCmdOut) # send vehicle model driver cmds: [ steer, v_x, pitch ]  in myVehicle.py | gather_inputs()
                qBehInfo.put( ( bIdCmd, behaviors[bIdCmd], processProgress[bIdCmd] ) ) # behavior ID, bIdName, and bIdProgress sent to myVehicle.py | gather_inputs()
                
                bIdName=behaviors[bIdCmd]
                steerSpdPitch='steer={0:6.2f}, spd={1:6.2f}, pitch={2:6.2f}'.format(vehCmdOut[0], vehCmdOut[1], vehCmdOut[2])
                myStr='[{0}], bId={1:d}, behavior [{2:15s}], {3:s} assigned to vehicle:          [{4:15s}]'.format( datetime.now().strftime('%c') , bIdCmd, bIdName, steerSpdPitch, bIdName)
                if (debug>1): logging.debug( myStr )
                
        logging.debug('[{0}]: exiting.'.format( datetime.now().strftime('%c') ))

































if __name__ == "__main__":
    import os
    import sys
    sys.path.append(os.path.relpath("../scenario")) # find ../scenario/readConfigFile.py w/o (a) being a package or (b) using linux file system symbolic link
    from myVehicle import MyVehicle
    from readConfigFile import readConfigFile
    from route_init import route_init
    
    # class MyVehicle:
    #     pass # dummy vehicle class for testing Behaviors class
    
    debug=1 # (0/1/2/3) 0->none, 1->some, 2->more, 3->lots
    vid = 100 # vid == vehicle identifier for this vehicle process, assigned by MoVE core
    vid = 101
    
    cfgFile = '../scenario/default.cfg' # python's .ini format config file
    cfgFile = '../scenario/point_figure_8.cfg'
    cfgFile = '../scenario/point_homerun.cfg'
    cfgFile = '../scenario/missionTestWaitTimes.cfg' # config file with easily-test-able time-based delays
    #cfgFile = '../scenario/missionTestWaitTimesAndgoToPoint.cfg' # time delays and goToPoint() to develop missionState behavior enable/disable
    cfgFile = '../scenario/swarm_default.cfg'
    
    cfg, veh_names_builtins, veh_names_live_gps   = readConfigFile( cfgFile, vid ) 
    
    cfg.live_gps_follower = False # (True/False)
    cfg.name = "behaviors testing"
    cfg.runState          = [1]   # initial runstate; valid range is: [1 5], note this is a mutable *list* not an integer so subsequent variables reference the object not the value
    cfg.runStatePrev      =  0    # created in cfg. for behavior accessibility
    
    
    cfg = route_init(cfg) # this creates cfg.routeCfg ; the route_init() function is in routes_functions.py in ./veh_model directory
    #code.interact(local=dict(globals(), **locals())) # drop into a python interpreter with globals and locals available
    
    # init the vehicle object and launch behavior threads
    veh = MyVehicle(cfg, 0)
    
    cnt=0
    main_thread = threading.main_thread()
    nThreadsAlive = threading.active_count()
    logging.debug('nThreadsAlive={0}'.format(nThreadsAlive))
    
    try:
        while nThreadsAlive>1:
            
            logging.debug('-------------------------------------')
            nThreadsAlive = threading.active_count()
            logging.debug('{0} threads alive, cnt={1}'.format(nThreadsAlive,cnt) )
            for thread in threading.enumerate(): 
                logging.debug('\tthread [{0:30s}] \taliveStatus:[{1}]'.format(thread.name,thread.is_alive()))
            #logging.debug('beh.qBehavior.qsize()={0}, beh.qCmdVeh.qsize()={1}, beh.qBehInfo.qsize()={2}'.format( qBehavior.qsize() , beh.qCmdVeh.qsize(), beh.qBehInfo.qsize() ) )
            # note: this qCmdVeh and qBehInfo queues have no consumers unless connected to the vehicle model
            #       which means running this __main__ will have an accumulating qCmdVeh queue size
            
            veh.e.wait(5) # interruptable sleep() wait's unless event flag is set
            
            cnt+=1
            if cnt>5 and veh.e.is_set() is not True:
                print('\n\n\nsending exit event to all threads\n\n\n')
                veh.e.set()
            
        print('nThreadsAlive={}, main_thread exiting.'.format(nThreadsAlive))
        
        
    except KeyboardInterrupt as err:
        print("caught keyboard ctrl-c:".format(err))
        veh.exit()     # exit myVehicle, v2v, behavior, and mState threads
        print("exiting.")
        exit(0)




