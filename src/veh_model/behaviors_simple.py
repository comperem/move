# simple behaviors:
#       wander()
#       periodicTurn()
#       periodicPitch()
#       circleInPlace()
#       multiRotorHover()
#
# this is an include in the Behaviors class, in ./behaviors.py
#
# Marc Compere, comperem@erau.edu
# created : 18 Feb 2024
# modified: 18 Feb 2024
#
# --------------------------------------------------------------
# Copyright 2018 - 2024 Marc Compere
#
# This file is part of the Mobility Virtual Environment (MoVE).
# MoVE is open source software licensed under the GNU GPLv3.
#
# MoVE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3
# as published by the Free Software Foundation.
#
# MoVE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License version 3 for more details.
#
# A copy of the GNU General Public License is included with MoVE
# in the COPYING file, or see <https://www.gnu.org/licenses/>.
# --------------------------------------------------------------

import random
import time
from datetime import datetime
import queue
import numpy as np
import logging
from math import sin,cos,atan2,pi,copysign,sqrt,tanh
import code # drop into a python interpreter to debug using: code.interact(local=locals())
from pprint import pformat # pretty print v2v structure



# --------------------------------------------------------------
#                  behavior: wander()
# --------------------------------------------------------------
# wander process: wake up once, tell prioritize() 'go forward'; thread does not stay alive
def wander(self,bId,behName,priority,veh,debug):
    logging.debug('\t\t[{0}]: bId={1} [{2}], priority={3}'.format( datetime.now().strftime('%c') ,bId,behName,priority))
    
    #spd = 3.1415926/10.0 # (m/s) vehicle nominal wander speed
    spd = 5 #2.0 # (m/s) vehicle nominal wander speed
    #spd = 2.236 # (m/s) wander speed to hit vid 100 (vid=101 should go faster at psi_ic=-0.4636)
    self.qBehavior.put( [ bId , 0.0 , spd, 0.0, 0.0, 0.0 ]) # enable with these cmds: [ bId , steer , speed, pitch, v_z, progress ]
    
# --------------------------------------------------------------
#                  behavior: periodicTurn()
# --------------------------------------------------------------
# periodic turn: every 10 secs, turn a bit
def periodicTurn(self,bId,behName,priority,veh,debug):
    logging.debug('\t\t[{0}]: bId={1} [{2}], priority={3}'.format( datetime.now().strftime('%c') ,bId,behName,priority))
    e=veh.e
    randFlag=veh.randFlag # (0/1) use random numbers?
    
    dtDormant = 20 + randFlag*random.uniform(0.0,20.0) # (s) behavior dormant period (not turning)
    #spd       = 2.2 # (m/s) speed while turning
    dtTurn    = 2.0 # (s) turn duration when randFlag==0
    turn      = 0.5 # (rad) nominal turn is positive, or right-hand turn when randFlag==0
    while e.is_set() is False:
        e.wait(dtDormant) # (sec) behavior is dormant for this duration; e.wait() is an interruptable sleep() with independent timers despite using the same event, e
        
        if randFlag>0:
            dtTurn = 1 + randFlag*random.uniform(0.0,4.0) # (s) turn duration
            turn = (randFlag | 1)*random.uniform(-0.5,+0.5) # uniformly distributed random number
        
        if (debug>1): logging.debug('{0:s}, bId={1:d}, pri={2:d}: turn={3:f}'.format(behName, bId, priority, turn))
        
        # enable with these cmds: [ bId , steer , speed, pitch, progress ]
        self.qBehavior.put( [ bId , turn , 0.8*veh.vel.spd_mps, 0.0, 0.0, 0.0 ] ) # use spd or veh.vel.spd_mps
        
        #time.sleep(dtTurn) # (sec) duration to turn
        e.wait(dtTurn) # (sec) duration to turn; interruptable sleep()
        
        self.qBehavior.put( [ bId , 0 ] ) # disable with [bId, 0]
    
    logging.debug('[{0}]: bId={1} [{2}], exiting.'.format( datetime.now().strftime('%c') ,bId,behName))



# --------------------------------------------------------------
#                  behavior: periodicPitch()
# --------------------------------------------------------------
# periodic pitch: every 15 secs, go up or down a bit
def periodicPitch(self,bId,behName,priority,veh,debug):
    e=veh.e
    randFlag=veh.randFlag # (0/1) use random numbers?
    
    logging.debug('\t\t[{0}]: bId={1} [{2}], priority={3}'.format( datetime.now().strftime('%c') ,bId,behName,priority))
    dt = 10 + randFlag*random.uniform(0.0,5.0) # (s) intermittency period
    dtPitch = 2 + randFlag*random.uniform(-1.0,2.0) # (s) climb or dive duration
    spd = 3; # (m/s) speed while turning
    pitch = 0.5 # positive is up
    minHeight = 10  # (m) stay at least this high above the ground
    maxHeight = 100 # (m) stay at least this high above the ground
    while e.is_set() is False:
        
        e.wait(dt) # (sec) behavior is dormant for this duration; e.wait() is an interruptable sleep() with independent timers despite using the same event, e
        
        if randFlag>0:
            pitch = randFlag*random.uniform(-0.5,+0.5) # uniformly distributed random number
        
        if veh.pos.Z < minHeight:
            # too low, go up
            pitch = +0.1 # (rad)
        if veh.pos.Z > maxHeight:
            # too high, go down
            pitch = -0.1 # (rad)
        
        if (debug>1): logging.debug('{0:s}, bId={1:d}, pri={2:d}: pitch={3:f}'.format(behName, bId, priority, pitch))
        
        # enable with these cmds: [ bId , steer , speed, pitch, progress ]
        self.qBehavior.put( [ bId , 0.0 , 0.8*veh.vel.spd_mps, pitch, 0.0, 0.0 ] ) # use spd or veh.vel.spd_mps
        
        #time.sleep(dtPitch) # (sec) duration to turn
        e.wait(dtPitch) # (sec) duration to pitch; interruptable sleep()
        
        self.qBehavior.put( [ bId , 0 ] ) # disable with [bId, 0]
        
    logging.debug('[{0}]: bId={1} [{2}], exiting.'.format( datetime.now().strftime('%c') ,bId,behName))



# --------------------------------------------------------------
#                  behavior: circleInPlace()
# --------------------------------------------------------------
# *UNTESTED* circle in place: meant for fixed-wing aerial vehciles, fly large circles of radius, R_turn, until told otherwise
def circleInPlace(self,bId,behName,priority,veh,debug):
    e=veh.e
    randFlag=veh.randFlag # (0/1) use random numbers?
    
    logging.debug('\t\t[{0}]: bId={1} [{2}], priority={3}'.format( datetime.now().strftime('%c') ,bId,behName,priority))
    dt = 1 # (s) loop evaluation period
    R_turn = 10 # (m)
    while e.is_set() is False:
        
        e.wait(dt) # (sec) behavior is dormant for this duration; e.wait() is an interruptable sleep() with independent timers despite using the same event, e
        
        steer = veh.L_char / R_turn; # (rad) delta = L/R
        if (debug>1): logging.debug('{0:s}, bId={1:d}, pri={2:d}: pitch={3:f}'.format(behName, bId, priority, pitch))
        
        # enable with these cmds: [ bId , steer , speed, pitch, progress ]
        self.qBehavior.put( [ bId , steer , veh.vel.spd_mps, 0.0, 0.0, 0.0 ] ) # [ bId , steer , speed, pitch, v_z, progress ]
        
        #self.qBehavior.put( [ bId , 0 ] ) # disable with [bId, 0]
        
    logging.debug('[{0}]: bId={1} [{2}], exiting.'.format( datetime.now().strftime('%c') ,bId,behName))





# --------------------------------------------------------------
#                  behavior: multiRotorHover()
# --------------------------------------------------------------
# behavior to stay still in case goToPoint() and goToGate() are disabled
# multirotor equivalent of default wander process with lowest priority, but for UAV
def multiRotorHover(self,bId,behName,priority,veh,debug):
    logging.debug('\t\t[{0}]: bId={1} [{2}], priority={3}'.format( datetime.now().strftime('%c') ,bId,behName,priority))
    
    self.qBehavior.put( [ bId , 0.0 , 0.0, 0.0, 0.0, 0.0 ]) # enable with these cmds: [ bId , steer , speed, pitch, v_z, progress ]



