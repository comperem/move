#!/bin/bash
#
# Xing Sang, hello@singsongaftermath.com
# created : 23 Oct 2024
# modified: 23 Oct 2024


# Function to check if a Python module is installed
is_module_installed() {
    python3 -c "import $1" 2>/dev/null
}

# List of required Python modules
modules=(
    "os"
    "sys"
    "csv"
    "time"
    "code"
    "copy"
    # Add more modules as needed
    "numpy"
    "msgpack"
    "utm"
    "imutils"
    "opencv-python"
    "msgpack_numpy"
    "parallel-ssh"
    "bokeh"
    "matplotlib"
)

# Check if each module is installed, and install if missing
for module in "${modules[@]}"; do
    if ! is_module_installed "$module"; then
        echo "Installing $module..."
        # conda install "$module"
        pip install "$module"
        #pip install --break-system-dependencies "$module"
    fi
done


echo "All required Python modules are installed."

# Optional: Add instructions for next steps (e.g., setting up ssh)
