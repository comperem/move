## This is the Mobility Virtual Environment, or MoVE.

MoVE is open-source software released under the GNU Public License, version 3 (GPLv3).  

MoVE is maintained at these these locations:   
  * Source code on GitLab: https://gitlab.com/comperem/move  
  * Description on GitLab.io: https://comperem.gitlab.io/move  
  * Google Groups for discussion: https://groups.google.com/forum/#!forum/mobility_virtual_environment
  

***
### Overview
MoVE is designed to simulate and test multi-vehicle scenarios with a combination of
real and virtual autonomous vehicles in a common coordinate frame with a common timestamp.

Simulated or real vehicles can be accommodated in the same web-based dashboard and mapping
environment:

    (1) In simulation, vehicle models with different behaviors can be
    designed to fly or drive to points, gates, follow routes,
    or avoid with specific behaviors, or even flock using the
    Boids algorithm behavior. Multi-vehicle missions can be
    designed using command sequences with the Multi-vehicle Mission Language.

    (2) For real vehicles, multiple wireless networks can be
    accommodated to track real vehicles in the real world
    being flown by real pilots. In this use-case, MoVE is a
    multi-vehicle monitor and data collection system.

The MoVE environment allows autonomy algorithms such as Detect-And-Avoid (DAA) and
search-and-rescue (SAR) algorithm development to progress from simulation-only
to mixed simulation-and-real testing to all real vehicles testing in the real world.

The MoVE environment is also ideally suited for FAA regulators and researchers
to develop and test Unmanned Aircraft System Traffic Management (UTM) regulations
to improve public safety.

keywords:
ground vehicles, drones, UAS, sUA, SUAS, simulation,
telemetry, multi-vehicle, multi-vehicle mission language (MML)


***

Technical descriptions:

    (1) Simulated vehicles are launched as separate computer processes, each with their own
    behavior, numerical integration, and communication threads. A soft-real-time subsystem
    integrates a set of mobility ODEs at the same rate as wall-clock time.
    Equations of motion are written in body-fixed `xyz` coordinates and transformed into
    the inertial frame, `XYZ`. The solution in `XYZ` represents vehicle motion in a
    3D orthogonal coordinate frame.

    Points, paths, gates, and initial conditions are specified with latitude and longitude
    and converted to the `SAE XYZ` frame, which is the `NED` frame,
    using the WGS-84 geodetic model in the [UTM](https://pypi.org/project/utm/) python library.

    (2) Motion from real vehicles or pedestrians can be incorporated in the same virtual
    environment with simulated vehicles using a vehicle process with no dynamic
    equations of motion. This model type is called a live-GPS-follower.
    When a real person or real vehicle moves and sends GPS latitude and longitude
    coordinates to the live-GPS-follower, the motion in the virtual world is based on
    the real motion of the real person or vehicle in the real world.

A configuration file captures all details of a scenario to make sharing scenarios simple
to enhance collaboration.


***

### Software Language:

MoVE is a work in progress and written nearly entirely in
[Python 3](https://www.python.org/) with two m-file post-processing scripts that work in [Matlab](www.mathworks.com)
(and probably [Octave](https://www.gnu.org/software/octave/)).

***
### GNU Public License, version 3:
  - The MoVE software is open-source and freely available under the terms of the GNU Public License, version 3 (GPLv3).
  - The GPLv3 is endorsed by the Open Source Initiative and is described here: https://opensource.org/licenses/gpl-3.0.html
  - The GPLv3 is a copyleft license which means you may redistribute it only under the same GPLv3 license terms.
    The intent behind the copyleft is to build a strong user community and ensure the original open-source software is
    not made proprietary or no longer open-source.
    Read more here about copyleft here: https://opensource.com/resources/what-is-copyleft




***
  Marc Compere, Ph.D.  
  comperem@erau.edu  
  created : 14 July 2018  
  modified: 26 Jun 2024



