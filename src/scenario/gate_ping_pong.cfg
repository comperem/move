# MoVE multi-vehicle scenario definition configuration file.
#
# gate_loop.cfg: 5 vehicles doing laps around a loop made of gates
#
#
# Marc Compere, comperem@gmail.com
# created : 04 Jan 2019
# modified: 20 Jun 2020
#
# --------------------------------------------------------------
# Copyright 2018 - 2020 Marc Compere
#
# This file is part of the Mobility Virtual Environment (MoVE).
# MoVE is open source software licensed under the GNU GPLv3.
#
# MoVE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3
# as published by the Free Software Foundation.
#
# MoVE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License version 3 for more details.
#
# A copy of the GNU General Public License is included with MoVE
# in the COPYING file, or see <https://www.gnu.org/licenses/>.
# --------------------------------------------------------------



# ------------------------------------------------------------------------------
# core and vehicle processes look at this section for common configuration
# ------------------------------------------------------------------------------
[common]

nVeh_builtin  = 5   # launch this many built-in vehicle models with behaviors specified below
nVeh_live_gps = 0   # launch this many live-GPS-follower processes listening for incoming GPS coords on
nVeh_custom   = 0   # unused; for future expansion

nVeh_hosts = 1 # keep nVeh_hosts=1 until functionality is coded to launch across multiple vehicle hosts (parallel-ssh can do this very easily)
core_host_ip: localhost # localhost only valid for core_host_ip when veh_host_ip is *also* localhost (if not, then veh_model starts somewhere else and sends to that machine)
veh_host_ip: localhost

# each vehicle identifier is assigned with:  vid = vid_base + i_th_veh  (builtins first, then live gps followers)
# each vehicle listens for core messages on port = (udp_port_base + vid)
# each vehicle sends updates to core on     port = (udp_port_base + vid + udp_port_offset)
# each live GPS follower process listens for lat/lon on port = udp_port_base_gps + vid
# don't touch these unless you need more than 500 built-in + live gps followers
vid_base = 100
udp_port_base = 6000 # for vehicle-to-Core messages (veh_udp_io.py; avoid zeroconf on port 5353 w ubuntu linux)
udp_port_offset = 500 # for Core-to-vehicle messages (veh_udp_io.py)
udp_port_base_gps = 9000  # for live GPS follower messages (with lat/lon) from a mobile device running HyperIMU: udp_port_base_gps+vid

# debug levels: 0,1,2,3 (higher gives more console debugging output)
debug=1


# ------------------------------------------------------------------------------
[scenario]
#origin latitude and longitude
#boundary geometry
#no-go geometry
# waypoint sets for path following
#detectId = 100 # vid for vehicle-side detectAndReport() behavior
detectThreshold = 20 # (m) detection radius threshold
#detectThreshold = 5 # (m) detection radius threshold

# define boundary for stayInBounds() behavior (if enabled); stay in this square
boundary_Xmax = +250.0 # (m) UTM coordinates w.r.t. origin
boundary_Xmin = -250.0 # (m)
boundary_Ymax = +250.0 # (m)
boundary_Ymin = -250.0 # (m)



# ------------------------------------------------------------------------------
[core]

sync_veh_and_cfg: True # (0/1) if true, scp this config file to the vehicle host prior to launching veh processes

logfile: 1 # (0/1) log all vehicles in State to local .csv file at 1s intervals (defined by dt in all_vehicles.py | poll_state() )
logfile_loc_core: /tmp #/run/shm #/tmp, note: careful! you can crash your machine using /run/shm b/c is a ramdisk by default on Ubuntu and Debian systems

dtOut = 0.3 # (s) communication output interval for Core to send to viz, database, and csv logfile

# incorporate core viz outputs:
vizUdpEnabled=True
vizIp   = localhost
#vizIp   = 10.33.106.227 # Garrett's ip on wireless network
#vizIp   = 155.31.130.51 # Garrett's ip on wired network
vizPort = 5555

dashIp   = localhost
dashPort = 5556

# MongoDB interface
dbUdpEnabled=False
#dbIp   = 10.33.106.103 # Otto's laptop IP on EagleNET
dbIp   = 155.31.130.169 # Otto's laptop IP on EagleNET
dbPort = 5000

# ------------------------------------------------------------------------------
[veh_v2v]
v2vEnabled=True # (True/False) enable or disable v2v threads and multicast udp/ip communications
multicast_group = 224.3.29.71 # ipv4 addresses 224.0.0.0 through 230.255.255.255 are reserved for multicast traffic
multicast_port  = 10000 # udp multicast port for all v2v senders and listeners
v2v_cint_send   = 1.0 # (s) outbound v2v communication interval for broadcasting a single vehicle's update to all other vehicles
v2v_cint_read   = 2.0 # (s) receiver's observation interval of complete v2vState snapshot; read complete v2v netowrk view used by mission scheduler at this interval
v2v_dRadio      = 500 # (m) radio range; any vehicles within this distance are radio active
v2v_dNeighViz   = 50  # (m) neighbor range; any vehicles within this distance are visual neighbors
v2v_dNeighClose = 10  # (m) neighbor range; any vehicles within this distance are visual and also very close neighbors (too close)
v2v_printLevel  = 1 #2 #1 # (0/1/2) how much to print v2v updates in the veh model console output



# ------------------------------------------------------------------------------
[veh_sim]

vehRuntimeDir_base=/tmp/move # rsync veh_model to remote machine for execution in this folder, note: /run/shm is a ramdisk where logging takes place
h = 0.010 # (s) vehicle dynamics RK4 integration stepsize; choose cInt and h such that they are an integer ratio, say cInt/h=10
cInt=0.1 # (s) vehicle-to-core reporting interval; local .csv logging interval; 0.1 is 10Hz, 0.5 is 2Hz
hInt = 1.0 # (s) vehicle-to-core hearbeat interval; only relevant during runState==1 (READY)
randFlag=1 # (0/1) use random numbers? plumbed in everwhere relevant (ICs and random triggers in behaviors)

IC_sel = 1 # (1/2/3) global default: 1->straight down the X-axis, 2-> grid pattern within geom extents, 3->collision scenario for 2 vehicles
L_char = 2.0 # (m) characteristic vehicle length
v_max = 26.8 # (m/s) nominal max vehicle speed (26.8m/s=60mph)

IC_elev = 0.0 # (m) default height for all vehicles in UTM XYZ coordinate frame


lat_origin =  29.193111 # (decimal deg) Home plate, Baseball fields, Embry-Riddle Aeronautical Univ, Daytona Beach, Florida
lon_origin = -81.046171 # (decimal deg)
#lat_origin =  29.188523  # (decimal deg) S-building curb, Embry-Riddle, Daytona Beach, Florida
#lon_origin = -81.045633 # (decimal deg)

log_level: 0 # (0/1/2) 0->no logging, 1-> .csv file only, 2-> .csv and capture console output in /tmp/debug*.txt for debugging veh model and behavior
             # log_level==1 -> monitor veh console output by reattaching to screen with 'screen -r' to watch running vehicle process
             # log_level==2 -> monitor veh console output with 'tail -f /tmp/debug100.txt' to monitor vehicle model console output (screen holds the process but has no output to display)
logfile_loc_veh: /tmp #/run/shm #/tmp, note: careful! you can crash your machine using /run/shm b/c is a ramdisk by default on Ubuntu and Debian systems


# vehicle types:
#   'pedestrian'
#   'ground', vehSubType--> 'onroad' or 'offroad'
#   'aerial', vehSubType--> 'fixedwing' or 'rotorcraft'
#   'surface'
#   'underwater'


# only change global defaults for these specific vehicles
[veh_details] # optionally assign: vehicle name, vehType, vehSubType, initial conditions
#veh_type_default: { 'vehType': 'pedestrian',  'vehSubType': 'none' }
#veh_type_default: { 'vehType': 'ground',      'vehSubType': 'none' }
veh_type_default: { 'vehType': 'aerial',      'vehSubType': 'fixedwing' }
#veh_type_default: { 'vehType': 'surface',     'vehSubType': 'none' }
#veh_type_default: { 'vehType': 'underwater',  'vehSubType': 'turtle' }

vid_100: {'name':'billy', 'vehType': 'aerial', 'vehSubType':'rotorcraft', 'IC_latlon':'29.193899, -81.046432', 'IC_elev': 0.0, 'IC_yaw_rad': 0.0 }
vid_101: {'name':'rufus', 'vehType': 'aerial', 'vehSubType':'rotorcraft', 'IC_latlon':'29.193337, -81.045272', 'IC_elev': 0.0, 'IC_yaw_rad': 2.4 } # 3*pi/4 = 2.3562


# ------------------------------------------------------------------------------
# prioritize or disable (set to 0) behaviors for all vehicles; see [veh_behaviors_individual] for vehicle-specific behavior settings
# note #1: higher priority takes precendence over lower; priority=0 disables
# note #2: these names are case-sensitive and must match exact function names as defined in vehicle's behaviors.py
[veh_behaviors_global]
wander          : 0 # default is to turn everything off globally
periodicTurn    : 0
periodicPitch   : 0
stayInBounds    : 0
avoidBySteer    : 0 # for aerial vehicles to avoid collision using *steer*
avoidByBrake    : 0 # for ground vehicles to avoid collision using longitudinal (braking) (slow down to not hit the veh in front)
detectAndReport : 0 # todo: convert this to (or add) followMe() behavior
followRoute     : 0 
goToGate        : 0 
goToPoint       : 0 
multiRotorHover : 0 # UAV equivalent to default wander process; lowest priority; when nothing else is happening, just hover



# per-vehicle behavior priorities override global settings in [veh_behaviors_global]
[veh_behaviors_individual] # enable behaviors for individual vehicles
vid_100: {'wander': 1, 'periodicTurn': 2, 'avoidBySteer': 5, 'goToGate': 7 } # 'stayInBounds': 12, 'avoidBySteer': 10, 
vid_101: {'wander': 1, 'periodicTurn': 2, 'avoidBySteer': 5, 'goToGate': 7 }
vid_102: {'wander': 1, 'periodicTurn': 2, 'avoidBySteer': 5, 'goToGate': 7 }
vid_103: {'wander': 1, 'periodicTurn': 2, 'avoidBySteer': 5, 'goToGate': 7 }
vid_104: {'wander': 1, 'periodicTurn': 2, 'avoidBySteer': 5, 'goToGate': 7 }


# define gates; to enable gates, the goToGate() behavior must be turned on;
# gates must be defined with Point A on the left and Point B on the right, plus an initial gate assignment must be made
[gates] # gate_NNN: [ <lat_A>, <lon_A>,    <lat_B>, <lon_B>,    <nextGate> ] # this makes a gate with point A at (lat_A,lon_A) and point B at (lat_B,lon_B)
gate_1: {'ptA_lat': 29.193577, 'ptA_lon': -81.046308,    'ptB_lat': 29.193865, 'ptB_lon': -81.046126,   'nextGate': 2 }   # gate 1 at West end of ERAU Baseball field, Goto gate 2
gate_2: {'ptA_lat': 29.193255, 'ptA_lon': -81.045650,    'ptB_lat': 29.193541, 'ptB_lon': -81.045468,   'nextGate': 1 }   # gate 2 at East end of ERAU Baseball field, Goto gate 1 (this causes the ping pong)
#gate_1: {'ptA_lat': 29.191032, 'ptA_lon': -81.048414,    'ptB_lat': 29.193865, 'ptB_lon': -81.046126,   'nextGate': 2 }   # gate 1 at West end of ERAU Baseball field, Goto gate 2
#gate_2: {'ptA_lat': 29.193255, 'ptA_lon': -81.045650,    'ptB_lat': 29.191032, 'ptB_lon': -81.048414,   'nextGate': 1 }   # gate 2 at East end of ERAU Baseball field, Goto gate 1 (this causes the ping pong)





#28.318481, -80.607708 # Gate 1, A, Cocoa Beach, main entrance
#28.303986, -80.607581 # Gate 1, B, Cocoa Beach, 8th St., 1mi South of main entrance




# ------------------------------------------------------------------------------
[veh_route_assignments] # vid_NNN: [ <route_number>, <nLaps>, <startOption>, <startingWaypoint>, <endOption> ]
#
# each vehicle wakes up and looks here to determine if and what route to follow
# note: make sure the followRoute() behavior has non-zero priority so it gets started
#
# format:
#     vid_NNN:                                      # vehicle identifier, vid_100, vid_101, ...
#              {'routeScript':'<route_file.py>',    # python script defining route (see ./routes)
#               'nLapsMax':<num>,                   # max number of laps, then trigger end option
#               'startOption':'startAt',            # either 'startAt' or 'goTo' (only 'startAt' is working now)
#               'startingWaypoint':  <num>,         # starting waypoint
#               'lapRestartWaypoint':<num>,         # restarting waypoint number after each lap
#               'endOption':'stop',                 # either 'stop' or 'continue'
#               'mapColor':'<color>' }              # for example, 'magenta', 'red'
#
#vid_100: {'routeScript':'route_01_waypoints.py', 'nLapsMax':19999, 'startOption':'startAt', 'startingWaypoint': 1, 'lapRestartWaypoint':1, 'endOption':'stop', 'mapColor':'yellow' }
#vid_101: {'routeScript':'route_01_waypoints.py', 'nLapsMax':29999, 'startOption':'startAt', 'startingWaypoint': 5, 'lapRestartWaypoint':1, 'endOption':'stop', 'mapColor':'yellow' }
#vid_102: {'routeScript':'route_01_waypoints.py', 'nLapsMax':39999, 'startOption':'startAt', 'startingWaypoint': 9, 'lapRestartWaypoint':1, 'endOption':'stop', 'mapColor':'yellow' }










[veh_names_builtins]
name1: doc
name2: bashful
name3: dopey
name4: grumpy
name5: happy
name6: sleepy
name7: sneezy
name8: mickey
name9: minnie
name10: daffy
#name11: donald
#name12: daisy
#name13: bambi
#name14: thumper
#name15: scrooge
#name16: pluto
#name17: mercury
#name18: venus
#name19: earth
#name20: mars
#name21: jupiter
#name22: saturn
#name23: uranus
#name24: neptune


[veh_names_live_gps]
name1: garrett
name2: otto
name3: roberto
name4: tyler
name5: shelby
name6: compere
