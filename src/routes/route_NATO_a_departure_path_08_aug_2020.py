#!/usr/bin/env python3
#
# restore waypoints for a MoVE route for a vehicle to follow in a .cfg file
#
# usage:
#    ./route_a_departure_path_08_aug_2020.py
#
#
# Marc Compere, comperem@gmail.com
# created : 14 Jul 2019
# modified: 09 Aug 2020
#
# --------------------------------------------------------------
# Copyright 2018, 2019 Marc Compere
#
# This file is part of the Mobility Virtual Environment (MoVE).
# MoVE is open source software licensed under the GNU GPLv3.
#
# MoVE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3
# as published by the Free Software Foundation.
#
# MoVE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License version 3 for more details.
#
# A copy of the GNU General Public License is included with MoVE
# in the COPYING file, or see <https://www.gnu.org/licenses/>.
# --------------------------------------------------------------

import numpy as np
import logging


# ------------------------------------------------------------------------------
# the restore_waypoint() function gets called from MoVE vehicle model
# or as a stand-alone program with __main__ below
def restore_waypoints( cfg ):
    import numpy as np
    from routes_functions import computeRouteStatistics
    import utm
    
    # create an empty route object 
    class Route:
        pass
    route = Route()
    
    # all waypoints must eventually be in an orthogonal, terrain-fixed XYZ Cartesian coordinate system
    # however, route, or path coordinates may be specified in lat/lon pairs or (X,Y) coordinates directly
    #
    # - if original route coordinates are specified in meters, then use 'meters' for route.originalUnits
    # - if original route coordinates are specified in lat/lon pairs in decimal degrees, use 'decimal_degrees'
    route.originalUnits = 'decimal_degrees' #'meters'
    route.desc = "route [a] for departure phase of NATO AVT-341 competition for Environments committee"
    
    # -------------------------------
    # specify the route coordinates:
    # -------------------------------
    # the lines below were copy-pasted directly from this .csv: fort_collins_spring_canyon_park_1_lap_13_Jul_2019.csv
    # the .csv file was written by fort_collins_lap_preprocessor.m which works in Matlab or Octave
    #
    # for a series of points without commas, python has something called
    # multi-line strings that are indicates by three quotes in a row (single or double quotes)
    #
#waypoints 2,3,4,5
#28.0072057 , -80.523831 , 18
#28.0069359 , -80.523831 , 18
#28.0059544 , -80.523831 , 18
#28.0056846 , -80.523831 , 18

    # format: lat,lon,speed (m/s)
    # straight from: output_for_move_route.txt and setup_read_kml_dev.m
    # in /Google_drive/Compere/ERAU/ERAU_admin_docs/committees/NATO_AVT-341_Mobility_Autonomy_Ground/Environments_subcommittee/02_Task_1_Departure_path_definition
    my_multi_line_string_of_lat_lon_points = """
47.1697744568723,-88.5018377825017,0
47.1698503359064,-88.5018294045748,3.87420489
47.169915654247,-88.501802394387,2.38406037616718
47.1699694025754,-88.5017395144107,2.22222222222222
47.1699840914951,-88.5016464029999,4.782969
47.1699782638912,-88.5015303283296,6.561
47.1699705370794,-88.5014314072628,8.1
47.1699618679627,-88.5012908281173,10
47.1699577390633,-88.5012256119693,10
47.1699504428434,-88.5011310616327,11.1111111111111     # waypoint 10
47.1699302670448,-88.5010455937645,9
47.169908722265,-88.5009713033495,8
47.1698686684688,-88.5009096807023,7
47.1698148750752,-88.5008816210563,4
47.1697631103688,-88.5008852811701,2.22222222222222
47.1697261063027,-88.5008350479066,2.22222222222222
47.1697007746413,-88.5007436000327,2.22222222222222
47.1696530732272,-88.5007195414905,2.22222222222222
47.1696054863099,-88.5007724509785,2.22222222222222
47.1695977348976,-88.5008899658213,2.22222222222222     # waypoint 20
47.1695192752657,-88.5009610363255,2.48801855868499
47.1694468241447,-88.5009790406562,2.22222222222222
47.1693822169382,-88.5009501532362,4.782969
47.1693432559529,-88.5009086426482,5.9049
47.1693137080054,-88.5008622885786,6.561
47.1692669406907,-88.5007885053944,8.1
47.1691963750028,-88.5006864833991,10
47.1691195035542,-88.5005990020406,11.1111111111111
47.1690154914553,-88.5005079939518,11.1111111111111
47.1688249775695,-88.5003825341526,11.1111111111111     # waypoint 30
47.1684345548302,-88.5001767131143,11.1111111111111
47.168356301391,-88.5001670253865,11.1111111111111
47.1682703769518,-88.5001835191846,11.1111111111111
47.1681984809736,-88.5002345510319,11.1111111111111
47.1681315756404,-88.5003142537818,11.1111111111111
47.1680862829607,-88.5003924277819,11.1111111111111
47.168038221776,-88.5005155527651,11.1111111111111
47.1680039578914,-88.5006283548196,11.1111111111111
47.1679675957551,-88.5007699238423,11.1111111111111
47.1679325431552,-88.5008989164248,11.1111111111111     # waypoint 40
47.1678506648743,-88.5010795611669,11.1111111111111
47.1677565670883,-88.5012777691264,11.1111111111111
47.1676374157792,-88.5014544841594,11.1111111111111
47.1675035227575,-88.5015586297469,11.1111111111111
47.1673243511808,-88.5016558673106,11.1111111111111
47.1666244484302,-88.5018721448058,11.1111111111111
47.166262181535,-88.5019714740138,11.1111111111111
47.1654080428354,-88.5022515715774,11.1111111111111
47.1649419300955,-88.5024102694676,11.1111111111111
47.1646600543911,-88.5025117811117,11.1111111111111     # waypoint 50
47.1644791990596,-88.5025968837681,11.1111111111111
47.1643685466841,-88.5027053886557,11.1111111111111
47.1642641173831,-88.5028771898459,11.1111111111111
47.1636700725437,-88.5038923460739,11.1111111111111
47.1635422878972,-88.5041109221757,11.1111111111111
47.1634576633421,-88.50428120249,11.1111111111111
47.1633808639692,-88.5046065939502,11.1111111111111
47.1632536140777,-88.5053830122859,11.1111111111111
47.1631680405748,-88.5059928918203,11.1111111111111
47.1630924414458,-88.5062801861585,8                    # waypoint 60
47.1629706879164,-88.5063864788005,8
47.1628325117264,-88.5063531875282,8
47.1627514562816,-88.5062957590177,8.1
47.1627103435477,-88.5062618383035,9
47.1620932107007,-88.5058346865295,11.1111111111111
47.1615929253571,-88.5055342997565,11.1111111111111
47.1614808304626,-88.5054663762361,11.1111111111111
47.1613690635582,-88.5053986733777,11.1111111111111
47.1613736521096,-88.5054054269124,11.1111111111111
47.161264726015,-88.5053395199628,11.1111111111111      # waypoint 70
47.1611342175985,-88.5052649409607,11.1111111111111
47.1610327452627,-88.5052105556305,11.1111111111111
47.1609540729963,-88.5051561744599,11.1111111111111
47.1608654005306,-88.5050864311457,11.1111111111111
47.1608085282824,-88.5049959239462,11.1111111111111
47.160774373835,-88.5048917932705,11.1111111111111
47.1607399213189,-88.5048013712214,11.1111111111111
        """
    # all math and route following must be done in orthogonal coordinates, so
    # convert these lat/lon pairs to UTM coordinates which assumes a flat Earth
    # within this UTM zone (there are 60 zones around Earth)
    
    list_of_rows = my_multi_line_string_of_lat_lon_points.split('\n')
    
    # initialize native python lists
    lat=[]
    lon=[]
    X=[]
    Y=[]
    spd_mps=[]
    cnt=0
    #print('---')
    for row in list_of_rows:
        #print(row)
        if row.strip() != '': # strip() removes leading and trailing whitespace
            print('processing row {0}: [{1}]'.format(cnt,row))
            comment_str=row.split('#')
            str=comment_str[0].strip().split(',')
            #print( repr(row) ) # repr() shows invisible characters to distinguish spaces from tabs and newlines
            #str=row.strip().split("\t")
            #str=row.strip().split(",")
            
            lat_single_pt   = float( str[0] )
            lon_single_pt   = float( str[1] )
            speed_single_pt = float( str[2] )
            
            # utm library converts from lat/lon in decimal degrees to orthogonal
            # XYZ coordinates in meters within a single UTM zone
            (X_single_pt,Y_single_pt,zone,latBand) = utm.from_latlon(lat_single_pt,lon_single_pt) # these are typically giant numbers, like (X,Y)=(489174.484, 4488110.896)
            
            # list.append() is super fast for growing native python lists dynamically
            lat.append( lat_single_pt )
            lon.append( lon_single_pt )
            X.append( X_single_pt ) # X_origin and Y_origin are also typically giant numbers, like 
            Y.append( Y_single_pt )
            spd_mps.append( speed_single_pt )
            cnt=cnt+1
    #print('---')
    
    lat     = np.array(   lat  ) # convert native python lists to numpy array objects
    lon     = np.array(   lon  )
    X       = np.array(     X  )
    Y       = np.array(     Y  )
    Z       = np.zeros( len(X) ) # (m) initialize Z with numpy array of all zeros
    spd_mps = np.array( spd_mps  ) # (m/s) commanded speed for all waypoints is from SensorLog's original GPS-based recording
    
    
    # --------------------------------------------------------------------------
    # now stuff these arrays (python lists) into the route object create at the top
    # MoVE will use this function's return 'route' object from here on out
    route.lat = lat
    route.lon = lon
    route.X = X # (m) X array in cartesian XYZ coordinates
    route.Y = Y # (m) Y array in cartesian XYZ coordinates
    route.Z = Z # (m) Z array in cartesian XYZ coordinates
    route.spd_mps = spd_mps # (m/s)
    if '__file__' in locals():
        route.source = __file__ # this file's name
    else:
        route.source = __name__ # __main__ when debugging in a terminal console
    
    route = computeRouteStatistics(route)
    
    return(route)
# ------------------------------------------------------------------------------


logging.basicConfig(
    level=logging.DEBUG,
    format='(%(threadName)-14s) %(message)s',
)



if __name__ == "__main__":
    
    import matplotlib.pyplot as plt
    import multiprocessing
    import os
    from routes_functions import computeRouteStatistics, plot_graph
    
    print("starting __main__")
    
    class Cfg:
        pass
    
    cfg = Cfg()
    #cfg.vid=101
    #cfg.L_char = 1.0 # (m) example vehicle's characteristic length
    #cfg.v_max = 20.0 # (m/s) example vehicle's maximum speed
    
    # this function is called from the vehicle model, main_veh_model.py
    route = restore_waypoints( cfg )
    
    # print full route data structure
    #for key,val in route.__dict__.items():
    #    print("{0}={1}".format(key,val))
    
    # optionally bring the plot window up in a separate process
    makePlots=True #False
    skipNthLabel=1 #1 % use 10 or 50 if waypoint labels clutter the plot
    if (makePlots==True):
        multiprocessing.Process(target=plot_graph, args=(route,skipNthLabel)).start()
        print("exiting main")
        os._exit(0) # this exits immediately with no cleanup or buffer flushing

    
    



























