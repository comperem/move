# MoVE multi-vehicle scenario definition configuration file.
#
# default.cfg: 5 vehicles with random motion inside a geofenced area
#
#
# Marc Compere, comperem@gmail.com
# created : 04 Jan 2019
# modified: 23 Jan 2022
#
# --------------------------------------------------------------
# Copyright 2018 - 2024 Marc Compere
#
# This file is part of the Mobility Virtual Environment (MoVE).
# MoVE is open source software licensed under the GNU GPLv3.
#
# MoVE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3
# as published by the Free Software Foundation.
#
# MoVE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License version 3 for more details.
#
# A copy of the GNU General Public License is included with MoVE
# in the COPYING file, or see <https://www.gnu.org/licenses/>.
# --------------------------------------------------------------



# ------------------------------------------------------------------------------
# core and vehicle processes look at this section for common configuration
# ------------------------------------------------------------------------------
[common]

nVeh_builtin  = 4   # launch this many built-in vehicle models with behaviors specified below
nVeh_live_gps = 0   # launch this many live-GPS-follower processes listening for incoming GPS coords on
nVeh_custom   = 0   # unused; for future expansion

nVeh_hosts = 1 # keep nVeh_hosts=1 until functionality is coded to launch across multiple vehicle hosts (parallel-ssh can do this very easily)
core_host_ip: localhost # localhost only valid for core_host_ip when veh_host_ip is *also* localhost (if not, then veh_model starts somewhere else and sends to that machine)
veh_host_ip: localhost

# each vehicle identifier is assigned with:  vid = vid_base + i_th_veh  (builtins first, then live gps followers)
# each vehicle listens for core messages on port = (udp_port_base + vid)
# each vehicle sends updates to core on     port = (udp_port_base + vid + udp_port_offset)
# each live GPS follower process listens for lat/lon on port = udp_port_base_gps + vid
# don't touch these unless you need more than 500 built-in + live gps followers
vid_base = 100
udp_port_base = 6000 # for vehicle-to-Core messages (veh_udp_io.py; avoid zeroconf on port 5353 w ubuntu linux)
udp_port_offset = 500 # for Core-to-vehicle messages (veh_udp_io.py)
udp_port_base_gps = 8000  # for live GPS follower messages (with lat/lon) from a mobile device running HyperIMU: udp_port_base_gps+vid

# debug levels: 0,1,2,3 (higher gives more console debugging output)
debug=1


# ------------------------------------------------------------------------------
[scenario]
#origin latitude and longitude
#boundary geometry
#no-go geometry
# waypoint sets for path following
#detectId = 100 # vid for vehicle-side detectAndReport() behavior
detectThreshold = 100 # (m) detection radius threshold
#detectThreshold = 5 # (m) detection radius threshold

# define boundary for stayInBounds() behavior (if enabled); stay in this square
boundary_Xmax = +250.0 # (m) UTM coordinates w.r.t. origin
boundary_Xmin = -250.0 # (m)
boundary_Ymax = +250.0 # (m)
boundary_Ymin = -250.0 # (m)



# ------------------------------------------------------------------------------
[core]

sync_veh_and_cfg: True # (0/1) if true, scp this config file to the vehicle host prior to launching veh processes

logfile: 1 # (0/1) log all vehicles in State to local .csv file at 1s intervals (defined by dt in all_vehicles.py | poll_state() )
logfile_loc_core: /tmp #/run/shm #/tmp, note: careful! you can crash your machine using /run/shm b/c is a ramdisk by default on Ubuntu and Debian systems

dtOut = 0.1 # (s) communication output interval for Core to send to viz, database, and csv logfile
coreCollision = 0 # (0/1) perform collision detection and reporting in [main_core | all_vehicles]?

# incorporate core viz outputs:
vizUdpEnabled=True
vizIp   = localhost
#vizIp   = 10.33.106.227 # Garrett's ip on wireless network
#vizIp   = 155.31.130.51 # Garrett's ip on wired network
vizPort = 5555

dashIp   = localhost
dashPort = 5556

# MongoDB interface
#dbUdpEnabled=False
#dbIp   = 10.33.106.103 # Otto's laptop IP on EagleNET
#dbIp   = 155.31.130.169 # Otto's laptop IP on EagleNET
#dbPort = 5000

# enable ADS-B with True below then start the ADSB sender: fileWatcher_ADSB_decoder.py
adsbEnabled=True # enable dedicated udp socket to listen on this port for individual ADS-B messages from fileWatcher_ADSB_decoder.py
adsbPort = 10000 # listen on this port from anyone

# ------------------------------------------------------------------------------
[veh_v2v]
v2vEnabled=True # (True/False) enable or disable v2v threads and multicast udp/ip communications
multicast_group = 224.3.3.3 # ipv4 addresses 224.0.0.0 through 230.255.255.255 are reserved for multicast traffic
multicast_port  = 10001 # udp multicast port for all v2v senders and listeners
v2v_cint_send   = 2.0 # (s) outbound v2v communication interval for broadcasting a single vehicle's update to all other vehicles
v2v_cint_read   = 1.0 # (s) receiver's observation interval of complete v2vState snapshot; read more frequently than sending! --> make sure (v2v_cint_read)<(v2v_cint_send)
v2v_staleTime   = 3.0 # (s) duration to wait before declaring a v2vState[] entry stale, which means a v2vState[vid] has not been received and data is old, or stale
v2v_dRadio      = 500 # (m) radio range; any vehicles within this distance are radio active
v2v_dNeighViz   = 50  # (m) neighbor range; any vehicles within this distance are visual neighbors
v2v_dNeighClose = 25  # (m) neighbor range; any vehicles within this distance are visual and also very close neighbors (too close)
v2v_printLevel  = 0 #2 #1 # (0/1/2) how much to print v2v updates in the veh model console output
v2v_showRadii   = 0 # (0/1) graphics flag to illustrate both Viz and Close radii in move_live_mapping.py
v2v_showVector  = 1 # (0/1) graphics flag to illustrate CG vector in move_live_mapping.py


# ------------------------------------------------------------------------------
[veh_sim]

vehRuntimeDir_base=/tmp/move # rsync veh_model to remote machine for execution in this folder + username, note: don't put this in /run/shm b/c it's a ramdisk where logging can consume all ram
h = 0.010 # (s) vehicle dynamics RK4 integration stepsize; choose cInt and h such that they are an integer ratio, say cInt/h=10
cInt=0.1 # (s) vehicle-to-core reporting interval; local .csv logging interval; 0.1 is 10Hz, 0.5 is 2Hz
hInt = 1.0 # (s) vehicle-to-core hearbeat interval; only relevant during runState==1 (READY)
randFlag=1 # (0/1) use random numbers? plumbed in everwhere relevant (ICs and random triggers in behaviors)

IC_sel = 1 # (1/2/3) global default: 1->straight down the X-axis, 2-> grid pattern within geom extents, 3->collision scenario for 2 vehicles
L_char = 2.0 # (m) characteristic vehicle length
v_max = 26.8 # (m/s) nominal max vehicle speed (26.8m/s=60mph)

IC_elev = 0.0 # (m) default height for all vehicles in UTM XYZ coordinate frame
lat_origin =  29.193111 # (decimal deg) Home plate, Baseball fields, Embry-Riddle Aeronautical Univ, Daytona Beach, Florida
lon_origin = -81.046171 # (decimal deg)


log_level: 2 # (0/1/2) 0->no logging, 1-> .csv file only, 2-> .csv and capture console output in /tmp/debug*.txt for debugging veh model and behavior
             # log_level==1 -> monitor veh console output by reattaching to screen with 'screen -r' to watch running vehicle process
             # log_level==2 -> monitor veh console output with 'tail -f /tmp/debug100.txt' to monitor vehicle model console output (screen holds the process but has no output to display)
logfile_loc_veh: /tmp #/run/shm #/tmp, note: careful! you can crash your machine using /run/shm b/c is a ramdisk by default on Ubuntu and Debian systems


# vehicle types:
#   'pedestrian' vehSubType--> 'male' or 'female'
#   'ground',    vehSubType--> 'onroad' or 'offroad'
#   'aerial',    vehSubType--> 'fixedwing' or 'rotorcraft'
#   'surface'
#   'underwater'


# only change global defaults for these specific vehicles
[veh_details] # optionally assign: vehicle name, vehType, vehSubType, initial conditions
# note: as of v1.07 only move_live_mapping_v3.py respects the vehType and vehSubType
#veh_type_default: { 'vehType': 'pedestrian',  'vehSubType': 'none' }
#veh_type_default: { 'vehType': 'ground',      'vehSubType': 'none' }
veh_type_default: { 'vehType': 'aerial',      'vehSubType': 'fixedwing' }
#veh_type_default: { 'vehType': 'surface',     'vehSubType': 'none' }
#veh_type_default: { 'vehType': 'underwater',  'vehSubType': 'turtle' }

vid_100: {'name':'Newton',      'vehType': 'pedestrian', 'vehSubType':'male',   'IC_latlon':'29.193111, -81.046171',  'IC_elev': 0.0, 'IC_yaw_rad':  0.0, 'IC_pt': 'first' } # start at home plate, run to 1st
vid_101: {'name':'Malala',      'vehType': 'pedestrian', 'vehSubType':'female', 'IC_latlon':'29.193159, -81.045979',  'IC_elev': 0.0, 'IC_yaw_rad':  2.1, 'IC_pt': 'second' } # start at 1st, run to 2nd
vid_102: {'name':'Davinci',     'vehType': 'pedestrian', 'vehSubType':'male',   'IC_latlon':'29.193329, -81.046032',  'IC_elev': 0.0, 'IC_yaw_rad':  3.3, 'IC_pt': 'third' } # start at 2nd, run to 3rd
vid_103: {'name':'AngeMerkel',  'vehType': 'pedestrian', 'vehSubType':'female', 'IC_latlon':'29.193279, -81.046226',  'IC_elev': 0.0, 'IC_yaw_rad': -1.3, 'IC_pt': 'home' } # start at 3rd, run home



# ------------------------------------------------------------------------------
# prioritize or disable (set to 0) behaviors for all vehicles; see [veh_behaviors_individual] for vehicle-specific behavior settings
# note #1: higher priority takes precendence over lower; priority=0 disables
# note #2: these names are case-sensitive and must match exact function names as defined in vehicle's behaviors.py
[veh_behaviors_global]
wander          : 0 # default is to turn everything off globally
periodicTurn    : 0
periodicPitch   : 0
stayInBounds    : 0
avoidBySteer    : 0 # for aerial vehicles to avoid collision using *steer*
avoidByBrake    : 0 # for ground vehicles to avoid collision using longitudinal (braking) (slow down to not hit the veh in front)
detectAndReport : 0 # todo: convert this to (or add) followMe() behavior
followRoute     : 0 
goToGate        : 0 
goToPoint       : 0 
multiRotorHover : 0 # UAV equivalent to default wander process; lowest priority; when nothing else is happening, just hover



# per-vehicle behavior priorities override global settings in [veh_behaviors_global]
[veh_behaviors_individual] # enable behaviors for individual vehicles
vid_100: {'multiRotorHover': 1, 'goToPoint': 5, 'stayInBounds': 10, 'avoidBySteer': 12 }
vid_101: {'multiRotorHover': 1, 'goToPoint': 5, 'stayInBounds': 10, 'avoidBySteer': 12 }
vid_102: {'multiRotorHover': 1, 'goToPoint': 5, 'stayInBounds': 10, 'avoidBySteer': 12 }
vid_103: {'multiRotorHover': 1, 'goToPoint': 5, 'stayInBounds': 10, 'avoidBySteer': 12 }





# define points for use with goToPoint() or the mission sequencer action 'goToPoint'
[points] # points of interest in mission commands
home:   {'lat': 29.193111, 'lon': -81.046171, 'elev': 0.000000, 'nextPoint': 'first',  'vel':1.5 } # (dec deg) and (m), ERAU baseball field, home plate
first:  {'lat': 29.193159, 'lon': -81.045979, 'elev': 0.000000, 'nextPoint': 'second', 'vel':3.5 } # (dec deg) and (m), ERAU baseball field, first base
second: {'lat': 29.193329, 'lon': -81.046032, 'elev': 0.000000, 'nextPoint': 'third',  'vel':1.5 } # (dec deg) and (m), ERAU baseball field, second base
third:  {'lat': 29.193279, 'lon': -81.046226, 'elev': 0.000000, 'nextPoint': 'home',   'vel':3.5 } # (dec deg) and (m), ERAU baseball field, third base

pt_B0: {'lat': 29.193184, 'lon': -81.045712, 'elev': 0.000000, 'nextPoint': 'pt_B1', 'vel':3.0 } # (dec deg) and (m), ERAU baseball field, right field sideline
pt_B1: {'lat': 29.193255, 'lon': -81.045649, 'elev': 0.000000, 'nextPoint': 'pt_B2', 'vel':3.0 } # (dec deg) and (m), ERAU baseball field, soccer corner kick
pt_B2: {'lat': 29.193449, 'lon': -81.045656, 'elev': 0.000000, 'nextPoint': 'pt_B3', 'vel':3.0 } # (dec deg) and (m), ERAU baseball field, soccer free kick penalty mark
pt_B3: {'lat': 29.193337, 'lon': -81.045813, 'elev': 0.000000, 'nextPoint': 'pt_B1', 'vel':3.0 } # (dec deg) and (m), ERAU baseball field, soccer 1/4 field sideline







# define gates; to enable gates, the goToGate() behavior must be turned on;
# gates must be defined with Point A on the left and Point B on the right, plus an initial gate assignment must be made
[gates] # gate_NNN: [ <lon_A>, <lat_A>,    <lon_B>, <lat_B>,    <nextGate> ] # this makes a gate with point A at (lat_A,lon_A) and point B at (lat_B,lon_B)
#gate_1: {'ptA_lon': -81.046308, 'ptA_lat': 29.193577,    'ptB_lon': -81.046126, 'ptB_lat': 29.193865,   'nextGate': 2 }   # gate 1 at West end of ERAU Baseball field, Goto gate 2
#gate_2: {'ptA_lon': -81.045650, 'ptA_lat': 29.193255,    'ptB_lon': -81.045468, 'ptB_lat': 29.193541,   'nextGate': 1 }   # gate 2 at East end of ERAU Baseball field, Goto gate 1 (this causes the ping pong)
#gate_1: {'ptA_lon': -81.048414, 'ptA_lat': 29.191032,    'ptB_lon': -81.046126, 'ptB_lat': 29.193865,   'nextGate': 2 }   # gate 1 at West end of ERAU Baseball field, Goto gate 2
#gate_2: {'ptA_lon': -81.045650, 'ptA_lat': 29.193255,    'ptB_lon': -81.048414, 'ptB_lat': 29.191032,   'nextGate': 1 }   # gate 2 at East end of ERAU Baseball field, Goto gate 1 (this causes the ping pong)







# a mission table is constructed from rows of commands, for a particular vehicle identifier (vid)
# labels prior to the ':' have no influence but must be unique; they do not need to be ordered
# each vid's set of commands are read (in any order) and sorted by missionState
# required: 'vid', 'missionState', 'action'; all others may or may not contain action-specific keys
[missionCommands]
# BabeRuth
#missionCmd100: {'vid':100, 'missionState':  1 , 'action':'goToMissionState', 'newMissionState':   25 , 'maxIterations': 1 } # 'maxIterations' is max number of goTo returns to 'newMissionState'; maxIt is only relevant if the goTo is to a prior missionState that returns to this missionState
missionCmd00:  {'vid':100, 'missionState':  2 , 'action':'waitElapsed',      'eTimeThresh'    :  4.0 } # (s) wait this many elapsed seconds within the mission sequencer
missionCmd01:  {'vid':100, 'missionState': 10 , 'action':'goToPoint',        'point'          : 'first',  'speed': 3, 'riseRate': 3, 'rThresh': 3.0, 'arrivalType':'stop' } # point names much match [location] names above; 'arrivalType'={'stop','passThru'}, 'rThresh' is arrival radius in meters
missionCmd02:  {'vid':100, 'missionState': 15 , 'action':'waitElapsed',      'eTimeThresh'    :  4.0 } # (s) wait this many elapsed seconds within the mission sequencer
missionCmd03:  {'vid':100, 'missionState': 20 , 'action':'goToPoint',        'point'          : 'second',  'speed': 3, 'riseRate': 3, 'rThresh': 3.0, 'arrivalType':'stop' } # point names much match [location] names above; 'arrivalType'={'stop','passThru'}, 'rThresh' is arrival radius in meters
missionCmd04:  {'vid':100, 'missionState': 25 , 'action':'waitElapsed',      'eTimeThresh'    :  4.0 } # (s) wait this many elapsed seconds within the mission sequencer
missionCmd05:  {'vid':100, 'missionState': 30 , 'action':'goToPoint',        'point'          : 'third',  'speed': 3, 'riseRate': 3, 'rThresh': 3.0, 'arrivalType':'stop' } # point names much match [location] names above; 'arrivalType'={'stop','passThru'}, 'rThresh' is arrival radius in meters
missionCmd06:  {'vid':100, 'missionState': 35 , 'action':'waitElapsed',      'eTimeThresh'    :  4.0 } # (s) wait this many elapsed seconds within the mission sequencer
missionCmd07:  {'vid':100, 'missionState': 40 , 'action':'goToPoint',        'point'          : 'home',  'speed': 3, 'riseRate': 3, 'rThresh': 3.0, 'arrivalType':'stop' } # point names much match [location] names above; 'arrivalType'={'stop','passThru'}, 'rThresh' is arrival radius in meters
missionCmd08:  {'vid':100, 'missionState':100 , 'action':'goToMissionState', 'newMissionState':    2 , 'maxIterations': 200 } # 'maxIterations' is max number of goTo returns to 'newMissionState'; maxIt is only relevant if the goTo is to a prior missionState that returns to this missionState
missionCmd09:  {'vid':100, 'missionState':200 , 'action':'waitElapsed',      'eTimeThresh'    :  4.0 } # (s) wait this many elapsed seconds within the mission sequencer

# Malala
missionCmd101: {'vid':101, 'missionState':  1 , 'action':'goToMissionState', 'newMissionState':   15 , 'maxIterations': 1 } # 'maxIterations' is max number of goTo returns to 'newMissionState'; maxIt is only relevant if the goTo is to a prior missionState that returns to this missionState
missionCmd10:  {'vid':101, 'missionState':  2 , 'action':'waitElapsed',      'eTimeThresh'    :  3.0 } # (s) wait this many elapsed seconds within the mission sequencer
missionCmd11:  {'vid':101, 'missionState': 10 , 'action':'goToPoint',        'point'          : 'first',  'speed': 3, 'riseRate': 3, 'rThresh': 3.0, 'arrivalType':'stop' } # point names much match [location] names above; 'arrivalType'={'stop','passThru'}, 'rThresh' is arrival radius in meters
missionCmd12:  {'vid':101, 'missionState': 15 , 'action':'waitElapsed',      'eTimeThresh'    :  3.0 } # (s) wait this many elapsed seconds within the mission sequencer
missionCmd13:  {'vid':101, 'missionState': 20 , 'action':'goToPoint',        'point'          : 'second',  'speed': 3, 'riseRate': 3, 'rThresh': 3.0, 'arrivalType':'stop' } # point names much match [location] names above; 'arrivalType'={'stop','passThru'}, 'rThresh' is arrival radius in meters
missionCmd14:  {'vid':101, 'missionState': 25 , 'action':'waitElapsed',      'eTimeThresh'    :  3.0 } # (s) wait this many elapsed seconds within the mission sequencer
missionCmd15:  {'vid':101, 'missionState': 30 , 'action':'goToPoint',        'point'          : 'third',  'speed': 3, 'riseRate': 3, 'rThresh': 3.0, 'arrivalType':'stop' } # point names much match [location] names above; 'arrivalType'={'stop','passThru'}, 'rThresh' is arrival radius in meters
missionCmd16:  {'vid':101, 'missionState': 35 , 'action':'waitElapsed',      'eTimeThresh'    :  3.0 } # (s) wait this many elapsed seconds within the mission sequencer
missionCmd17:  {'vid':101, 'missionState': 40 , 'action':'goToPoint',        'point'          : 'home',  'speed': 3, 'riseRate': 3, 'rThresh': 3.0, 'arrivalType':'stop' } # point names much match [location] names above; 'arrivalType'={'stop','passThru'}, 'rThresh' is arrival radius in meters
missionCmd18:  {'vid':101, 'missionState':100 , 'action':'goToMissionState', 'newMissionState':    2 , 'maxIterations': 200 } # 'maxIterations' is max number of goTo returns to 'newMissionState'; maxIt is only relevant if the goTo is to a prior missionState that returns to this missionState
missionCmd19:  {'vid':101, 'missionState':200 , 'action':'waitElapsed',      'eTimeThresh'    :  3.0 } # (s) wait this many elapsed seconds within the mission sequencer

# HankAaron
missionCmd102: {'vid':102, 'missionState':  1 , 'action':'goToMissionState', 'newMissionState':   25 , 'maxIterations': 1 } # 'maxIterations' is max number of goTo returns to 'newMissionState'; maxIt is only relevant if the goTo is to a prior missionState that returns to this missionState
missionCmd20:  {'vid':102, 'missionState':  2 , 'action':'waitElapsed',      'eTimeThresh'    :  2.0 } # (s) wait this many elapsed seconds within the mission sequencer
missionCmd21:  {'vid':102, 'missionState': 10 , 'action':'goToPoint',        'point'          : 'first',  'speed': 3, 'riseRate': 3, 'rThresh': 3.0, 'arrivalType':'stop' } # point names much match [location] names above; 'arrivalType'={'stop','passThru'}, 'rThresh' is arrival radius in meters
missionCmd22:  {'vid':102, 'missionState': 15 , 'action':'waitElapsed',      'eTimeThresh'    :  2.0 } # (s) wait this many elapsed seconds within the mission sequencer
missionCmd23:  {'vid':102, 'missionState': 20 , 'action':'goToPoint',        'point'          : 'second',  'speed': 3, 'riseRate': 3, 'rThresh': 3.0, 'arrivalType':'stop' } # point names much match [location] names above; 'arrivalType'={'stop','passThru'}, 'rThresh' is arrival radius in meters
missionCmd24:  {'vid':102, 'missionState': 25 , 'action':'waitElapsed',      'eTimeThresh'    :  2.0 } # (s) wait this many elapsed seconds within the mission sequencer
missionCmd25:  {'vid':102, 'missionState': 30 , 'action':'goToPoint',        'point'          : 'third',  'speed': 3, 'riseRate': 3, 'rThresh': 3.0, 'arrivalType':'stop' } # point names much match [location] names above; 'arrivalType'={'stop','passThru'}, 'rThresh' is arrival radius in meters
missionCmd26:  {'vid':102, 'missionState': 35 , 'action':'waitElapsed',      'eTimeThresh'    :  2.0 } # (s) wait this many elapsed seconds within the mission sequencer
missionCmd27:  {'vid':102, 'missionState': 40 , 'action':'goToPoint',        'point'          : 'home',  'speed': 3, 'riseRate': 3, 'rThresh': 3.0, 'arrivalType':'stop' } # point names much match [location] names above; 'arrivalType'={'stop','passThru'}, 'rThresh' is arrival radius in meters
missionCmd28:  {'vid':102, 'missionState':100 , 'action':'goToMissionState', 'newMissionState':    2 , 'maxIterations': 200 } # 'maxIterations' is max number of goTo returns to 'newMissionState'; maxIt is only relevant if the goTo is to a prior missionState that returns to this missionState
missionCmd29:  {'vid':102, 'missionState':200 , 'action':'waitElapsed',      'eTimeThresh'    :  2.0 } # (s) wait this many elapsed seconds within the mission sequencer

# AngelMerkel
missionCmd103: {'vid':103, 'missionState':  1 , 'action':'goToMissionState', 'newMissionState':   35 , 'maxIterations': 1 } # 'maxIterations' is max number of goTo returns to 'newMissionState'; maxIt is only relevant if the goTo is to a prior missionState that returns to this missionState
missionCmd30:  {'vid':103, 'missionState':  2 , 'action':'waitElapsed',      'eTimeThresh'    :  1.0 } # (s) wait this many elapsed seconds within the mission sequencer
missionCmd31:  {'vid':103, 'missionState': 10 , 'action':'goToPoint',        'point'          : 'first',  'speed': 3, 'riseRate': 3, 'rThresh': 3.0, 'arrivalType':'stop' } # point names much match [location] names above; 'arrivalType'={'stop','passThru'}, 'rThresh' is arrival radius in meters
missionCmd32:  {'vid':103, 'missionState': 15 , 'action':'waitElapsed',      'eTimeThresh'    :  1.0 } # (s) wait this many elapsed seconds within the mission sequencer
missionCmd33:  {'vid':103, 'missionState': 20 , 'action':'goToPoint',        'point'          : 'second',  'speed': 3, 'riseRate': 3, 'rThresh': 3.0, 'arrivalType':'stop' } # point names much match [location] names above; 'arrivalType'={'stop','passThru'}, 'rThresh' is arrival radius in meters
missionCmd34:  {'vid':103, 'missionState': 25 , 'action':'waitElapsed',      'eTimeThresh'    :  1.0 } # (s) wait this many elapsed seconds within the mission sequencer
missionCmd35:  {'vid':103, 'missionState': 30 , 'action':'goToPoint',        'point'          : 'third',  'speed': 3, 'riseRate': 3, 'rThresh': 3.0, 'arrivalType':'stop' } # point names much match [location] names above; 'arrivalType'={'stop','passThru'}, 'rThresh' is arrival radius in meters
missionCmd36:  {'vid':103, 'missionState': 35 , 'action':'waitElapsed',      'eTimeThresh'    :  1.0 } # (s) wait this many elapsed seconds within the mission sequencer
missionCmd37:  {'vid':103, 'missionState': 40 , 'action':'goToPoint',        'point'          : 'home',  'speed': 3, 'riseRate': 3, 'rThresh': 3.0, 'arrivalType':'stop' } # point names much match [location] names above; 'arrivalType'={'stop','passThru'}, 'rThresh' is arrival radius in meters
missionCmd38:  {'vid':103, 'missionState':100 , 'action':'goToMissionState', 'newMissionState':    2 , 'maxIterations': 200 } # 'maxIterations' is max number of goTo returns to 'newMissionState'; maxIt is only relevant if the goTo is to a prior missionState that returns to this missionState
missionCmd39:  {'vid':103, 'missionState':200 , 'action':'waitElapsed',      'eTimeThresh'    :  1.0 } # (s) wait this many elapsed seconds within the mission sequencer

#missionCmd1:  {'vid':100, 'missionState':  1 , 'action':'goToPoint',        'point'          : 'pt_A1' } # point names much match [location] names above
#missionCmd2:  {'vid':100, 'missionState': 10 , 'action':'waitOnVeh',        'otherVeh'       :  101,     'progThresh': 11.0   } # otherVeh's progress must be >= threshold
#missionCmd3:  {'vid':100, 'missionState': 20 , 'action':'goToPoint',        'point'          : 'pt_A2' } # point names much match [location] names above
#missionCmd4:  {'vid':100, 'missionState': 30 , 'action':'goToPoint',        'point'          : 'pt_A3' } # point names much match [location] names above
#missionCmd5:  {'vid':100, 'missionState':100 , 'action':'goToMissionState', 'newMissionState':    1 }
#missionCmd10: {'vid':100, 'missionState': 40 , 'action':'waitOnVeh',        'otherVeh'       :  101,     'progThresh': 31.0   } # otherVeh's progress must be >= threshold
# actionWhenComplete:'missionState=3' is achieved by the next command
# what is implied is to advance to the next mission command in the mission table.

#missionCmd17:  {'vid':101, 'missionState':  1 , 'action':'waitOnVeh',        'otherVeh'       :  100,     'progThresh': 2.0   } # otherVeh's progress must be >= threshold
#missionCmd18:  {'vid':101, 'missionState': 10 , 'action':'goToPoint',        'point'          : 'pt_B1' } # point names much match [location] names above
#missionCmd19:  {'vid':101, 'missionState': 20 , 'action':'goToPoint',        'point'          : 'pt_B2' } # point names much match [location] names above
#missionCmd20: {'vid':101, 'missionState': 30 , 'action':'goToPoint',        'point'          : 'pt_B3' } # point names much match [location] names above
#missionCmd21: {'vid':101, 'missionState': 40 , 'action':'waitOnVeh',        'otherVeh'       :  100,     'progThresh': 31.0   } # otherVeh's progress must be >= threshold
#missionCmd22: {'vid':101, 'missionState':100 , 'action':'goToMissionState', 'newMissionState':    1 }








# ------------------------------------------------------------------------------
[veh_route_assignments] # vid_NNN: [ <route_number>, <nLaps>, <startOption>, <startingWaypoint>, <endOption> ]
#
# each vehicle wakes up and looks here to determine if and what route to follow
# note: make sure the followRoute() behavior has non-zero priority so it gets started
#
# format:
#     vid_NNN:                                      # vehicle identifier, vid_100, vid_101, ...
#              {'routeScript':'<route_file.py>',    # python script defining route (see ./routes)
#               'nLapsMax':<num>,                   # max number of laps, then trigger end option
#               'startOption':'startAt',            # either 'startAt' or 'goTo' (only 'startAt' is working now)
#               'startingWaypoint':  <num>,         # starting waypoint
#               'lapRestartWaypoint':<num>,         # restarting waypoint number after each lap
#               'endOption':'stop',                 # either 'stop' or 'continue'
#               'mapColor':'<color>' }              # for example, 'magenta', 'red'
#
#vid_100: {'routeScript':'route_01_waypoints.py', 'nLapsMax':19999, 'startOption':'startAt', 'startingWaypoint': 1, 'lapRestartWaypoint':1, 'endOption':'stop', 'mapColor':'yellow' }
#vid_101: {'routeScript':'route_01_waypoints.py', 'nLapsMax':29999, 'startOption':'startAt', 'startingWaypoint': 5, 'lapRestartWaypoint':1, 'endOption':'stop', 'mapColor':'yellow' }
#vid_102: {'routeScript':'route_01_waypoints.py', 'nLapsMax':39999, 'startOption':'startAt', 'startingWaypoint': 9, 'lapRestartWaypoint':1, 'endOption':'stop', 'mapColor':'yellow' }





[default_display_excludes]
# these strings must match [titles] list entries in move_dashboard.py | [col_titles]
exclude_table_col_0 = 'msgType'
#exclude_table_col_1 = 'vid'
#exclude_table_col_2 = 'vehName'
#exclude_table_col_3 = 'vehType'
#exclude_table_col_4 = 'runState'
exclude_table_col_5 = 'X'
exclude_table_col_6 = 'Y'
#exclude_table_col_7 = 'Z'
#exclude_table_col_8 = 'psi'
#exclude_table_col_9 = 'lat'
#exclude_table_col_10 = 'lon'
exclude_table_col_11 = 'Xd'
exclude_table_col_12 = 'Yd'
exclude_table_col_13 = 'Zd'
exclude_table_col_14 = 'psiDot'
#exclude_table_col_15 = 'spd_mps'
#exclude_table_col_16 = 'tStamp'
exclude_table_col_17 = 'vehSubType'
exclude_table_col_18 = 'msg_cnt_in'
exclude_table_col_19 = 'msg_cnt_out'
exclude_table_col_20 = 'msg_errorCnt'
exclude_table_col_21 = 'srt_margin_avg'
exclude_table_col_22 = 'srt_err_avg'
exclude_table_col_23 = 't'
#exclude_table_col_24 = 'gps_unixTime'
exclude_table_col_25 = 'u'
exclude_table_col_26 = 'bIdCmd'
exclude_table_col_27 = 'bIdName'
exclude_table_col_28 = 'bIdProgress'
exclude_table_col_29 = 'missionAction'
#exclude_table_col_30 = 'missionState'
#exclude_table_col_31 = 'missionPctComplete'
#exclude_table_col_32 = 'missionProgress'
exclude_table_col_33 = 'missionLastCmdTime'
#exclude_table_col_34 = 'missionStatus'
#exclude_table_col_35 = 'missionCounter'
exclude_table_col_36 = 'L_char'
exclude_table_col_37 = 'hdg'
exclude_table_col_38 = 'mac'
#exclude_table_col_39 = 'batt_stat'



[veh_names_builtins]
name1: doc
name2: bashful
name3: dopey
name4: grumpy
name5: happy
name6: sleepy
name7: sneezy
name8: mickey
name9: minnie
name10: daffy
#name11: donald
#name12: daisy
#name13: bambi
#name14: thumper
#name15: scrooge
#name16: pluto
#name17: mercury
#name18: venus
#name19: earth
#name20: mars
#name21: jupiter
#name22: saturn
#name23: uranus
#name24: neptune
# Ninja turtles: Leonardo da Vinci, Michelangelo, Raphael, and Donatello
# Marvel characters: SpiderMan, Thanos, IronMan, DeadPool, BlackWidow, CaptainAmerica, Hulk, Wolverine, Thor, BlackPanther, Thing, Loki, Wasp, Quicksilver, Nebula


[veh_names_live_gps]
name1: garrett
name2: otto
name3: roberto
name4: tyler
name5: shelby
name6: compere
