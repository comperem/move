Steps to making a vehicle follow a new route in MoVE:

(1) choose how the new route will be specified:
    (a) by a mathematical description
        --> see route_01_waypoints.py as an example with a circle radius and array of angles
    (b) by a sequence of lat/lon waypoints
        --> see route_04_waypoints.py or route_foco_park_exterior_waypoints.py

(2) edit the new waypoints file in ./routes and make sure it runs properly at 
    the command line:
        python3 my_new_route.py
    A window should pop up with a graphic illustrating the plot waypoints.
    If this fails, adjust until this runs correctly. This python .py file must
    run stand-alone without errors before proceeding.

(3) determine which scenario and vehicle(s) will use this route.
    This means either:
    (a) using a .cfg file in ./scenario
        or
    (b) creating a new .cfg in ./scenario folder

(4) in either case in the previous step, ensure there is at least one vehicle
    designated to use this route by specifying the route's python .py file
    in [veh_route_assignments]

(5) follow other steps described in:
        00_Readme_on_making_a_new_scenario.txt




---
Marc Compere, comperem@gmail.com
created : 03 Aug 2020
modified: 03 Aug 2020

