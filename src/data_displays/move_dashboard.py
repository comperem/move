#!/usr/bin/env python3
#
# move_dashboard.py - dashboard gui to start and monitor MoVE experiments defined
#                     by scenario configuration files
#
# this version was updated in v1.16 to display separate move and adsb tables
#
# based on: radioButtonGroup_example_5.py, dataFrame_to_dataTable_example_f.py
#
# the simplest way to run is the bash shell script:  ./run_move_dashboard.sh
#
# or run directly with:     bokeh serve --show move_dashboard.py
#
# Marc Compere, comperem@gmail.com
# created : 01 May 2020
# modified: 08 Apr 2023
#
# ---
# Copyright 2018 - 2023 Marc Compere
#
# This file is part of the Mobility Virtual Environment (MoVE).
#
# MoVE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3
# as published by the Free Software Foundation.
#
# MoVE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License version 3 for more details.
#
# A copy of the GNU General Public License is included with MoVE
# in the COPYING file, or see <https://www.gnu.org/licenses/>.
# ---


import os
import sys
import subprocess
from queue import Queue
from threading import Thread, Event
from collections import deque
from datetime import date, datetime
import time
from random import randint
import logging
import socket
import msgpack

from bokeh.plotting import figure, show, curdoc
#from bokeh.io import curdoc
from bokeh.models import Paragraph, RadioButtonGroup, Select, ColumnDataSource, Toggle, Button, StringEditor
from bokeh.models import DateFormatter, NumberFormatter, Select, HTMLTemplateFormatter
from bokeh.models.widgets import Div, DataTable, TableColumn
from bokeh.models.widgets    import DateFormatter
from bokeh.models.formatters import DatetimeTickFormatter
from bokeh.layouts import column, layout, row
from bokeh.models import CustomJS, MultiChoice
sys.path.append(os.path.relpath("../scenario")) # find ../scenario/readConfigFile.py w/o (a) being a package or (b) using linux file system symbolic link
from readConfigFile import readConfigFile # for displaying correct columns
import pprint

import pandas as pd
import copy # copy.deepcopy()


logging.basicConfig(level=logging.DEBUG,format='[%(levelname)s] (%(threadName)-14s) %(message)s',)

debug=1 # (0/1/2/3) 0->none, 1->some, 2->more, 3->lots


# ---------------------------------------------------------------------
#screen_width=1200
screen_width=900
description = Div(background='lightgreen', text="""<b><code>move_dashboard.py</code></b> - MoVE GUI dashboard data display with command buttons""", width=screen_width)
uptime_bar = Div(background='lightskyblue' , width=screen_width, text="", render_as_text=True)




# ---------------------------------------------------------------------
#print('\n\n\tcwd={}\n'.format( os.getcwd() ))
MOVE_HOME=os.path.dirname(os.getcwd()) # MOVE_HOME should be base of MoVE; os.path.dirname() gives parent directory
#MOVE_HOME=os.getcwd() # MOVE_HOME is base of MoVE
print('\n\n\tMOVE_HOME=[{}]\n'.format(MOVE_HOME))
CFG_DIR=MOVE_HOME+'/scenario'
CORE_DIR=MOVE_HOME+'/core'
DISPLAY_DIR=MOVE_HOME+'/data_displays'
global cfgFile

def scan_all_cfg_files():
    cfg_list=[] # init empty list
    print('\tscanning config files in [{0}]'.format(CFG_DIR))
    for file in os.listdir( CFG_DIR ):
        if file.endswith(".cfg"): # the .endswith() is a string manipulation built-in
            print('\t {0}'.format(file))
            cfg_list.append(file)
            #print(os.path.join( CFG_DIR , file))
    cfg_list.sort() # make file list display in alphabetical order
    #print('cfg_list={0}'.format(cfg_list))
    return cfg_list

# pull-down for scenario select
def select_handler(attr, old, new):
    print("\tScenario selection: " + new)

cfg_list = scan_all_cfg_files() # make list the first time for Select pull-down
scenario_file_select = Select(title="Scenario selection:", value="default.cfg", options=cfg_list)
scenario_file_select.on_change('value',select_handler)
scenario_file_select.height_policy='fit'
scenario_file_select.height=80


# ---------------------------------------------------------------------
def core_ON_OFF_handler(attr, old, new):
    print('\tbutton: Radio ON_OFF ' + str(new) + ' selected.')
    #print('RSGPS_buttons={0}'.format(dir(RSGPS_buttons)))
    #RSGPS_buttons.visible = bool(new)
    if new==1: # enable button to ON state
        print('\tbutton: launching main_core.py process')
        qProcButton.put(3) # press the button, launch ./main_core.py
        eRSGPS.clear() # set to False; this causes periodic callback to auto-set Ready-Set-Go-Pause-Stop button from latest udp message
        core_ON_OFF.button_type='success' # options: default (grey), primary (tiel), success (green), warning (orange), danger (red)
    else:
        print('\tbutton: stopping main_core.py process')
        qProcButton.put(4) # press the button, terminate ./main_core.py
        core_ON_OFF.button_type='primary'

core_ON_OFF = RadioButtonGroup(labels=["Core OFF", "Core ON"], active=0)
core_ON_OFF.height_policy='fit'
core_ON_OFF.height=80
core_ON_OFF.button_type='primary' # options: default (grey), primary (tiel), success (green), warning (orange), danger (red)
#core_ON_OFF.margin=(100,100,100,100)
#core_ON_OFF.width_policy='fit'
#core_ON_OFF.width=100
core_ON_OFF.on_change('active',core_ON_OFF_handler)






# ---------------------------------------------------------------------
# RSGPS --> Ready, Set, Go, Pause, Stop
def RSGPS_radio_handler(attr, old, new):
    print('\tRadio button option ' + str(new) + ' selected.')
    print('\teRSGPS.is_set()==[{0}]'.format(eRSGPS.is_set()))
    if new==0:
        RSGPS_buttons.button_type='primary' # ready / tiel
        if eRSGPS.is_set()==True:
            qProcButton.put(6) # press the button, issue commands for all vehicles to enter runState 1, or READY
    if new==1:
        RSGPS_buttons.button_type='warning' # set / orange
        if eRSGPS.is_set()==True:
            qProcButton.put(7) # press the button, issue commands for all vehicles to enter runState 1, or SET
    if new==2:
        RSGPS_buttons.button_type='success' # go / green
        if eRSGPS.is_set()==True:
            qProcButton.put(8) # press the button, issue commands for all vehicles to enter runState 1, or GO
    if new==3:
        RSGPS_buttons.button_type='default' # pause / grey
        if eRSGPS.is_set()==True:
            qProcButton.put(9) # press the button, issue commands for all vehicles to enter runState 1, or PAUSE
    if new==4:
        RSGPS_buttons.button_type='danger'  # stop / red
        veh_launch_toggle.disabled=False # re-enable launch button
        veh_launch_toggle.active=False # set to Un-launched state
        scenario_lock_toggle.disabled=False # you could select a different scenario file
        if eRSGPS.is_set()==True:
            print('sending qProcButton.put(10)')
            qProcButton.put(10) # press the button, issue commands for all vehicles to enter runState 1, or STOP
    
        
    
    
RSGPS_buttons = RadioButtonGroup(
        labels=["Ready", "Set", "Go", "Pause", "Stop"], active=0)
#RSGPS_buttons.disabled=True
RSGPS_buttons.height_policy='fit'
RSGPS_buttons.height=80
RSGPS_buttons.button_type='primary' # ready / tiel
RSGPS_buttons.on_change('active',RSGPS_radio_handler)
RSGPS_buttons.disabled=True # start disabled; enable if (a) launch vehicles or (b) udp sees Core with running vehicles in vehicle table



# ---------------------------------------------------------------------
def veh_launch_toggle_handler(attr, old, new):
    print('\tveh_launch_toggle value: [' + str(new) + ']')
    if new is True:
        scenario_lock_toggle.disabled=True # cannot change scenario while processes are launched
        RSGPS_buttons.active=0
        RSGPS_buttons.disabled=False
        qProcButton.put(5) # press the button, run ./main_launch_veh_process.py
        veh_launch_toggle.label='Vehicle processes: Launched'
        veh_launch_toggle.button_type='success' # options: default (grey), primary (tiel), success (green), warning (orange), danger (red)
        veh_launch_toggle.disabled=True # cannot just 'unlaunch' - must issue Stop message
        eRSGPS.set() # this enables buttons for commanding runState changes with main_changeRunState.py (without Core running)
    else:
        veh_launch_toggle.label='Press to Launch Vehicle Processes'
        veh_launch_toggle.button_type='default'

veh_launch_toggle = Toggle(label="Press to Launch Vehicle Processes", button_type='primary')
veh_launch_toggle.on_change('active',veh_launch_toggle_handler)
veh_launch_toggle.height_policy='fit'





# ==============================================================================
# =         BEGIN:   web console output and subprocess execution               =
# ==============================================================================
doc=curdoc() # make a copy so bokeh server and thread use same curdoc()

template="""<div style="background:<%= "wheat" %>"> <%= value %> </div>"""
formatter =  HTMLTemplateFormatter(template=template)

web_console_data = dict(index=[], msg_list=[])
web_console_src = ColumnDataSource(data=web_console_data)

web_console = DataTable(source=web_console_src,
              columns=[TableColumn(field="msg_list", title="Debug Console", formatter=formatter, editor=StringEditor())],
              width=1200, height=240, editable=True) #, index_position=None,

ii = 0 # console snap-to index
qProcLine=Queue()


# -----------------------------------------------------------------------------
qProcLine=Queue()
nLines=500 # lines of history output to keep in scrollbar
def print_subproc_output():
    global ii
    data = dict(index=[ii], msg_list=["{0}".format(qProcLine.get())])
    web_console_src.stream(data, nLines) # add just the new info; keep nLines in scrollbar history
    web_console_src.selected.indices = [ii] # snap to bottom DataTable cell
    if ii<(nLines-1):
        ii += 1


# run a subprocess in the background and stream console output to the web_console text box
def blocking_call_subprocess(eProc,qProcButton):
    global cfg
    lineCnt=0
    while eProc.is_set() is False:
        qmsg = qProcButton.get(block=True) # block here until button press
        print('received qProcButton qmsg=[{0}]'.format(qmsg))
        
        cfg_str = '{0}/{1}'.format(CFG_DIR,scenario_file_select.value)  # CFG_DIR=MOVE_HOME+'/scenario'
        
        if qmsg==0: # launch MoVE mapping process in a new brower window
            #exe_str = '{0}/run_move_live_mapping.sh'.format(DISPLAY_DIR)
            print('move_dashboard.py pwd={0}'.format( os.getcwd() )) # ./MoVE/v1.14_dev/src/move/data_displays
            if os.path.isdir('../bin_src'):
                exe_str = '../bin_src/run_move_live_mapping.sh' # this will only occur during development when run from move source code tree
            else:
                exe_str = 'run_move_live_mapping.sh' # expected result for move as an installed python package
                                                     # this shell script is in the path, in ./bin, wherever python3 installed it
                
            cmd = [exe_str, '-f', cfg_str] # ./run_move_map_client.sh -f ../scenario/default.cfg
            #cmd = ['bokeh', 'serve', '--port=5007', '--show', '--args', '-f', cfg_str] # ./run_move_map_client.sh -f ../scenario/default.cfg
            print('\texe_str={0}'.format(exe_str))
            print('\tcmd={0}'.format(cmd),flush=True)
            map_proc = subprocess.Popen(cmd,stdout=subprocess.DEVNULL)
            
        if qmsg==1: # kill MoVE mapping process
            if 'map_proc' in locals():
                helper_str1 = 'terminating MoVE map display process with: map_proc.kill()'
                qProcLine.put( '{0}'.format(helper_str1) )
                doc.add_next_tick_callback(print_subproc_output) # update web console; technique from: https://stackoverflow.com/a/60745271/7621907
                map_proc.kill() # send SIGKILL signal to shell script that launched the map subprocess (but this doesn't kill bokeh)
                del(map_proc)
                
                kill_str="kill -9 ` ps ax | grep move_live_mapping | grep -v grep | awk '{print $1}' `"
                helper_str1 = 'and with shell process: {0}'.format(kill_str)
                qProcLine.put( '{0}'.format(helper_str1) )
                doc.add_next_tick_callback(print_subproc_output) # update web console; technique from: https://stackoverflow.com/a/60745271/7621907
                adios = subprocess.Popen(kill_str,shell=True) # shell=True required to issue Linux shell command
            else:
                helper_str2 = '{0}: no map_proc variable to terminate. doing nothing.'.format(datetime.now().strftime('%c'))
                qProcLine.put( '{0}'.format(helper_str2) )
                doc.add_next_tick_callback(print_subproc_output) # update web console; technique from: https://stackoverflow.com/a/60745271/7621907
        
        if qmsg==2:
            cmd = ['screen', '-ls']
        
        if qmsg==3: # start ./main_core.py
            exe_str = '{0}/main_core.py'.format(CORE_DIR)
            cmd = ['python3', exe_str, '-f', cfg_str] # ./main_core.py -f ../scenario/default.cfg
            qProcLine.put( 'Launching {0}'.format(cmd) )
            doc.add_next_tick_callback(print_subproc_output) # update web console; technique from: https://stackoverflow.com/a/60745271/7621907
            proc_core = subprocess.Popen(cmd) # launch ./main_core.py as a running background process (do not monitor output in python - it's in the console where bokeh launches this script)
        
        if qmsg==4: # stop ./main_core.py
            if 'proc_core' in locals(): # if proc_core exists, then stop it
                proc_core.terminate() # end this subprocess
        
        
        if qmsg==5: # launch vehicles with output streamed to div textbox
            exe_str = '{0}/main_launch_veh_process.py'.format(CORE_DIR)
            cmd = ['python3', exe_str, '-f', cfg_str] # ./main_launch_veh_process.py -f ../scenario/default.cfg
        
        
        if qmsg==6: # tell all vehicles to enter runState=1 --> READY
            exe_str = '{0}/main_changeRunState.py'.format(CORE_DIR)
            cmd = ['python3', exe_str, '-f', cfg_str, '1'] # ./main_changeRunState.py -f ../scenario/default.cfg 1
        
        if qmsg==7: # tell all vehicles to enter runState=2 --> SET
            exe_str = '{0}/main_changeRunState.py'.format(CORE_DIR)
            cmd = ['python3', exe_str, '-f', cfg_str, '2'] # ./main_changeRunState.py -f ../scenario/default.cfg 2
        
        if qmsg==8: # tell all vehicles to enter runState=3 --> GO
            exe_str = '{0}/main_changeRunState.py'.format(CORE_DIR)
            cmd = ['python3', exe_str, '-f', cfg_str, '3'] # ./main_changeRunState.py -f ../scenario/default.cfg 3
        
        if qmsg==9: # tell all vehicles to enter runState=4 --> PAUSE
            exe_str = '{0}/main_changeRunState.py'.format(CORE_DIR)
            cmd = ['python3', exe_str, '-f', cfg_str, '4'] # ./main_changeRunState.py -f ../scenario/default.cfg 4
        
        if qmsg==10: # tell all vehicles to enter runState=5 --> STOP
            exe_str = '{0}/main_changeRunState.py'.format(CORE_DIR)
            cmd = ['python3', exe_str, '-f', cfg_str, '5'] # ./main_changeRunState.py -f ../scenario/default.cfg 5
        
        if qmsg==11: # find filename and size of most likely Core logfile
            # get the top line from:   ls -lt /tmp/*State.csv
            exe_str=' ls -lt {0}/*State.csv'.format(cfg.logfile_loc_core)
            ps = subprocess.Popen(exe_str, stdout=subprocess.PIPE, shell=True)
            res=ps.communicate()
            top_line = list(res)[0].decode('utf-8').split('\n')[0]
            print('\ttop_line={0}'.format(top_line))
            if top_line is not None:
                qProcLine.put( '{0}'.format(top_line) )
            doc.add_next_tick_callback(print_subproc_output) # update web console; technique from: https://stackoverflow.com/a/60745271/7621907
        
        # make the subprocess.Popen() call with 'cmd' just constructed above (except for certain cases)
        if qmsg in [2, 5, 6, 7, 8, 9, 10]: # launch cmd and redirect stdout and stderr to div textbox
            qProcLine.put( 'Launching: {0}'.format(cmd) ) # send string to web console
            doc.add_next_tick_callback(print_subproc_output) # update web console; technique from: https://stackoverflow.com/a/60745271/7621907
            #print('executing {0}'.format(cmd))
            process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)   
            for line in iter(process.stdout.readline, b''):
                #print('line={0}'.format(line)) # command output in command line console, line-by-line
                qProcLine.put( '{0}'.format(line.decode('utf-8')) ) # send string to web console
                doc.add_next_tick_callback(print_subproc_output) # technique from: https://stackoverflow.com/a/60745271/7621907


    logging.debug('thread exiting') 


# -----------------------------------------------------------------------------
# add white space in the web_console
def bt_map_launch_callback():
    qProcButton.put(0) # launch map subprocess in a new browser window

bt_width=120
bt_map_launch = Button(label="Start Map process" , button_type="success", width=bt_width)
bt_map_launch.on_click(bt_map_launch_callback)

def bt_map_kill_callback():
    qProcButton.put(1) # kill map subprocess (this will not close the browser)

bt_map_kill = Button(label="Stop Map Process" , button_type="success", width=bt_width)
bt_map_kill.on_click(bt_map_kill_callback)

# -----------------------------------------------------------------------------
def bt_subproc_callback2():
    qProcButton.put(2) # press the button, trigger the blocking subprocess

bt_checkVehProcs = Button(label="Veh models on?" , button_type="warning", width=bt_width)
bt_checkVehProcs.on_click(bt_subproc_callback2)

# -----------------------------------------------------------------------------
# add white space in the web_console
def add_whitespace():
    qProcLine.put(' ')
    doc.add_next_tick_callback(print_subproc_output) # technique from: https://stackoverflow.com/a/60745271/7621907
    qProcLine.put(' ')
    doc.add_next_tick_callback(print_subproc_output)

bt_space = Button(label="add whitespace" , button_type="success", width=bt_width)
bt_space.on_click(add_whitespace)

# -----------------------------------------------------------------------------
def bt_logfile_size_callback():
    qProcButton.put(11) # check logfile size

bt_logfile_size = Button(label="Core Logfile Status?" , button_type="success", width=bt_width)
bt_logfile_size.on_click(bt_logfile_size_callback)


# ==============================================================================
# =                       END: console output                                  =
# ==============================================================================




# ---------------------------------------------------------------------
def scenario_lock_toggle_handler(attr, old, new):
    global cfg
    print('\tscenario_lock_toggle value: [' + str(new) + ']')
    if new is True:
        scenario_lock_toggle.label       = 'Scenario: Locked, Press to Unlock'
        scenario_lock_toggle.button_type = 'success' # options: default (grey), primary (tiel), success (green), warning (orange), danger (red)
        scenario_file_select.disabled    = True # drop-down is greyed out 
        core_ON_OFF.disabled             = False # "Core OFF", "Core ON"
        veh_launch_toggle.disabled       = False # "Press to Launch Vehicle Processes"
        bt_map_kill.disabled             = False # "Show Map Launch Commands"
        bt_map_launch.disabled           = False # "Show GPS port assignments"
        bt_logfile_size.disabled         = False # "Logfile Size?"
        cfgFile='{0}/{1}'.format( CFG_DIR , scenario_file_select.value )
        print('\treading config file: [{0}]'.format(cfgFile))
        
        # read config file just locked in
        cfg, veh_names_builtins, veh_names_live_gps = readConfigFile( cfgFile, -1 )
        #if len(cfg.default_display_cols)>0:
        #    #print('cfg.default_display_cols={0}'.format(cfg.default_display_cols))
        #    multichoice_col_sel.value=cfg.default_display_cols # update MultiChoice button values to what was in config file
        #    cols_subset = selected_columns( cfg.default_display_cols )
        #    #print('cols_subset={0}'.format( cols_subset ))
        #    veh_table.columns=cols_subset # dynamically changing columns to match defaults specified in config file
        #else:
        #    print('error: something went wrong reading initial data table column set from config file: {0}'.format(cfgFile))
        
    else:
        scenario_file_select.options     = scan_all_cfg_files() # udpate scenario file list in Select pull-down
        scenario_lock_toggle.label       ='Press to Lock Scenario File'
        scenario_lock_toggle.button_type ='default'
        scenario_file_select.disabled    = False
        
        print('\tstopping main_core.py process')
        qProcButton.put(4) # press the button, terminate ./main_core.py
        core_ON_OFF.button_type          = 'primary'
        core_ON_OFF.active               = 0     # make the button's active state the first, which is "Core OFF"
        core_ON_OFF.disabled             = True  # disable MovE Core until a scenario is selected
        
        veh_launch_toggle.disabled       = True
        RSGPS_buttons.disabled           = True

scenario_lock_toggle = Toggle(label="Press to Lock Scenario File", button_type="default")
scenario_lock_toggle.on_change('active',scenario_lock_toggle_handler)
scenario_lock_toggle.height_policy='fit'

# disable Core, veh, RSGPS buttons until scenario selected
core_ON_OFF.disabled        = True # "Core OFF", "Core ON"
veh_launch_toggle.disabled  = True # "Press to Launch Vehicle Processes"
RSGPS_buttons.disabled      = True # "Ready", "Set", "Go", "Pause", "Stop"
bt_map_kill.disabled        = True # "Stop Map Process"
bt_map_launch.disabled      = True # "Start Map process"
bt_logfile_size.disabled    = True # "Logfile Size?"





# bound a (in radians) on the interval [0 2*pi]; from wrap_theta.py
def wrap(a):
    sign = lambda x: x and (1, -1)[x<0] # from: https://stackoverflow.com/a/16726462/7621907
    return ( abs(a) % (2*3.14159265359) )*sign(a)


State={} # dictionary container for all inbound msgTypes: 'move', 'cell', 'xbee', 'adsb', 'lora', 'rid'

# setup DataTable with a simple source that will be overwritten when data arrives from udp thread
data={'notEmpty':[]}
table_source_move = ColumnDataSource(data=data)
table_source_adsb = ColumnDataSource(data=data)

data_table_move = DataTable(source=table_source_move)
data_table_adsb = DataTable(source=table_source_adsb)


# -----------------------------------------------------------------------------
# thread to update State with udp updates as they arrive for next periodic_callback_update_browser()
def udp_listener_thread(eNewUdp,e,debug):
    udp_ip = '0.0.0.0' # listen from anyone
    udp_port = 5556 # listen on port from sender: move_core | all_vehicles.py | dashSock.sendto(), specified in config file
    
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # udp/ip
    try:
        sock.bind((udp_ip, udp_port)) # must bind() to listen
    except OSError as err:
        logging.debug('OSError: {0}'.format(err) )
        raise
        logging.debug('exiting!')
        exit(-1)
    except:
        logging.debug("Unexpected error:", sys.exc_info()[0])
        raise # raise the error and stop execution
        exit(-1)
    
    cnt=0
    tNow = datetime.now()
    while e.is_set() is False:
        data, addr = sock.recvfrom(2048) # buffer size is 1024 bytes
        #print('udp raw data: {0}'.format(data))
        cnt += 1
        msg = msgpack.unpackb(data) # receive State[vid]; State for a single vid (from core | all_vehicles.py | poll_state() )
        if debug>1: print("\nmove_dashboard.py received [", len(data), "] bytes with msg=",msg)
        if debug>1: print("from [", addr , "], receive cnt=", cnt)
        if debug>1:
            print('\nmsg:')
            pprint.pprint(msg)
            print('\n')
        
        # move msg:
        # move_dashboard.py received [ 786 ] bytes with msg= {'tStamp': 1664896151.1795902, 'vid': 104, 'vehName': 'viper', 'msgType': 'move',
        #       'vehType': 'aerial', 'vehSubType': 'fixedwing', 'runState': 1, 'msg_cnt_in': 0, 'msg_cnt_out': 9707, 'msg_errorCnt': 0,
        #       'srt_margin_avg': 0.0, 'srt_err_avg': 0.0, 't': -1, 'gps_unixTime': 0,
        #       'pos': {'X': 0.0, 'Y': 0.0, 'Z': 0.0, 'psi': 0.0, 'hdg': 0.0, 'lat': 29.193110999581492, 'lon': -81.0461709999998},
        #       'vel': {'Xd': 0.0, 'Yd': 0.0, 'Zd': 0.0, 'psiDot': 0.0, 'spd_mps': 0.0},
        #       'detectMsg': {'objId': 0, 'lastSeen': 0, 'lastLoc_X': 0, 'lastLoc_Y': 0, 'lastLoc_Z': 0, 'lastLoc_lat': 0, 'lastLoc_lon': 0},
        #       'waypoint_status': {'waypoint': -1, 'lap': -1, 'spd_mps': -1},
        #       'u': [0.0, 0.0, 0.0, 0.0], 'bIdCmd': -1, 'bIdName': 'no behavior', 'bIdProgress': 0.0, 'missionAction': 'none', 'missionState': -1, 'missionPctComplete': 0.0,
        #       'missionProgress': -1, 'missionLastCmdTime': 'Tue Oct  4 08:27:08 2022', 'missionStatus': 'readyWait', 'missionCounter': 1, 'L_char': 2.0,
        #       'sensorData': {'mac': 104, 'batt_stat': -1}}
        #
        # adsb msg:
        # move_dashboard.py received [ 258 ] bytes with msg= {'tStamp': 1664896150.525659, 'hex': 'aa1e30', 'altitude': 26400, 'mlat': [], 'tisb': [], 'messages': 24,
        #        'seen': 11.3, 'rssi': -32.5, 'msgCnt': 131, 'msgType': 'adsb', 'lat': 28.300003, 'lon': -81.735271, 'nucp': 7, 'seen_pos': 68.1, 'vert_rate': -1984,
        #        'track': 166, 'speed': 499, 'squawk': '3534', 'vid': 'aa1e30', 'vehName': 'aa1e30', 'vehType': 'adsb', 'vehSubType': 'A'}
        
        
        # new philosophy as of v1.15: "take it as is comes" (don't map individual fields; respect incoming message contents)
        if msg['msgType']=='move':
            # pandas only accomodates dictionaries without dictionaries; flatten all dictionaries in State[vid]
            vid=msg['vid']
            State[vid]=copy.deepcopy(msg) # deep copy State entry from msg
            
            for k,v in msg['pos'].items(): # map pos dict into individual State entries: 'pos': {'X': -179.9, 'Y': 121.9, 'Z': 0.0, 'psi': -5.0, 'lat': 29.1, 'lon': -81.0}
                State[vid][k]=v
            State[vid].pop('pos', None) # delete 'pos' dict so pandas doesn't handle
            
            for k,v in msg['vel'].items(): # map vel dict into individual State entries: 'vel': {'Xd': 3.5, 'Yd': 4.8, 'Zd': 0.0, 'psiDot': 0.0, 'spd_mps': 5.0}
                State[vid][k]=v
            State[vid].pop('vel', None) # delete 'pos' dict so pandas doesn't handle
            
            #for k,v in msg['detectMsg'].items(): # map pos dict into individual State entries: 'detectMsg': {'objId': 0, 'lastSeen': 0, 'lastLoc_X': 0, 'lastLoc_Y': 0, 'lastLoc_Z': 0, 'lastLoc_lat': 0, 'lastLoc_lon': 0}
            #    State[vid][k]=v
            State[vid].pop('detectMsg', None) # delete 'detectMsg' dict so pandas doesn't handle
            
            #for k,v in msg['waypoint_status'].items(): # map waypoint_status dict into individual State entries: {'waypoint': -1, 'lap': -1, 'spd_mps': -1}
            #    State[vid][k]=v
            State[vid].pop('waypoint_status', None) # delete 'waypoint_status' dict so pandas doesn't handle
            
            #for k,v in msg['sensorData'].items(): # map sensor_data dict into individual State entries: sensorData': {'mac': '', 'batt_stat': 67.0, 'accel_x': -1.0606186, 'accel_y': 0.68225354, 'accel_z': 9.698409}
            #    State[vid][k]=v
            State[vid].pop('sensorData', None) # delete 'sensor_data' dict so pandas doesn't handle
            
            if debug>1: print('updated msgType=move, vid=[{0}]'.format(vid))
        
        if msg['msgType']=='adsb':
            vid=msg['hex']
            State[vid]=copy.deepcopy(msg)
            if debug>1: print('updated msgType=adsb, vid=[{0}]'.format(vid))
        #pprint.pprint(State)
        
        if debug>1: print(State.keys()) # show mixed 'msgTypes' in State: 'move', 'adsb', 'cell', 'xbee', 'lora', 'rid'
        if debug>2:
            print('\nState:')
            pprint.pprint(State)
            print('\n')
        
        tNow = datetime.now()
        uptime_str = '{0}'.format( str(tNow-t0).split('.')[0] ) # truncate microseconds on uptime
        if debug>1: print("\t{0}: received {1} bytes updating vid={2}, t_veh={3:10.2f}, X={4:10.2f}(m), Y={5:10.2f}(m), Z={6:10.2f}(m), spd={7:10.2f}(m/s), beh={8}, behProgress={9}"
                        .format( uptime_str, len(data), vid, msg['t'],pos['X'],pos['Y'],pos['Z'],vel['spd_mps'],msg['bIdName'],msg['bIdProgress'] ))
        
        # if execution gets  here, it means:
        #     (a) move_core.py is running, (because this only listens to main_core.py)
        #     (b) a vehicle model is running and reporting to main_core.py
        # let periodic_callback_update_browser() know at least 1 new udp message arrived since it's last evaluation
        eNewUdp.set() # notify periodic callback that new udp message(s) have arrived since last periodic callback update



# instantiate empty MultiChoice object (assume no udp data yet, and cfg. class is not available until lock button pressed)
multi_choice_move = MultiChoice(value=[], options=[], height=100, sizing_mode='stretch_width')
multi_choice_move.js_on_change("value", CustomJS(code="""console.log('multi_choice: value=' + this.value, this.toString())"""))

multi_choice_adsb = MultiChoice(value=[], options=[], height=100, sizing_mode='stretch_width')
multi_choice_adsb.js_on_change("value", CustomJS(code="""console.log('multi_choice: value=' + this.value, this.toString())"""))


# function to choose the subset of all possible columns for display
def select_TableColumns( title_sel_list, tableColumns_move ):
    # take in a list of titles and return a list of TableColumn()'s in the
    # same order as [titles] (for a common, predictable user experience)
    tableColumns_subset=[]
    print('entering: select_TableColumns()')
    #print('\t\ttitle_sel_list={0}'.format(title_sel_list)) # title_sel_list=['msgType', 'vid', 'vehName', 'vehType', ... ]
    #print('\t\ttableColumns_move={0}'.format(tableColumns_move)) # tableColumns_move=[TableColumn(id='p1071', ...), TableColumn(id='p1074', ...), TableColumn(id='p1077', ...), ... ]
    for i,tc in enumerate(tableColumns_move):
        #print('\ti={0}, tc.title={1}'.format(i,tc.title)) # i=0, tc.title=msgType,        i=1, tc.title=vid,       ...
        if tc.title in title_sel_list:
            #print('\ti={0}, tc.title={1}'.format(i,tc.title))
            tableColumns_subset.append(tableColumns_move[i])
    #print('tableColumns_subset=[{0}]'.format(tableColumns_subset)) # exiting: tableColumns_subset=[[TableColumn(id='p1071', ...), TableColumn(id='p1074', ...),
    print('exiting: select_TableColumns()'.format(tableColumns_subset)) # exiting: tableColumns_subset=[[TableColumn(id='p1071', ...), TableColumn(id='p1074', ...),
    return tableColumns_subset # return a list of TableColumn() objects


# initialize the DataTable with all columns ( this is before config file is chosen in scenario_lock_toggle_handler )
#cols_subset = selected_columns( multichoice_col_sel.value )


# sensor data table
sensor_data = dict(
        vid          = [], # 0
        vehName      = [], # 1
        )
sensor_source = ColumnDataSource(sensor_data)
sensor_cols = []
#for k in sensor_data.keys():
#    sensor_cols.append( TableColumn(field=k,title=k) )

sensor_table = DataTable(source=sensor_source, columns=[], width=screen_width, header_row=True) # editable=True, height=2000





#State_move={} # native MoVE simulated vehicle
#State_cell={} # ground-based cellular communications (android HyperIMU, iOS SensorLog)
#State_adsb={} # nearby (real) aircraft, General Aviation (GA)
#State_xbee={} # Xbee-based mesh networked aerial vehicle
#State_lora={} # Lora-based mesh networked aerial vehicle

nColsLast_move=0
nColsLast_adsb=0
nMultiChoiceValuesLast = 0 # this triggers dynamic table columns for display

# ------------------------------------------------------------------------------
# create a callback to update the browser's uptime counter and vehicle table
dt = 1000 # (ms) browser update rate specified in add_periodic_callback()
t0=datetime.now()
def periodic_callback_update_browser():
    global nColsLast_move, nColsLast_adsb, nMultiChoiceValuesLast
    tNow = datetime.now()
    uptime_str = 'dashboard uptime [{0}]'.format( str(tNow-t0).split('.')[0] ) # truncate microseconds on uptime
    tNow_str   = 'Currently: [{0}]'.format( tNow.strftime("%c") )
    uptime_bar.text = tNow_str + ", " + uptime_str
    
    if eNewUdp.is_set()==True: # new udp message(s) just came in since last periodic callback evaluation
        
        # construct dictionaries for separate msgType tables: 'move', 'cell', 'xbee', 'adsb', 'lora', 'rid'
        State_move={}
        State_adsb={}
        for k,v in State.items():
            vid=k
            if v['msgType']=='move':
                State_move[vid]=copy.deepcopy(State[vid]) # deep copy to avoid changing State[vid]
            if v['msgType']=='adsb':
                State_adsb[vid]=copy.deepcopy(State[vid])
        
        #df = pd.DataFrame.from_dict(nested_dictionary, orient='index')
        df_move = pd.DataFrame.from_dict(State_move, orient='index')
        df_adsb = pd.DataFrame.from_dict(State_adsb, orient='index')
        #print('df_move.head():',df_move.head())
        #print('df_adsb.head():',df_adsb.head())
        
        source_move = ColumnDataSource(df_move) # this is a dictionary of lists, like Bokeh expects: 'index': array([100, 103, 107, 102]), 'vid': array([100, 103, 107, 102]), 'vehName': array(['speedracer', 'maverick', 'goose', 'goose'], ...and so on
        source_adsb = ColumnDataSource(df_adsb)
        
        # pandas Dataframe -> bokeh DataTable
        table_cols_move = [TableColumn(field=name, title=name) for name in df_move.columns] # df_move.columns -> 'msgType', 'vid', 'vehName', 'vehType', 'runState', 'X', 'Y', 'Z', 'psi', 'lat', 'lon', 'Xd', 'Yd', 'Zd', 'psiDot', 'spd_mps'
        table_cols_adsb = [TableColumn(field=name, title=name) for name in df_adsb.columns] # df_adsb.columns -> 'msgType', 'hex', 'lat', 'lon', 'nucp', 'seen_pos', 'altitude', 'mlat', 'tisb', 'messages', 'seen', 'rssi', 'vert_rate', 'track', 'speed', 'msgCnt', 'squawk', 'flight', 'category'
        #print('table_cols_move columns:')
        #for col in table_cols_move:
        #    print('{0}, '.format(col.title),end='')
        
        # ***CLIP START*** --> move_dashboard.py from dataFrame_to_dataTable_example_i.py
        # Goal: change native move vehicle tables when first vehicle message arrives or when user changes multichoice .values
        # Algo:
        # (a) if N_keys in State changes, update multi_choice .options and .values (the .values are current selection, .options are all possible options)
        # (b) if length of MultiChoice.values changes, change table columns displayed
        # (c) changing column order makes it difficult for the user to study, so only update when a new field has arrived
        # updating columns from pandas DataFrame triggers tornado web page update
        #print('len(table_cols_move)={0}'.format(len(table_cols_move)))
        #print('\n\ttable_cols_move={0}\n'.format(table_cols_move))
        if len(table_cols_move) != nColsLast_move:
            nColsLast_move = len(table_cols_move)
            
            print('assigning multi_choice_move button value and options')
            multi_choice_move.options = list(df_move.columns)
            multi_choice_move.value   = list(df_move.columns)
            
            # remove cfg. excludes list to make data_table_move more easily viewed in web page - prevents too many (super thin) columns
            print('len(cfg.default_display_excludes)={0}'.format(len(cfg.default_display_excludes)))
            if len(cfg.default_display_excludes)>0:
                for item in cfg.default_display_excludes:
                    #print('considering excludes item=[{0}]'.format(item))
                    if item in multi_choice_move.value:
                        print('\tremoving cfg file excludes list: [{0}]'.format(item))
                        multi_choice_move.value.remove(item) # if the cfg excludes string is in the multichoice .value list, remove it
            
            # [add excludes code ] #  exclude the cfg excludes list from cfg file
            print('MultiChoice .value: {0},\nMultiChoice .options: {1}'.format( multi_choice_move.value, multi_choice_move.options ))
            print('len(multi_choice_move.value)={0}'.format(len(multi_choice_move.value)))
            
        # change table columns displayed?
        if len(multi_choice_move.value) != nMultiChoiceValuesLast:
            nMultiChoiceValuesLast = len(multi_choice_move.value)
            print('\n\tchanged! multi_choice_move={0}'.format(multi_choice_move.value))
            print('-------------------')
            tableColumns_subset = select_TableColumns( multi_choice_move.value, table_cols_move )
            print('\ttableColumns_subset={0}'.format( tableColumns_subset ))
            print('\t-------------------')
            data_table_move.columns=tableColumns_subset # dynamically changing columns!
        
        # adsb table update?
        if len(table_cols_adsb) != nColsLast_adsb:
            nColsLast_adsb = len(table_cols_adsb)
            data_table_adsb.columns = table_cols_adsb
            print('\n\tchanged! data_table_adsb={0}'.format(data_table_adsb.columns))
        # ***CLIP END*** --> move_dashboard.py from dataFrame_to_dataTable_example_i.py
        
        # trigger tornado web page update with .data update
        data_table_move.source.data = dict(source_move.data) # this updates data displayed on the DataTables
        data_table_adsb.source.data = dict(source_adsb.data)
        
        
        # first time through, sensor_table columns is empty, so populate with sensor_cols just created from incoming msg['sensorData']
        if len(sensor_table.columns)==0:
            sensor_table.columns=sensor_cols
        
        if eRSGPS.is_set()==False: # flag for auto-setting Ready-Set-Go-Pause-Stop button
            RSGPS_buttons.disabled = False # enable the Ready-Set-Go-Pause-Stop button
            # ----------> SET RUNSTATE BUT WITHOUT USING DS_NEW!!
            #RSGPS_buttons.active = ds_new['runState'][0]-1 # assign this runState, which assumes all vehicles are in this runState (element [0] is one vehicle's runState)
            eRSGPS.set() # set to True to prevent subsequent assignments
        
        


#qUdp    = Queue() # queue to get udp data out of the udp_listener() thread and into Bokeh's main thread
#qRSGPS  = Queue() # so udp thread can turn on Ready-Set-Go-Pause-Stop button
eRSGPS  = Event() # initially False; this toggle re-evaluates Ready-Set-Go-Pause-Stop button from incoming udp data (at startup and every time Core is turned on)
e       = Event() # for signaling exit to *all* threads simultaneously (except main() )
eNewUdp = Event() # for notifying periodic callback that new udp data arrived; Event default is_set()==False


# ensure data_table creation before layout()
eNewUdp.set()
periodic_callback_update_browser() # make sure callback runs once before defining layout


# start blocking thread for launching background processes with a button
eProc = Event()
qProcButton = Queue() # queue for sending commands to launch in the host operating system
tProc = Thread(target=blocking_call_subprocess, args=(eProc,qProcButton),daemon=True) # daemon==True allows thread to exit when Bokeh captures ctrl+c
tProc.start() # press button, start thread



# start udp_listener() thread
pUdp = Thread(name='udp_listener', target=udp_listener_thread,args=(eNewUdp,e,debug),daemon=True) # daemon==True allows thread to exit when Bokeh captures ctrl+c)
#time.sleep(5)
pUdp.start()


# arrange all map views in a grid layout then add to the document
layout = layout([
    [description],
    [uptime_bar],
    [scenario_file_select,scenario_lock_toggle],
    [core_ON_OFF],
    [veh_launch_toggle],
    [RSGPS_buttons],
    [bt_map_launch,bt_map_kill,bt_checkVehProcs,bt_space,bt_logfile_size], # bt_disp_cols,
    [web_console],
    
    [multi_choice_move],
    [data_table_move],
    
    [multi_choice_adsb],
    [data_table_adsb]
    #[veh_table],
                      ],sizing_mode='stretch_width') #, sizing_mode='scale_width') # "fixed", "stretch_both", "scale_width", "scale_height", "scale_both"

curdoc().add_root(layout)
curdoc().add_periodic_callback(periodic_callback_update_browser, dt) # <-- this controls browser update frequency
curdoc().title = "MoVE Experiment Control and Live Data Display"









