# behaviors for swarm motion:
#       swarmSteer()
#       swarmBoids()
#
# this is an include in the Behaviors class, in ./behaviors.py
#
# Marc Compere, comperem@erau.edu
# created : 18 Feb 2024
# modified: 24 Mar 2024
#
# --------------------------------------------------------------
# Copyright 2018 - 2024 Marc Compere
#
# This file is part of the Mobility Virtual Environment (MoVE).
# MoVE is open source software licensed under the GNU GPLv3.
#
# MoVE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3
# as published by the Free Software Foundation.
#
# MoVE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License version 3 for more details.
#
# A copy of the GNU General Public License is included with MoVE
# in the COPYING file, or see <https://www.gnu.org/licenses/>.
# --------------------------------------------------------------

import random
import time
from datetime import datetime
import queue
import numpy as np
import logging
from math import sin,cos,atan2,pi,copysign,sqrt,tanh
import code # drop into a python interpreter to debug using: code.interact(local=locals())
from pprint import pformat # pretty print v2v structure
import copy # copy.deepcopy()


def computeAverageHeading(vid,v2vState):
    N = len( v2vState.keys() ) # total number of keys in v2vState
    hdgList=[]
    # scan entrire v2vState to:
    # (a) get all heading values in a list
    # (b) ...

    for ith_vid in v2vState:
        logging.debug('vid={0}, evaluating ith_vid={1}'.format(vid,ith_vid))
        if (v2vState[ith_vid]['stale']==False):
            #logging.debug("\nv2vstate[{0}]={1}".format(ith_vid, pformat(v2vState[ith_vid]) ))
            if (ith_vid != vid):
                hdgList.append( v2vState[ith_vid]['hdg_deg'] ) # (deg) append to heading list

    if len(hdgList)>0:
        hdgAvg = np.average(hdgList)
    else:
        hdgAvg = 0.0

    return N,hdgAvg


# --------------------------------------------------------------
#                  behavior: swarmSteer()
#              for use with swarm_default.cfg
# --------------------------------------------------------------
# swarmSteer: a Vicsek 2d steering behavior (thread) that uses v2v to determine neighbors
def swarmSteer(self,bId,behName,priority,veh,debug):
    logging.debug('\t\t[{0}]: bId={1} [{2}], priority={3}'.format( datetime.now().strftime('%c') ,bId,behName,priority))
    e=veh.e
    vid=veh.vid
    randFlag=veh.randFlag # (0/1) use random numbers?
    printLevelLocal = 0 # (0/1) local debugging print flag
    
    spd = 10 #2.0 # (m/s) vehicle nominal wander speed
    dtDormant = 0.1 #20 + randFlag*random.uniform(0.0,20.0) # (s) behavior dormant period (not turning)
    #spd       = 2.2 # (m/s) speed while turning
    dtTurn_o   = 0.2 # (s) turn duration when randFlag==0
    steer     = 0.0 # (rad) nominal steer command is positive, or left-hand turn when randFlag==0
    while e.is_set() is False:
        
        e.wait(dtDormant) # (sec) behavior is dormant for this duration; e.wait() is an interruptable sleep() with independent timers despite using the same event, e
        
        # compute average of all vehicle headings in the v2vState
        N,hdgAvg = computeAverageHeading(vid,veh.v2v.v2vState)
        hdgErrRaw = (3.1415926/180.0)*(hdgAvg - veh.pos.hdg_deg) # (rad) from (deg)
        
        # unwrap such that psi_err is on [-pi  +pi]
        # credit: Mike Fair, Nov 2004
        spsi = sin(hdgErrRaw)
        cpsi = cos(hdgErrRaw)
        # psi_err is in body-fixed frame on inteval [-pi +pi] - this essentially says, turn left or turn right
        hdgErr = atan2(spsi,cpsi) # (rad) heading error w.r.t. vehicle x-axis, atan2() returns [-pi  +pi]
        
        steer_raw = -1.0*tanh( hdgErr )
        steer = np.clip( steer_raw , -0.5, +0.5) # (rad) impose max steer angle, clip is numpy's saturation function
        logging.debug('\theading avg for [{0}] vehicles = {1}(deg)'.format( (N-1),hdgAvg ))
        logging.debug('\thdgErrRaw={0}(rad), steer_raw={1}(rad), steer={2}(rad), veh.vel.spd_mps={3}'.format( hdgErrRaw,steer_raw,steer,veh.vel.spd_mps ))
        
        if randFlag>0:
            dtTurn = dtTurn_o + random.uniform( 0.0 , 2.0*dtTurn_o) # (s) turn duration
            steer  = steer    + random.uniform(-0.2 ,+0.2          ) # uniformly distributed random number
        else:
            dtTurn = dtTurn_o
        
        if (debug>1): logging.debug('{0:s}, bId={1:d}, pri={2:d}: turn={3:f}'.format(behName, bId, priority, steer))
        
        if (printLevelLocal>0):
            logging.debug('current v2vState:\n' + pformat(veh.v2v.v2vState))
        
        
        # enable cmds with this format: [ bId , steer , speed, pitch, progress ]
        self.qBehavior.put( [ bId , steer , 1.1*spd, 0.0, 0.0, 0.0 ] )
        
        #time.sleep(dtTurn) # (sec) duration to turn
        e.wait(dtTurn) # (sec) duration to turn; interruptable sleep()
        
        #self.qBehavior.put( [ bId , 0 ] ) # disable commands with [bId, 0]
    
    logging.debug('[{0}]: bId={1} [{2}], exiting.'.format( datetime.now().strftime('%c') ,bId,behName))



















# ==============================================================================
#                               BOIDS BEHAVIORS
# ==============================================================================

# separation from: https://vanhunteradams.com/Pico/Animal_Movement/Boids-algorithm.html
# compute separation by accumulating all other vehicles positions that are too close
def boidsSeparation(vid,v2vState,v2v_dNeighClose,sepfactor):
    dXaccum  = 0.0 # (m) close_dx
    dYaccum  = 0.0 # (m) close_dy
    dZaccum  = 0.0 # (m)
    dv_X_sep = 0.0  # (deg)
    dv_Y_sep = 0.0  # (m/s)
    debug    = 0
    
    # scan entrire v2vState to:
    # accumulate all other vehicle's positions that are too close
    for ith_vid in v2vState.keys():
        #print('ith_vid={0}'.format(ith_vid))
        if debug>0: logging.debug('vid={0}, evaluating ith_vid={1}'.format(vid,ith_vid))
        if (v2vState[ith_vid]['stale']==False) and (vid in v2vState.keys()):
            if debug>0: logging.debug("\nv2vstate[{0}]={1}".format(ith_vid, pformat(v2vState[ith_vid]) ))
            if (ith_vid != vid):
                dX,dY,dZ,distToIth = computeDistance(ith_vid,vid,v2vState)
                if (distToIth <= v2v_dNeighClose):
                    dXaccum += -dX # (m)
                    dYaccum += -dY # (m)
                    dZaccum += -dZ # (m)
    
    dv_X = dXaccum*sepfactor  # (m/s)
    dv_Y = dYaccum*sepfactor  # (m/s)
    return dv_X_sep, dv_Y_sep


def computeDistance(ith_vid,vid,v2vState):
    pos_i = v2vState[vid]     # self's v2vState
    pos_j = v2vState[ith_vid] # 'the other guy's v2vState'
    
    # vector from i'th (me) vehicle to j'th vehicle (the other guy) - recall vectors: "tip minus tail"
    dX = pos_j['posX'] - pos_i['posX'] # (m) X_j - X_i   in inertial XYZ frame
    dY = pos_j['posY'] - pos_i['posY'] # (m) Y_j - Y_i   in inertial XYZ frame
    dZ = pos_j['posZ'] - pos_i['posZ'] # (m) Z_j - Z_i   in inertial XYZ frame
    
    distToIth = sqrt( pow(dX,2) + pow(dY,2) ) #+ pow(dZ,2) ) # (m)
    #logging.debug('dX={0},\tdY={1},\tdZ={2}'.format(dX,dY,dZ))
    return dX,dY,dZ,distToIth




# alignment from: https://vanhunteradams.com/Pico/Animal_Movement/Boids-algorithm.html
# compute aligntment velocity and direction by averaging all other vehicle speeds and directions
def boidsAlignment(vid,v2vState,alignfactor):
    v_X_list   = []
    v_Y_list   = []
    v_X_avg    = 0.0 # (m/s)
    v_Y_avg    = 0.0 # (m/s)
    dv_X_align = 0.0 # (m/s)
    dv_Y_align = 0.0 # (m/s)
    nNeighbors = 0
    v_XY_self  = np.array([0,0])
    debug      = 0
    
    # get v_X and v_Y of self, vid
    if vid in v2vState.keys():
        psi = v2vState[vid]['hdg_deg']*(np.pi/180.0) # (rad) from (deg)
        v_x = v2vState[vid]['spd_mps']               # (m/s)
        T_transpose = np.array( [[cos(psi) ,-sin(psi)],
                                [sin(psi) , cos(psi)]] )
        v_xy=np.array( [v_x,0] ) # (m/s) body-fixed vel; note: vy = b*psiDot is set to zero
        v_XY = T_transpose @ v_xy   # transform body-fixed xy vel into inertial-frame XY velocity
        v_XY_self = copy.deepcopy(v_XY)
    
    # scan entrire v2vState to:
    # get all other vehicle's v_XY
    for ith_vid in v2vState.keys():
        if debug>0: logging.debug('vid={0}, evaluating ith_vid={1}'.format(vid,ith_vid))
        if (v2vState[ith_vid]['stale']==False) and (vid in v2vState.keys()):
            if debug>0: logging.debug("\nv2vstate[{0}]={1}".format(ith_vid, pformat(v2vState[ith_vid]) ))
            if (ith_vid != vid):
                nNeighbors+=1
                psi = v2vState[ith_vid]['hdg_deg']*(np.pi/180.0) # (rad) from (deg)
                v_x = v2vState[ith_vid]['spd_mps']               # (m/s)
                T_transpose = np.array( [[cos(psi) ,-sin(psi)],
                                         [sin(psi) , cos(psi)]] )
                v_xy=np.array( [v_x,0] ) # (m/s) body-fixed vel; note: vy = b*psiDot is set to zero here
                v_XY = T_transpose @ v_xy   # transform body-fixed xy vel into inertial-frame XY velocity
                v_X_list.append( v_XY[0] ) # (deg) append Earth-fixed frame X-velocity to v_X_list (see veh_util.py | f(t,x,u) for reference)
                v_Y_list.append( v_XY[1] ) # (m/s) append Earth-fixed frame X-velocity to v_Y_list
    
    if nNeighbors>0:
        v_X_avg    = np.average(v_X_list)
        v_Y_avg    = np.average(v_Y_list)
        dv_X_align = (v_X_avg - v_XY_self[0])*alignfactor
        dv_Y_align = (v_Y_avg - v_XY_self[1])*alignfactor
    else:
        dv_X_align = 0.0
        dv_Y_align = 0.0
    
    return dv_X_align, dv_Y_align





# cohesiion from: https://vanhunteradams.com/Pico/Animal_Movement/Boids-algorithm.html
# steer towards the center of mass of all other vehicles within visible range
def boidsCohesion(vid,v2vState,v2v_dNeighViz,cohesfactor):
    Xpos_avg   = 0.0 # (m) intermediate var
    Ypos_avg   = 0.0 # (m) intermediate var
    Zpos_avg   = 0.0 # (m) intermediate var
    dv_X_coh   = 0.0 # (m) output var
    dv_Y_coh   = 0.0 # (m) output var
    #dv_Z_coh   = 0.0 # (m) output var
    nNeighbors = 0
    debug      = 0
    
    # scan entrire v2vState to:
    # average all other vehicle's x and y (and z) positions
    for ith_vid in v2vState.keys():
        #print('ith_vid={0}'.format(ith_vid))
        if debug>0: logging.debug('vid={0}, evaluating ith_vid={1}'.format(vid,ith_vid))
        if (v2vState[ith_vid]['stale']==False) and (vid in v2vState.keys()):
            if debug>0: logging.debug("\nv2vstate[{0}]={1}".format(ith_vid, pformat(v2vState[ith_vid]) ))
            if (ith_vid != vid):
                dX,dY,dZ,distToIth = computeDistance(ith_vid,vid,v2vState)
                if (distToIth <= v2v_dNeighViz):
                    Xpos_avg   += v2vState[ith_vid]['posX']
                    Ypos_avg   += v2vState[ith_vid]['posY']
                    Zpos_avg   += v2vState[ith_vid]['posZ']
                    nNeighbors += 1
    
    if (nNeighbors>0):
        Xpos_avg = Xpos_avg/nNeighbors
        Ypos_avg = Ypos_avg/nNeighbors
        Zpos_avg = Zpos_avg/nNeighbors
        
        dv_X_coh = ( Xpos_avg - v2vState[vid]['posX'] ) * cohesfactor
        dv_Y_coh = ( Ypos_avg - v2vState[vid]['posY'] ) * cohesfactor
        #dv_Z_coh = ( Zpos_avg - v2vState[vid]['posZ'] ) * cohesfactor
    
    return dv_X_coh, dv_Y_coh


# --------------------------------------------------------------
#                  behavior: swarmBoids()
#              for use with swarm_default.cfg
# --------------------------------------------------------------
# swarmSteer: a Boids implementation (behavior thread) that uses v2v to determine neighbors
def swarmBoids(self,bId,behName,priority,veh,debug):
    logging.debug('\t\t[{0}]: bId={1} [{2}], priority={3}'.format( datetime.now().strftime('%c') ,bId,behName,priority))
    e=veh.e
    vid=veh.vid
    randFlag=veh.randFlag # (0/1) use random numbers?
    printLevelLocal = 1 # (0/1) local debugging print flag
    v2v_dRadio      = veh.v2v.v2v_dRadio      # 500m radio range
    v2v_dNeighViz   = veh.v2v.v2v_dNeighViz   # 50m  visible range
    v2v_dNeighClose = veh.v2v.v2v_dNeighClose # 10m  too close
    
    # ========= ADJUST THESE TO TUNE BOIDS BEHAVIORS ========= #
    sepfactor   = 0.0 #25 # separation importance weight: all avoid others inside the v2v_dNeighClose radius (too close)
    alignfactor = 0.0#15 # alignment  importance weight: all align to average of other's headings inside v2v_dNeighViz radius (visible)
    cohesfactor = 0.15 # cohesion   importance weight: all move to CG of swarm
    # ========= ==================================== ========= #
    
    steer_max = 45*(np.pi/180.0) # (rad) set lock-to-lock max steer limit of 35deg, from ME620 Adv. Veh. Dyn. (~0.6rad)
    k_steer   = 0.1 #1.5 # steer gain between 0.3 and 0.5 is good; tuned in Simulink, ME620 Advanced Vehicle Dynamics
    
    spdNominal = 10 #5 #2.0 # (m/s) vehicle nominal wander speed
    dtDormant  = 0.01 #20 + randFlag*random.uniform(0.0,20.0) # (s) behavior dormant period (not turning)
    dtTurn_o   = 0.01 # (s) turn duration when randFlag==0
    steer      = 0.0 # (rad) nominal steer command is positive, or left-hand turn when randFlag==0
    while e.is_set() is False:
        
        e.wait(dtDormant) # (sec) behavior is dormant for this duration; e.wait() is an interruptable sleep() with independent timers despite using the same event, e
        
        # default on all boids speeds and headings
        #hdgSep=0; hdgAlign=0; hdgCoh=0;  spdSep=0; spdAlign=0; spdCoh=0
        
        # boids separation, alignment, and cohesion algorithms
        dv_X_sep  , dv_Y_sep   = boidsSeparation( vid,veh.v2v.v2vState,v2v_dNeighClose, sepfactor   )
        dv_X_align, dv_Y_align = boidsAlignment ( vid,veh.v2v.v2vState                , alignfactor )
        dv_X_coh  , dv_Y_coh   = boidsCohesion  ( vid,veh.v2v.v2vState,v2v_dNeighViz  , cohesfactor )
        
        dv_X = dv_X_sep + dv_X_align + dv_X_coh # (m/s) add inertial X and Y dv components; important weights for each (sep,align,coh) are built into the dv's
        dv_Y = dv_Y_sep + dv_Y_align + dv_Y_coh
        # --- boids() algo stops here --- #
        
        
        if (printLevelLocal>0):
            logging.debug('\tdv_X_sep = {0:0.2f},\tdv_X_align = {1:0.2f},\tdv_X_coh = {2:0.2f}'.format(dv_X_sep,dv_X_align,dv_X_coh))
            logging.debug('\tdv_Y_sep = {0:0.2f},\tdv_Y_align = {1:0.2f},\tdv_Y_coh = {2:0.2f}'.format(dv_Y_sep,dv_Y_align,dv_Y_coh))
        
        
        # --- convert boids() XY changes in XY velocity to MoVE's body-fixed speed (scalar) and heading (deg) command
        spdCmd = 0.0*spdNominal + sqrt( pow(dv_X,2) + pow(dv_Y,2) ) #+ pow(dv_Z,2) ) # (m/s) scalar
        hdg_des_deg = (180.0/np.pi)*atan2(dv_Y,dv_X) # (deg) from (rad)
        
        
        psi_desired = hdg_des_deg     # (deg) desired steer angle
        psi_veh     = veh.pos.hdg_deg # (deg) current vehicle heading; w.r.t. North, hdg_deg=(180/pi)*(pi/2 - psi), psi is orientation w.r.t. +X-axis
        psi_err_raw = (np.pi/180.0)*(psi_desired - psi_veh) # (rad) heading error, psi_err_raw = psi_desired - psi_veh
        
        # unwrap such that hdgSum is on [-pi  +pi]
        # credit: Mike Fair, Nov 2004
        spsi = sin(psi_err_raw)
        cpsi = cos(psi_err_raw)
        # psi_err is in body-fixed frame on inteval [-pi +pi] - this essentially says, turn left or turn right
        psi_err = atan2(spsi,cpsi) # (rad) heading error w.r.t. vehicle x-axis, atan2() returns [-pi  +pi]
        
        
        steer_raw = -k_steer*tanh( psi_err ) - 0.05*veh.vel.psiDot
        steer = np.clip( steer_raw , -steer_max, +steer_max) # (rad) impose max steer angle, clip is numpy's saturation function
        
        if (printLevelLocal>0):
            logging.debug('\tpsi_desired = {0:0.2f} (deg),\tpsi_veh = {1:0.2f} (deg)'.format(psi_desired,psi_veh))

        #logging.debug('\thdgErrRaw={0}(rad), steer_raw={1}(rad), steer={2}(rad), spdSum={3}'.format( hdgErrRaw,steer_raw,steer,spdSum ))
        
        if randFlag>0:
            dtTurn = dtTurn_o + random.uniform( 0.0 , 2.0*dtTurn_o) # (s) turn duration
            steer  = steer    + random.uniform(-0.2 ,+0.2          ) # uniformly distributed random number
        else:
            dtTurn = dtTurn_o
        
        if (debug>1): logging.debug('{0:s}, bId={1:d}, pri={2:d}: turn={3:f}'.format(behName, bId, priority, steer))
        
        if (printLevelLocal>0):
            logging.debug('current v2vState:\n' + pformat(veh.v2v.v2vState))
        
        
        # enable cmds with this format: [ bId , steer , speed, pitch, progress ]
        self.qBehavior.put( [ bId , steer , spdCmd, 0.0, 0.0, 0.0 ] )
        
        #time.sleep(dtTurn) # (sec) duration to turn
        e.wait(dtTurn) # (sec) duration to turn; interruptable sleep()
        
        #self.qBehavior.put( [ bId , 0 ] ) # disable commands with [bId, 0]
    
    logging.debug('[{0}]: bId={1} [{2}], exiting.'.format( datetime.now().strftime('%c') ,bId,behName))









# example v2vState for testing
def myDev():
    vid=100
    ith_vid=101
    v2vState={100: {'hdg_deg': 90.00021199438781,
       'inRangeNeighClose': set(),
       'inRangeNeighViz': set(),
       'inRangeRadio': set(),
       'missionAction': 'none',
       'missionCounter': 1,
       'missionLastCmdTime': 'Sun Mar 17 15:20:41 2024',
       'missionPctComplete': 0.0,
       'missionProgress': -1,
       'missionState': -1,
       'missionStatus': 'readyWait',
       'posX': 0.0,
       'posY': 0.0,
       'posZ': 0.0,
       'psiDot': 0.0,
       'self': True,
       'spd_mps': 0.0,
       'stale': False,
       'tLocal_elap': 102.1316876411438,
       'tLocal_fmtd': '2024_03_17__15_22_23',
       'tLocal_recd': 1710703343.6749022,
       'tNowVeh': 1710703341.7890763,
       'tNowVeh_fmtd': '2024_03_17__15_22_21',
       'v2vMcastCntRecd': 152,
       'v2vMcastCntSent': 147,
       'vid': 100},
 101: {'hdg_deg': 90.00021199438781,
       'inRangeNeighClose': {100},
       'inRangeNeighViz': {100},
       'inRangeRadio': {100},
       'missionAction': 'none',
       'missionCounter': 1,
       'missionLastCmdTime': 'Sun Mar 17 15:21:24 2024',
       'missionPctComplete': 0.0,
       'missionProgress': -1,
       'missionState': -1,
       'missionStatus': 'readyWait',
       'posX': 0.0,
       'posY': 0.0,
       'posZ': 0.0,
       'psiDot': 0.0,
       'self': False,
       'spd_mps': 0.0,
       'stale': True,
       'tLocal_elap': 46.555726289749146,
       'tLocal_fmtd': '2024_03_17__15_21_28',
       'tLocal_recd': 1710703288.0989408,
       'tNowVeh': 1710703286.770344,
       'tNowVeh_fmtd': '2024_03_17__15_21_26',
       'v2vMcastCntRecd': 3,
       'v2vMcastCntSent': 0,
       'vid': 101}
    }
    return vid,ith_vid,v2vState

# call the myDev() function
vid,ith_vid,v2vState = myDev()

