# MoVE multi-vehicle scenario definition configuration file.
#
# default.cfg: 2 or 3 vehicles demonstration at Tall Timbers in Tallahassee for NASA SBIR Wildfires
#
#
# Marc Compere, comperem@gmail.com
# created : 04 Jan 2019
# modified: 12 Dec 2022
#
# --------------------------------------------------------------
# Copyright 2018 - 2022 Marc Compere
#
# This file is part of the Mobility Virtual Environment (MoVE).
# MoVE is open source software licensed under the GNU GPLv3.
#
# MoVE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3
# as published by the Free Software Foundation.
#
# MoVE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License version 3 for more details.
#
# A copy of the GNU General Public License is included with MoVE
# in the COPYING file, or see <https://www.gnu.org/licenses/>.
# --------------------------------------------------------------



# ------------------------------------------------------------------------------
# core and vehicle processes look at this section for common configuration
# ------------------------------------------------------------------------------
[common]

nVeh_builtin  = 0   # launch this many built-in vehicle models with behaviors specified below
nVeh_live_gps = 3   # launch this many live-GPS-follower processes listening for incoming GPS coords on
nVeh_custom   = 0   # unused; for future expansion

nVeh_hosts = 1 # keep nVeh_hosts=1 until functionality is coded to launch across multiple vehicle hosts (parallel-ssh can do this very easily)
core_host_ip: localhost # localhost only valid for core_host_ip when veh_host_ip is *also* localhost (if not, then veh_model starts somewhere else and sends to that machine)
veh_host_ip: localhost

# each vehicle identifier is assigned with:  vid = vid_base + i_th_veh  (builtins first, then live gps followers)
# each vehicle listens for core messages on port = (udp_port_base + vid)
# each vehicle sends updates to core on     port = (udp_port_base + vid + udp_port_offset)
# each live GPS follower process listens for lat/lon on port = udp_port_base_gps + vid
# don't touch these unless you need more than 500 built-in + live gps followers
vid_base = 100
udp_port_base = 6000 # for vehicle-to-Core messages (veh_udp_io.py; avoid zeroconf on port 5353 w ubuntu linux)
udp_port_offset = 500 # for Core-to-vehicle messages (veh_udp_io.py)
udp_port_base_gps = 8000  # for live GPS follower messages (with lat/lon) from a mobile device running HyperIMU: udp_port_base_gps+vid

# debug levels: 0,1,2,3 (higher gives more console debugging output)
debug=1 #2 #1


# ------------------------------------------------------------------------------
[scenario]
#origin latitude and longitude
#boundary geometry
#no-go geometry
# waypoint sets for path following
#detectId = 100 # vid for vehicle-side detectAndReport() behavior
detectThreshold = 100 # (m) detection radius threshold
#detectThreshold = 5 # (m) detection radius threshold

# define boundary for stayInBounds() behavior (if enabled); stay in this square
boundary_Xmax = +250.0 # (m) UTM coordinates w.r.t. origin
boundary_Xmin = -250.0 # (m)
boundary_Ymax = +250.0 # (m)
boundary_Ymin = -250.0 # (m)

# airCube definition
# c[cid] = { cid, center, s, direction }
# where:
#   cid    - cube identifier
#   center - (lat,lon,zCeil)
#   s      - (m) cube side length
#   dir    - travel direction: (e2w: East-to-West), (w2e: West-to-East), (s2n: South-to-North), or (n2s: North-to-South)
airCube_100: {'centerLat': , dir:'e2w'}


# ------------------------------------------------------------------------------
[core]

sync_veh_and_cfg: True # (0/1) if true, scp this config file to the vehicle host prior to launching veh processes

logfile: 1 #1 # (0/1) log all vehicles in State to local .csv file at 1s intervals (defined by dt in all_vehicles.py | poll_state() )
logfile_loc_core: /tmp #/run/shm #/tmp, note: careful! you can crash your machine using /run/shm b/c is a ramdisk by default on Ubuntu and Debian systems

dtOut = 0.1 # (s) communication output interval for Core to send to viz, database, and csv logfile

# incorporate core viz outputs:
vizUdpEnabled=True
vizIp   = localhost
#vizIp   = 10.33.106.227 # Garrett's ip on wireless network
#vizIp   = 155.31.130.51 # Garrett's ip on wired network
vizPort = 5555

dashIp   = localhost
dashPort = 5556

# MongoDB interface
#dbUdpEnabled=False
#dbIp   = 10.33.106.103 # Otto's laptop IP on EagleNET
#dbIp   = 155.31.130.169 # Otto's laptop IP on EagleNET
#dbPort = 5000

# enable ADS-B with True below then start the ADSB sender: fileWatcher_ADSB_decoder.py
adsbEnabled=True # enable dedicated udp socket to listen on this port for individual ADS-B messages from fileWatcher_ADSB_decoder.py
adsbPort = 10000 # listen on this port from anyone

# ------------------------------------------------------------------------------
[veh_v2v]
v2vEnabled=False #True # (True/False) enable or disable v2v threads and multicast udp/ip communications
multicast_group = 224.3.29.71 # ipv4 addresses 224.0.0.0 through 230.255.255.255 are reserved for multicast traffic
multicast_port  = 10001 # udp multicast port for all v2v senders and listeners
v2v_cint_send   = 1.0 # (s) outbound v2v communication interval for broadcasting a single vehicle's update to all other vehicles
v2v_cint_read   = 2.0 # (s) receiver's observation interval of complete v2vState snapshot; read complete v2v netowrk view used by mission scheduler at this interval
v2v_dRadio      = 500 # (m) radio range; any vehicles within this distance are radio active
v2v_dNeighViz   = 50  # (m) neighbor range; any vehicles within this distance are visual neighbors
v2v_dNeighClose = 10  # (m) neighbor range; any vehicles within this distance are visual and also very close neighbors (too close)
v2v_printLevel  = 1 #2 #1 # (0/1/2) how much to print v2v updates in the veh model console output


# ------------------------------------------------------------------------------
[veh_sim]
vehRuntimeDir_base=/tmp/move # rsync veh_model to remote machine for execution in this folder + username, note: don't put this in /run/shm b/c it's a ramdisk where logging can consume all ram
h = 0.010 # (s) vehicle dynamics RK4 integration stepsize; choose cInt and h such that they are an integer ratio, say cInt/h=10
cInt=0.1 # (s) vehicle-to-core reporting interval; local .csv logging interval; 0.1 is 10Hz, 0.5 is 2Hz
hInt = 1.0 # (s) vehicle-to-core hearbeat interval; only relevant during runState==1 (READY)
randFlag=1 # (0/1) use random numbers? plumbed in everwhere relevant (ICs and random triggers in behaviors)

IC_sel = 1 # (1/2/3) global default: 1->straight down the X-axis, 2-> grid pattern within geom extents, 3->collision scenario for 2 vehicles
L_char = 2.0 # (m) characteristic vehicle length
v_max = 26.8 # (m/s) nominal max vehicle speed (26.8m/s=60mph)

IC_elev = 0.0 # (m) default height for all vehicles in UTM XYZ coordinate frame

#lat_origin =  29.193111 # (decimal deg) Home plate, Baseball fields, Embry-Riddle Aeronautical Univ, Daytona Beach, Florida
#lon_origin = -81.046171 # (decimal deg)
#lat_origin =  29.1916 # (decimal deg) Home plate, Softball fields, Embry-Riddle Aeronautical Univ, Daytona Beach, Florida
#lon_origin = -81.04555 # (decimal deg)
#lat_origin =  29.18869 # (decimal deg) M-building parking lot, Embry-Riddle Aeronautical Univ, Daytona Beach, Florida
#lon_origin = -81.04554 # (decimal deg)

lat_origin =  30.659579 # (decimal deg) Tall Timbers, Prescribed Fire, Tallahassee, Florida
lon_origin = -84.226824 # (decimal deg)

#lat_origin = 39.230999 # (decimal deg) XELEVATE conference Virgina
#lon_origin = -77.550504 # (decimal deg)
#lat_origin =  34.614 # (decimal deg) Prescott dorms
#lon_origin = -112.449 # (decimal deg)
#lat_origin =  34.59557 # (decimal deg) 29 Jul 2021, day 1, morning, Cherry ridge
#lon_origin = -112.058338 # (decimal deg)


log_level: 2 # (0/1/2) 0->no logging, 1-> .csv file only, 2-> .csv and capture console output in /tmp/debug*.txt for debugging veh model and behavior
             # log_level==1 -> monitor veh console output by reattaching to screen with 'screen -r' to watch running vehicle process
             # log_level==2 -> monitor veh console output with 'tail -f /tmp/debug100.txt' to monitor vehicle model console output (screen holds the process but has no output to display)
logfile_loc_veh: /tmp #/run/shm #/tmp, note: careful! you can crash your machine using /run/shm b/c is a ramdisk by default on Ubuntu and Debian systems


# vehicle types:
#   'pedestrian' vehSubType--> 'male' or 'female'
#   'ground',    vehSubType--> 'onroad' or 'offroad'
#   'aerial',    vehSubType--> 'fixedwing' or 'rotorcraft'
#   'surface'
#   'underwater'


# only change global defaults for these specific vehicles
[veh_details] # optionally assign: vehicle name, vehType, vehSubType, initial conditions
# note: as of v1.07 only move_live_mapping_v3.py respects the vehType and vehSubType
#veh_type_default: { 'vehType': 'pedestrian',  'vehSubType': 'none' }
#veh_type_default: { 'vehType': 'ground',      'vehSubType': 'none' }
veh_type_default: { 'vehType': 'aerial',      'vehSubType': 'rotorcraft' }
#veh_type_default: { 'vehType': 'surface',     'vehSubType': 'none' }
#veh_type_default: { 'vehType': 'underwater',  'vehSubType': 'turtle' }

# SAE +X-axis is to the East, or right on the page
# SAE +Y-axis is North, or towards the top of the page
# SAE +Z-axis is down, or into the page, thereforce +yaw means a RH-turn
# IC_yaw_rad=0 aims the vehicle directly East, down the +X-axis

vid_100: {'name':'thor'    , 'vehType': 'aerial', 'vehSubType':'fixedwing',  'IC_latlon':'29.192   , -81.046   ', 'IC_elev': 0.0, 'IC_yaw_rad': -1.5  }
vid_101: {'name':'ironman'  , 'vehType': 'aerial', 'vehSubType':'rotorcraft', 'IC_latlon':'29.192   , -81.046   ', 'IC_elev': 0.0, 'IC_yaw_rad': -1.5  }
vid_102: {'name':'falcon' , 'vehType': 'aerial', 'vehSubType':'rotorcraft', 'IC_latlon':'29.192   , -81.046   ', 'IC_elev': 0.0, 'IC_yaw_rad': -1.5  }

#vid_100: {'name':'kaleb', 'vehType': 'ground', 'vehSubType':'none', 'IC_latlon':'29.193899, -81.046432', 'IC_elev': 0.0, 'IC_yaw_rad':  0.0  }
#
#vid_101: {'name':'rocio'   , 'vehType': 'pedestrian', 'vehSubType':'none', 'IC_latlon':'29.1915  , -81.045   ', 'IC_elev': 0.0, 'IC_yaw_rad': +1.0  }
#vid_102: {'name':'diogo'   , 'vehType': 'pedestrian', 'vehSubType':'none', 'IC_latlon':'29.1915  , -81.045   ', 'IC_elev': 0.0, 'IC_yaw_rad': +1.0  }
#vid_103: {'name':'danny'   , 'vehType': 'pedestrian', 'vehSubType':'none', 'IC_latlon':'29.1915  , -81.045   ', 'IC_elev': 0.0, 'IC_yaw_rad': +1.0  }
#
#vid_104: {'name':'falcon'  , 'vehType': 'aerial', 'vehSubType':'rotorcraft', 'IC_latlon':'29.193337, -81.045272', 'IC_elev': 0.0, 'IC_yaw_rad': +2.15 } # 3*pi/4 = 2.3562
#vid_105: {'name':'spiderman'   , 'vehType': 'aerial', 'vehSubType':'rotorcraft', 'IC_latlon':'29.1925  , -81.047   ', 'IC_elev': 0.0, 'IC_yaw_rad': -2.8  }


# ------------------------------------------------------------------------------
# prioritize or disable (set to 0) behaviors for all vehicles; see [veh_behaviors_individual] for vehicle-specific behavior settings
# note #1: higher priority takes precendence over lower; priority=0 disables
# note #2: these names are case-sensitive and must match exact function names as defined in vehicle's behaviors.py
[veh_behaviors_global]
wander          : 0 # default is to turn everything off globally
periodicTurn    : 0
periodicPitch   : 0
stayInBounds    : 0
avoidBySteer    : 0 # for aerial vehicles to avoid collision using *steer*
avoidByBrake    : 0 # for ground vehicles to avoid collision using longitudinal (braking) (slow down to not hit the veh in front)
detectAndReport : 0 # todo: convert this to (or add) followMe() behavior
followRoute     : 0
goToGate        : 0
goToPoint       : 0
multiRotorHover : 0 # UAV equivalent to default wander process; lowest priority; when nothing else is happening, just hover


# per-vehicle behavior priorities override global settings in [veh_behaviors_global]
[veh_behaviors_individual] # enable behaviors for individual vehicles
#vid_100: {'wander': 1, 'periodicTurn': 2, 'stayInBounds': 10, 'avoidBySteer': 15 }
#vid_101: {'wander': 1, 'periodicTurn': 2, 'stayInBounds': 10, 'avoidBySteer': 15 }
#vid_102: {'wander': 1, 'periodicTurn': 2, 'stayInBounds': 10, 'avoidBySteer': 15 }
#vid_103: {'wander': 1, 'periodicTurn': 2, 'stayInBounds': 10, 'avoidBySteer': 15 }
#vid_104: {'wander': 1, 'periodicTurn': 2, 'stayInBounds': 10, 'avoidBySteer': 15 }





# define points for use with goToPoint() or the mission sequencer action 'goToPoint'
[points] # points of interest in mission commands
#pt_A0: {'lat': 29.193111, 'lon': -81.046171, 'elev': 0.000000, 'nextPoint': 'pt_A1', 'vel':4.0 } # (dec deg) and (m), ERAU baseball field, home plate
#pt_A1: {'lat': 29.193159, 'lon': -81.045979, 'elev': 0.000000, 'nextPoint': 'pt_A2', 'vel':1.0 } # (dec deg) and (m), ERAU baseball field, first base
#pt_A2: {'lat': 29.193329, 'lon': -81.046032, 'elev': 0.000000, 'nextPoint': 'pt_A3', 'vel':2.0 } # (dec deg) and (m), ERAU baseball field, second base
#pt_A3: {'lat': 29.193279, 'lon': -81.046226, 'elev': 0.000000, 'nextPoint': 'pt_A0', 'vel':3.0 } # (dec deg) and (m), ERAU baseball field, third base
#
#pt_B0: {'lat': 29.193184, 'lon': -81.045712, 'elev': 0.000000, 'nextPoint': 'pt_B1', 'vel':3.0 } # (dec deg) and (m), ERAU baseball field, right field sideline
#pt_B1: {'lat': 29.193255, 'lon': -81.045649, 'elev': 0.000000, 'nextPoint': 'pt_B2', 'vel':3.0 } # (dec deg) and (m), ERAU baseball field, soccer corner kick
#pt_B2: {'lat': 29.193449, 'lon': -81.045656, 'elev': 0.000000, 'nextPoint': 'pt_B3', 'vel':3.0 } # (dec deg) and (m), ERAU baseball field, soccer free kick penalty mark
#pt_B3: {'lat': 29.193337, 'lon': -81.045813, 'elev': 0.000000, 'nextPoint': 'pt_B1', 'vel':3.0 } # (dec deg) and (m), ERAU baseball field, soccer 1/4 field sideline







# define gates; to enable gates, the goToGate() behavior must be turned on;
# gates must be defined with Point A on the left and Point B on the right, plus an initial gate assignment must be made
[gates] # gate_NNN: [ <lon_A>, <lat_A>,    <lon_B>, <lat_B>,    <nextGate> ] # this makes a gate with point A at (lat_A,lon_A) and point B at (lat_B,lon_B)
#gate_1: {'ptA_lon': -81.046308, 'ptA_lat': 29.193577,    'ptB_lon': -81.046126, 'ptB_lat': 29.193865,   'nextGate': 2 }   # gate 1 at West end of ERAU Baseball field, Goto gate 2
#gate_2: {'ptA_lon': -81.045650, 'ptA_lat': 29.193255,    'ptB_lon': -81.045468, 'ptB_lat': 29.193541,   'nextGate': 1 }   # gate 2 at East end of ERAU Baseball field, Goto gate 1 (this causes the ping pong)
#gate_1: {'ptA_lon': -81.048414, 'ptA_lat': 29.191032,    'ptB_lon': -81.046126, 'ptB_lat': 29.193865,   'nextGate': 2 }   # gate 1 at West end of ERAU Baseball field, Goto gate 2
#gate_2: {'ptA_lon': -81.045650, 'ptA_lat': 29.193255,    'ptB_lon': -81.048414, 'ptB_lat': 29.191032,   'nextGate': 1 }   # gate 2 at East end of ERAU Baseball field, Goto gate 1 (this causes the ping pong)





# a mission table is constructed from rows of commands, for a particular vehicle identifier (vid)
# labels prior to the ':' have no influence but must be unique
# each vid's set of commands are read (in any order) and sorted by missionState
[missionCommands]
#missionCmd1:  {'vid':100, 'missionState':  1 , 'action':'goToPoint',        'point'          : 'pt_A1' } # point names much match [location] names above
#missionCmd2:  {'vid':100, 'missionState': 10 , 'action':'waitOnVeh',        'otherVeh'       :  101,     'progress': 11.0   } # progress >= threshold
#missionCmd3:  {'vid':100, 'missionState': 20 , 'action':'goToPoint',        'point'          : 'pt_A2' } # point names much match [location] names above
#missionCmd4:  {'vid':100, 'missionState': 30 , 'action':'goToPoint',        'point'          : 'pt_A3' } # point names much match [location] names above
#missionCmd5:  {'vid':100, 'missionState': 40 , 'action':'waitOnVeh',        'otherVeh'       :  101,     'progress': 31.0   } # progress >= threshold
#missionCmd6:  {'vid':100, 'missionState':100 , 'action':'goToMissionState', 'newMissionState':    1 }

# actionWhenComplete:'missionState=3' is achieved by the next command
# what is implied is to advance to the next mission command in the mission table.

#missionCmd7:  {'vid':101, 'missionState':  1 , 'action':'waitOnVeh',        'otherVeh'       :  100,     'progress': 2.0   } # progress >= threshold
#missionCmd8:  {'vid':101, 'missionState': 10 , 'action':'goToPoint',        'point'          : 'pt_B1' } # point names much match [location] names above
#missionCmd9:  {'vid':101, 'missionState': 20 , 'action':'goToPoint',        'point'          : 'pt_B2' } # point names much match [location] names above
#missionCmd10: {'vid':101, 'missionState': 30 , 'action':'goToPoint',        'point'          : 'pt_B3' } # point names much match [location] names above
#missionCmd11: {'vid':101, 'missionState': 40 , 'action':'waitOnVeh',        'otherVeh'       :  100,     'progress': 31.0   } # progress >= threshold
#missionCmd12: {'vid':101, 'missionState':100 , 'action':'goToMissionState', 'newMissionState':    1 }






# ------------------------------------------------------------------------------
[veh_route_assignments] # vid_NNN: [ <route_number>, <nLaps>, <startOption>, <startingWaypoint>, <endOption> ]
#
# each vehicle wakes up and looks here to determine if and what route to follow
# note: make sure the followRoute() behavior has non-zero priority so it gets started
#
# format:
#     vid_NNN:                                      # vehicle identifier, vid_100, vid_101, ...
#              {'routeScript':'<route_file.py>',    # python script defining route (see ./routes)
#               'nLapsMax':<num>,                   # max number of laps, then trigger end option
#               'startOption':'startAt',            # either 'startAt' or 'goTo' (only 'startAt' is working now)
#               'startingWaypoint':  <num>,         # starting waypoint
#               'lapRestartWaypoint':<num>,         # restarting waypoint number after each lap
#               'endOption':'stop',                 # either 'stop' or 'continue'
#               'mapColor':'<color>' }              # for example, 'magenta', 'red'
#
#vid_100: {'routeScript':'route_01_waypoints.py', 'nLapsMax':19999, 'startOption':'startAt', 'startingWaypoint': 1, 'lapRestartWaypoint':1, 'endOption':'stop', 'mapColor':'yellow' }
#vid_101: {'routeScript':'route_01_waypoints.py', 'nLapsMax':29999, 'startOption':'startAt', 'startingWaypoint': 5, 'lapRestartWaypoint':1, 'endOption':'stop', 'mapColor':'yellow' }
#vid_102: {'routeScript':'route_01_waypoints.py', 'nLapsMax':39999, 'startOption':'startAt', 'startingWaypoint': 9, 'lapRestartWaypoint':1, 'endOption':'stop', 'mapColor':'yellow' }





[datatable_display]
# these strings must match [titles] list entries in move_dashboard.py | [col_titles]
enable_table_col_0= "Veh ID"
enable_table_col_1= "Veh Name"
enable_table_col_2= "Veh Type"
enable_table_col_3= "Veh Sub Type"
enable_table_col_4= "Last Core Update"
#enable_table_col_5= "Run State"
enable_table_col_6= "Behvaior"
#enable_table_col_7= "Beh Progress"
#enable_table_col_8= "GPS time"
#enable_table_col_9= "Veh time (s)"
#enable_table_col_10= "X Loc (m)"
#enable_table_col_11="Y Loc (m)"
#enable_table_col_12="Z Loc (m)"
#enable_table_col_13="Lat"
#enable_table_col_14="Lon"
#enable_table_col_15="Heading (deg)"
enable_table_col_16="Yaw (deg)"
enable_table_col_17="Veh Spd (m/s)"
enable_table_col_18="Waypoint"
enable_table_col_19="Lapnum"
enable_table_col_20="misAction"
enable_table_col_21="misState"
enable_table_col_22="misPctComplete"
enable_table_col_23="misProgress"
enable_table_col_24="misLastCmdTime"
enable_table_col_25="misStatus"
enable_table_col_26="misCounter"







[veh_names_builtins]
name1: doc
name2: bashful
name3: dopey
name4: grumpy
name5: happy
name6: sleepy
name7: sneezy
name8: mickey
name9: minnie
name10: daffy
#name11: donald
#name12: daisy
#name13: bambi
#name14: thumper
#name15: scrooge
#name16: pluto
#name17: mercury
#name18: venus
#name19: earth
#name20: mars
#name21: jupiter
#name22: saturn
#name23: uranus
#name24: neptune
# Ninja turtles: Leonardo da Vinci, Michelangelo, Raphael, and Donatello
# Marvel characters: SpiderMan, Thanos, IronMan, DeadPool, BlackWidow, CaptainAmerica, Hulk, Wolverine, Thor, BlackPanther, Thing, Loki, Wasp, Quicksilver, Nebula


[veh_names_live_gps]
name1: kaleb
name2: falon
name3: spiderman
name4: ironman
name5: danny
name6: rocio
name7: diogo
